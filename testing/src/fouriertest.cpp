#include "fouriertest.h"
#include "processing/oclprojectionfilters.h"

#include <vector>
#include <complex>
#include <algorithm>

using namespace CTL::assist;

void FourierTest::testDft()
{
    std::vector<float> evenImageSpace(8);
    std::vector<float> oddImageSpace(9);

    std::iota(evenImageSpace.begin(), evenImageSpace.end(), 0.f);
    std::iota(oddImageSpace.begin(), oddImageSpace.end(), 0.f);

    const auto evenFourierSpace = dft(evenImageSpace);
    const auto oddFourierSpace = dft(oddImageSpace);

    const std::vector<std::complex<double>> expectedEvenFourierSpace {
        {28., 0.}, {-4., 9.65685424949238}, {-4., 4.}, {-4., 1.6568542494923806},
        {-4., 0.}, {-4., -1.6568542494923806}, {-4., -4.}, {-4., -9.65685424949238}
    };

    const std::vector<std::complex<double>> expectedOddFourierSpace {
        {36., 0.}, {-4.5, 12.36364839}, {-4.5, 5.36289117},
        {-4.5, 2.59807621}, {-4.5, 0.79347141}, {-4.5, -0.79347141},
        {-4.5, -2.59807621}, {-4.5, -5.36289117}, {-4.5, -12.36364839}
    };

    auto fuzzyCompare = [](const std::complex<double>& lhs, const std::complex<double>& rhs)
    {
        return (abs(lhs.real() - rhs.real()) < 1e-8) &&
            (abs(lhs.imag() - rhs.imag()) < 1e-8);
    };

    for(auto idx = 0u; idx < evenFourierSpace.size(); ++idx)
    {
        if(!fuzzyCompare(evenFourierSpace[idx], expectedEvenFourierSpace[idx]))
        {
            QFAIL("evenFourierSpace has not been computed correctly.");
        }
    }

    for(auto idx = 0u; idx < oddFourierSpace.size(); ++idx)
    {
        if(!fuzzyCompare(oddFourierSpace[idx], expectedOddFourierSpace[idx]))
        {
            QFAIL("oddFourierSpace has not been computed correctly.");
        }
    }
}

void FourierTest::testIdft()
{
    const std::vector<std::complex<double>> evenFourierSpace {
        {1., 0.}, {1., 1.}, {2., .5}, {3., 1./3},
        {4., 0}, {3., -1./3}, {2., -.5}, {1., -1}
    };
    const std::vector<std::complex<double>> oddFourierSpace {
        {1., 0.}, {1., 1.}, {2., .5}, {3., 1./3}, {4, .25},
        {4., -.25}, {3., -1./3}, {2., -.5}, {1., -1}
    };

    const std::vector<float> expectedEvenImageSpace {
        2.125f, -1.0892556509887896f,
       -0.041666666666666685f, -0.13214886980224205f,
        0.125f, 0.08925565098878963f,
        0.2916666666666667f, -0.36785113019775795f
    };

    const std::vector<float> expectedOddImageSpace {
        2.333333333333333f, -1.1455114091799263f, -0.0773344620194598f,
       -0.14433756729740643f, 0.04975320917680198f, 0.07779744687642866f,
        0.14433756729740643f, 0.23664144360658237f, -0.4746795617937603f
    };

    const auto evenImageSpace = idft(evenFourierSpace);
    const auto oddImageSpace = idft(oddFourierSpace);

    QCOMPARE(evenImageSpace, expectedEvenImageSpace);
    QCOMPARE(oddImageSpace, expectedOddImageSpace);
}

void FourierTest::testFftshift()
{
    std::vector<int> evenSpace(8);
    std::vector<int> oddSpace(9);

    std::iota(evenSpace.begin(), evenSpace.end(), 0);
    std::iota(oddSpace.begin(), oddSpace.end(), 0);

    fftshift(evenSpace);
    fftshift(oddSpace);

    const std::vector<int> expectedShiftedEvenSpace{4, 5, 6, 7, 0, 1, 2, 3};
    const std::vector<int> expectedShiftedOddSpace{5, 6, 7, 8, 0, 1, 2, 3, 4};

    QCOMPARE(evenSpace, expectedShiftedEvenSpace);
    QCOMPARE(oddSpace, expectedShiftedOddSpace);
}

void FourierTest::testIfftshift()
{
    std::vector<int> evenSpace(8);
    std::vector<int> oddSpace(9);

    std::iota(evenSpace.begin(), evenSpace.end(), 0);
    std::iota(oddSpace.begin(), oddSpace.end(), 0);

    ifftshift(evenSpace);
    ifftshift(oddSpace);

    const std::vector<int> expectedShiftedEvenSpace{4, 5, 6, 7, 0, 1, 2, 3};
    const std::vector<int> expectedShiftedOddSpace{4, 5, 6, 7, 8, 0, 1, 2, 3};

    QCOMPARE(evenSpace, expectedShiftedEvenSpace);
    QCOMPARE(oddSpace, expectedShiftedOddSpace);
}
#include "errormetrictest.h"
#include "processing/errormetrics.h"
#include <iostream>
#include <iomanip>

using namespace CTL::metric;

class Kiss32
{
public:
    Kiss32(float upperBound = 1.0f, uint seed = 42)
    : X(345678901u + (seed << 11))
    , Y(456789012u + (seed << 13))
    , Z(789012345u + (seed << 17))
    , W(890123456u + (seed << 19))
    , C(0u)
    , scale(upperBound)
    {}

    float operator()()
    {
        return computeNext() * scale / 4294967295.0f;
    }

private:
    uint computeNext()
    {
        X += 545925293;
        Y ^= Y << 13;
        Y ^= Y >> 17;
        Y ^= Y << 5;

        uint t = Z + W + C;
        Z = W;
        C = t >> 31;
        W = t & 2147483647;

        return X + Y + W;
    }

    uint X;
    uint Y;
    uint Z;
    uint W;
    uint C;
    float scale;
};

void ErrorMetricTest::initTestCase()
{
    constexpr auto seqLength = 1u << 21;
    _randSeq1.resize(seqLength);
    _randSeq2.resize(seqLength);

    std::generate(_randSeq1.begin(), _randSeq1.end(), Kiss32(3.0f, 12345u));
    std::generate(_randSeq2.begin(), _randSeq2.end(), Kiss32(3.5f, 67891u));

/*
    using namespace CTL;
    std::cout << std::setprecision(16)
              << "L1:\t"  << metric::L1(_randSeq1, _randSeq2) << '\n'
              << "L2:\t"  << metric::L2(_randSeq1, _randSeq2) << '\n'
              << "rL1:\t" << metric::rL1(_randSeq1, _randSeq2)<< '\n'
              << "rL2:\t" << metric::rL2(_randSeq1, _randSeq2)<< '\n'
              << "RMSE:\t"<< metric::RMSE(_randSeq1, _randSeq2)<< '\n'
              << "rRMSE:\t"<< metric::rRMSE(_randSeq1, _randSeq2)<< '\n'
              << "corrErr:\t"<< metric::corrErr(_randSeq1, _randSeq2)<< '\n'
              << "GMCPreuhs:\t"<< metric::GMCPreuhs(_randSeq1, _randSeq2)<< '\n'
              << "cosSimErr:\t"<< metric::cosSimErr(_randSeq1, _randSeq2)<< '\n'
              << "nGMCPreuhs:\t"<< metric::nGMCPreuhs(_randSeq1, _randSeq2)<< '\n';
*/
}

void ErrorMetricTest::testCorr() const
{
    QCOMPARE(corrErr(_randSeq1, _randSeq2), 0.9982798998600207);
    QCOMPARE(cosSimErr(_randSeq1, _randSeq2), 0.2497012399780127);
}

void ErrorMetricTest::testLN() const
{
    QCOMPARE(L1(_randSeq1, _randSeq2), 2319972.265225879);
    QCOMPARE(L2(_randSeq1, _randSeq2), 1959.205324436132);
    QCOMPARE(rL1(_randSeq1, _randSeq2), 0.6324461754217506);
    QCOMPARE(rL2(_randSeq1, _randSeq2), 0.6697399382328262);
}

void ErrorMetricTest::testGMC() const
{
    QCOMPARE(GMCPreuhs(_randSeq1, _randSeq2), 346509.8087950504);
    QCOMPARE(nGMCPreuhs(_randSeq1, _randSeq2), 0.6609150100613601);
}

void ErrorMetricTest::testRMSE() const
{
    QCOMPARE(RMSE(_randSeq1, _randSeq2), 1.352897822896073);
    QCOMPARE(rRMSE(_randSeq1, _randSeq2), 0.6697399382328262);
}

#ifndef FOURIERTEST_H
#define FOURIERTEST_H

#include <QtTest>

class FourierTest : public QObject
{
    Q_OBJECT
public:
    FourierTest() = default;

private Q_SLOTS:
    void testDft();
    void testIdft();
    void testFftshift();
    void testIfftshift();
};

#endif // FOURIERTEST_H

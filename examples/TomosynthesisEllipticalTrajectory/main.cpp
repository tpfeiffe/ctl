#include <QApplication>
#include "ctl.h"
#include "ctl_ocl.h"
#include "ctl_qtgui.h"

/*
 * This example is supposed to show a C-arm acquisition setup using a tomosynthesis trajectory
 * that is an ellipsis on a sphere.
 */

// helper function for creating a disk phantom
CTL::VoxelVolume<float> createDisk(float offSet, float muValue)
{
    constexpr auto radius = 50.f;
    constexpr auto height = 20.f;
    constexpr auto voxSize = 0.5f;
    auto ret = CTL::VoxelVolume<float>::cylinderZ(radius, height, voxSize, muValue);
    ret.setVolumeOffset(0.f, 0.f, offSet);
    return ret;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // define a phantom
    const auto diskPhantom = CTL::CompositeVolume{ createDisk(-30.f, 0.1f),
                                                   createDisk(  0.f, 0.2f),
                                                   createDisk( 30.f, 0.3f) };

    //defining the setup
    constexpr auto nbViews = 100u;
    constexpr auto sourceToIsocenter = 785.;
    constexpr auto startAngle = 10._deg;
    constexpr auto smallAngle = 15._deg;
    constexpr auto largeAngle = 23._deg;
    constexpr auto landscapeDetectorOrientation = true;

    auto setup = CTL::AcquisitionSetup(
        CTL::makeCTSystem<CTL::blueprints::GenericCarmCT>(CTL::DetectorBinning::Binning4x4),
        nbViews);

    setup.applyPreparationProtocol(CTL::protocols::TomosynthesisEllipticalTrajectory(
        sourceToIsocenter, startAngle, smallAngle, largeAngle, landscapeDetectorOrientation));

    CTL::gui::plot(setup);

    // projecting the volume
    const auto projections = CTL::OCL::RayCasterProjector{}.configureAndProject(setup, diskPhantom);

    CTL::gui::plot(projections);

    // reconstruct
    // CTL::gui::plot(CTL::OCL::FDKReconstructor{}.configureAndReconstruct(
    //                    setup, projections, CTL::VoxelVolume<float>(256u, 256u, 256u,
    //                                                                1.f, 1.f, 1.f)));
    //
    // CTL::gui::plot(CTL::ARTReconstructor{}.configureAndReconstruct(
    //                    setup, projections, CTL::VoxelVolume<float>(256u, 256u, 256u,
    //                                                                1.f, 1.f, 1.f)));


    return a.exec();
}

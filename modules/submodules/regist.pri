# dependency
!CTL_CORE_MODULE: error("REGIST_MODULE needs CTL_CORE_MODULE -> include ctl_core.pri before regist.pri")
!OCL_CONFIG_MODULE: error("REGIST_MODULE needs OCL_CONFIG_MODULE -> include ocl_config.pri before regist.pri")
!OCL_ROUTINES_MODULE: error("REGIST_MODULE needs OCL_ROUTINES_MODULE -> include ocl_routines.pri before regist.pri")

# declare module
CONFIG += REGIST_MODULE
DEFINES += REGIST_MODULE_AVAILABLE

HEADERS += \
    $$PWD/../src/registration/grangeatregistration2d2d.h \
    $$PWD/../src/registration/grangeatregistration2d3d.h \
    $$PWD/../src/registration/projectionregistration2d3d.h

SOURCES += \
    $$PWD/../src/registration/grangeatregistration2d2d.cpp \
    $$PWD/../src/registration/grangeatregistration2d3d.cpp \
    $$PWD/../src/registration/projectionregistration2d3d.cpp

# NLopt library [ https://github.com/stevengj/nlopt/ ]
unix:LIBS += -L/usr/local/lib
LIBS += -lnlopt

#ifndef CTL_GROWINGVESSELPHANTOM_H
#define CTL_GROWINGVESSELPHANTOM_H

#include "img/abstractdynamicvolumedata.h"
#include "models/vesselphantomgenerator.h"

namespace CTL {

class GrowingVesselPhantom : public AbstractDynamicVolumeData
{
    // abstract interface
    protected: void updateVolume() override;
    public: SpectralVolumeData* clone() const override;

public:
    GrowingVesselPhantom(VoxelVolume<float> initalVolume);

    VesselPhantomGenerator::Parameters& generatorSettings();
    void setSpeed(double speed);
    void setSeed(const uint &seed);
    uint seed() const;
    double speed() const;

private:
    VesselPhantomGenerator::Parameters _par; //!< all parameters of the used vessel generator
    uint _seed = 133337u; //!< seed for the underlying RNG
    double _speed = 10.0; //!< number of iterations per ms
};

} // namespace CTL

#endif // CTL_GROWINGVESSELPHANTOM_H

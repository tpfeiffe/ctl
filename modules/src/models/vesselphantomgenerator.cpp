#include "vesselphantomgenerator.h"
#include "mat/matrix_algorithm.h"

namespace CTL {

namespace  {
Vector3x1 positionToIndex(const Vector3x1& position, const VoxelVolume<float>& volume);
}

struct VesselPhantomGenerator::WalkerState
{
    Vector3x1 position; //!< position in 3D space [mm]
    Vector3x1 direction; //!< unit vector with current direction
    double diameter; //!< current diameter [mm]
    uint64_t age; //!< age is inherited to children
};

VesselPhantomGenerator::Parameters& VesselPhantomGenerator::parameters() { return _p; }

void VesselPhantomGenerator::setSeed(uint seed)
{
    _seed = seed;
    _uRNG.seed(seed);
}

void VesselPhantomGenerator::drawPhantom(VoxelVolume<float>& volume) const
{
    if(!volume.hasData())
        volume.fill(0.0f);

    WalkerState walker{ _p.startingPoint, randomDirection().normalized(), _p.startDiameter, 0 };
    performRandomWalk(walker, volume);
}

void VesselPhantomGenerator::advanceWalker(WalkerState& walker) const
{
    double directionVectorLength;
    do
        directionVectorLength = (walker.direction += randomDirection()).norm();
    while(qFuzzyIsNull(directionVectorLength));

    walker.direction /= directionVectorLength;
    walker.position += walker.direction * _p.stepLength;
    walker.diameter *= _p.linearNarrowing;
    walker.age += 1;
}

void VesselPhantomGenerator::drawDisk(const WalkerState& walker,
                                      VoxelVolume<float>& volume) const
{
    const auto voxSize = Vector3x1{ volume.voxelSize().x,
                                    volume.voxelSize().y,
                                    volume.voxelSize().z };
    // in-plane basis vectors
    auto b1 = mat::orthonormalTo(walker.direction);
    auto b2 = mat::cross(walker.direction, b1);
    // scale to voxel grid
    b1 |= voxSize;
    b2 |= voxSize;

    // radii
    const auto r1 = 0.5 * b1.norm() * walker.diameter;
    const auto r2 = 0.5 * b2.norm() * walker.diameter;

    b1.normalize();
    b2.normalize();

    const auto center = positionToIndex(walker.position, volume);
    const auto corner = center - r1 * b1 - r2 * b2;
    const auto invRadSqr1 = 1.0 / (r1 * r1);
    const auto invRadSqr2 = 1.0 / (r2 * r2);
    // diameters (semi-major and semi-minor axes as numbers of voxels)
    const auto d1 = uint(std::ceil(2.0 * r1));
    const auto d2 = uint(std::ceil(2.0 * r2));

    if(d1 <= 2u || d2 <= 2u)
        paintIntoVoxel(center, volume);
    else
        for(auto i = 0u; i < d1; ++i)
            for(auto j = 0u; j < d2; ++j)
            {
                if((i - r1) * (i - r1) * invRadSqr1 +
                   (j - r2) * (j - r2) * invRadSqr2 > 1.0)
                    continue;

                paintIntoVoxel(corner + i * b1 + j * b2, volume);
            }
}

void VesselPhantomGenerator::drawPoint(const WalkerState& walker,
                                       VoxelVolume<float>& volume) const
{
    paintIntoVoxel(positionToIndex(walker.position, volume), volume);
}

void VesselPhantomGenerator::performRandomWalk(WalkerState& walker,
                                               VoxelVolume<float>& volume) const
{
    do
        walk(walker, volume);
    while(walker.diameter >= _p.minimumDiameter);
}

void VesselPhantomGenerator::walk(WalkerState& walker, VoxelVolume<float>& volume) const
{
    // paint walker into volume
    if(walker.age < _maxNbIter)
        drawDisk(walker, volume); //drawPoint(walker, volume);

    // walker takes one step further
    advanceWalker(walker);

    // proliferation = possible creation of a 2nd walker
    tryBranching(walker, volume);
}

uint64_t VesselPhantomGenerator::maxNbIter() const { return _maxNbIter; }

void VesselPhantomGenerator::setMaxNbIter(uint64_t maxNbIter) { _maxNbIter = maxNbIter; }

void VesselPhantomGenerator::paintIntoVoxel(const Vector3x1& voxelIdx,
                                            VoxelVolume<float>& volume) const
{
    const auto xIdx = uint(std::round(voxelIdx.get<0>()));
    const auto yIdx = uint(std::round(voxelIdx.get<1>()));
    const auto zIdx = uint(std::round(voxelIdx.get<2>()));

    if(xIdx < volume.nbVoxels().x &&
       yIdx < volume.nbVoxels().y &&
       zIdx < volume.nbVoxels().z)
        volume(xIdx, yIdx, zIdx) = _p.vesselValue;
}


Vector3x1 VesselPhantomGenerator::randomDirection() const
{
    std::normal_distribution<> d{ 0.0, _p.gaussianSigma };
    return { d(_uRNG), d(_uRNG), d(_uRNG) };
}

void VesselPhantomGenerator::tryBranching(WalkerState& walker, VoxelVolume<float>& volume) const
{
    std::uniform_real_distribution<> d(0.0, 1.0);
    if(d(_uRNG) < _p.branchingProb)
    {
        static constexpr auto oneOverSqrt2 = 0.7071067811865475244;
        walker.diameter *= oneOverSqrt2;
        if(walker.diameter <= _p.minimumDiameter)
            return;

        auto secondWalker = walker;
        performRandomWalk(secondWalker, volume);
    }
}

namespace {

Vector3x1 positionToIndex(const Vector3x1& position, const VoxelVolume<float>& volume)
{
    return ((position - Vector3x1{ volume.offset().x, volume.offset().y, volume.offset().z })
            | Vector3x1{ volume.voxelSize().x, volume.voxelSize().y, volume.voxelSize().z })
           + Vector3x1{ volume.nbVoxels().x - 1.0,
                        volume.nbVoxels().y - 1.0,
                        volume.nbVoxels().z - 1.0 } * 0.5;
}

} // unnamed namespace

} // namespace CTL

#ifndef CTL_ABSTRACTPHANTOMGENERATOR_H
#define CTL_ABSTRACTPHANTOMGENERATOR_H

#include "img/voxelvolume.h"

namespace CTL {

class AbstractPhantomGenerator
{
public:virtual void drawPhantom(VoxelVolume<float>& volume) const = 0;

public:
    virtual ~AbstractPhantomGenerator() = default;

    virtual VoxelVolume<float> createPhantom(const VoxelVolume<float>::Dimensions& nbVoxels,
                                             const VoxelVolume<float>::VoxelSize& voxelSize) const
    {
        VoxelVolume<float> ret(nbVoxels, voxelSize);
        ret.fill(0.0f);

        drawPhantom(ret);

        return ret;
    }
};

} // namespace CTL

#endif // CTL_ABSTRACTPHANTOMGENERATOR_H

#include "intervaldataseries.h"

namespace CTL {

/*!
 * \brief Constructs an empty IntervalDataSeries object for a series with bin width \a binWidth.
 *
 * If required, set the beginning of the first interval (i.e. lower edge of the first bin) with
 * \a firstBinStart and specify the type of data, your sampled values will represent (i.e. either bin
 * integrals or mean values) by \a valueType. Note that none of these properties may be changed
 * afterwards. If unspecified, the first bin will start at 0.0 and the value type will default to
 * BinIntegral.
 *
 * Data points can be added to the series with append().
 *
 * Example: we create a series that contains values with bin width 1.0, with its first bin starting
 * at x=-2.5 and representing mean values. We then fill in the values 1, 2, 3, and 4.
 * \code
 * auto series = IntervalDataSeries(1.0f, -2.5f, IntervalDataSeries::MeanValue);
 *
 * series.append(1.0f); // series: (-2.0, 1.0)
 * series.append(2.0f); // series: (-2.0, 1.0), (-1.0, 2.0)
 * series.append(3.0f); // series: (-2.0, 1.0), (-1.0, 2.0), (0.0, 3.0)
 * series.append(4.0f); // series: (-2.0, 1.0), (-1.0, 2.0), (0.0, 3.0), (1.0, 4.0)
 * \endcode
 *
 * Note that the sampling points (i.e. bin centers) are computed automatically based on the bin
 * width and starting point of the first bin.
 */
IntervalDataSeries::IntervalDataSeries(float binWidth, float firstBinStart, IntervalDataSeries::ValueType valueType)
    : _binWidth(binWidth)
    , _firstBinStart(firstBinStart)
    , _valueType(valueType)
{
}

/*!
 * \brief Constructs an IntervalDataSeries object and directly sets its data to \a values.
 *
 * The series is created with a bin width of \a binWidth and its first bin starting at
 * \a firstBinStart. The type of data that is represented by the values in the series (i.e. either
 * bin integrals or mean values) can be set by \a valueType. Note that none of these properties may
 * be changed afterwards. If unspecified, the first bin will start at 0.0 and the value type will
 * default to BinIntegral.
 *
 * Data passed by \a values will be added to the series, defining the first values.size() samples.
 * Additional data points can later be added to the series with append().
 *
 * Example: we create a series that contains the following samples: (-2.0, 1.0), (-1.0, 2.0),
 * (0.0, 3.0), (1.0, 4.0), which shall represent mean values. For that, we need a bin width of 1.0
 * and a first bin starting point of -2.5f.
 *
 * \code
 * auto series = IntervalDataSeries(1.0f, -2.5f, { 1.0f, 2.0f, 3.0f, 4.0f }, IntervalDataSeries::MeanValue);
 * \endcode
 *
 * Note that the sampling points (i.e. bin centers) are computed automatically based on the bin
 * width and starting point of the first bin.
 */
IntervalDataSeries::IntervalDataSeries(float binWidth, float firstBinStart, const std::vector<float>& values,
                                       IntervalDataSeries::ValueType valueType)
    : IntervalDataSeries(binWidth, firstBinStart, valueType)
{
    setValues(values);
}

/*!
 * \brief Return a series of \a nbSamples values sampled from \a dataModel within the specified
 * interval [ \a from, \a to ].
 *
 * You can choose whether the sampled values shall be data integrated over the entire bin or mean
 * values within the individual intervals, by passing the corresponding flag to \a samples
 * (default: IntervalDataSeries::BinIntegral).
 *
 * \a nbSamples must be greater than 0. The resulting series has a bin width of
 * \f$(\to - from) / nbSamples\f$.
 *
 * Example:
 * \code
 * // create a step function (steps from 0 to 1 at x=0)
 * auto dataModel = StepFunctionModel();
 *
 * // sample the interval [-5, 5] with 5 values, i.e. bin width is 2.0
 * // we create two series: series1 containing bin integrals (default) and series2 with mean values
 * auto series1 = IntervalDataSeries::sampledFromModel(dataModel, -5.0f, 5.0f, 5);
 * auto series2 = IntervalDataSeries::sampledFromModel(dataModel, -5.0f, 5.0f, 5, IntervalDataSeries::MeanValue);
 *
 * // print results
 * for(const auto& pt : series1.data())
 *     qInfo() << pt.x() << pt.y();
 *
 * // output: (bin center, value)-pairs
 * // -4 0
 * // -2 0
 * // 0 1
 * // 2 2
 * // 4 2
 *
 * for(const auto& pt : series2.data())
 *     qInfo() << pt.x() << pt.y();
 *
 * // output: (bin center, value)-pairs
 * // -4 0
 * // -2 0
 * // 0 0.5
 * // 2 1
 * // 4 1
 * \endcode
 */
IntervalDataSeries IntervalDataSeries::sampledFromModel(const AbstractIntegrableDataModel& dataModel,
                                                        float from, float to, uint nbSamples,
                                                        ValueType samples)
{
    Q_ASSERT(from <= to);
    Q_ASSERT(nbSamples > 0);

    using SamplingFct = float (AbstractIntegrableDataModel::*) (float, float) const;

    SamplingFct sampFct = nullptr;
    switch (samples) {
    case BinIntegral:
        sampFct = &AbstractIntegrableDataModel::binIntegral;
        break;
    case MeanValue:
        sampFct = &AbstractIntegrableDataModel::meanValue;
        break;
    }

    const auto binWidth = (to - from) / float(nbSamples);

    IntervalDataSeries ret(binWidth, from, samples);
    ret._data.reserve(nbSamples);

    const auto smpPts = ret.samplingPointsForIndices(0, nbSamples);
    for(const auto& smpPt : smpPts)
        ret._data.append(QPointF(smpPt, (dataModel.*sampFct)(smpPt, binWidth)));

    return ret;
}

/*!
 * \brief Overload for std::shared_ptr input.
 */
IntervalDataSeries IntervalDataSeries::sampledFromModel(
        std::shared_ptr<AbstractIntegrableDataModel> dataModel, float from, float to, uint nbSamples,
        ValueType samples)
{
    return sampledFromModel(*dataModel, from, to, nbSamples, samples);
}

/*!
 * \brief Appends all values in \a values to the series.
 *
 * Automatically computes the corresponding sampling points (i.e. bin center coordinates).
 *
 * Example:
 * \code
 * auto series = IntervalDataSeries(1.0f, -1.5f);
 *
 * series.append({ 1.0f, 2.0f, 3.0f }); // series: (-1.0, 1.0), (0.0, 1.0), (1.0, 2.0)
 * \endcode
 */
void IntervalDataSeries::append(const std::vector<float>& values)
{
    const auto nbNewSamples = uint(values.size());
    const auto smpPts = samplingPointsForIndices(nbSamples(), nbSamples() + nbNewSamples);
    _data.reserve(nbSamples() + nbNewSamples);

    for(uint i = 0; i < nbNewSamples; ++i)
        _data.append(QPointF(smpPts[i], values[i]));
}

/*!
 * \brief Appends \a value to the series.
 *
 * Automatically computes the corresponding sampling point (i.e. bin center coordinate).
 *
 * Example:
 * \code
 * auto series = IntervalDataSeries(1.0f, -1.5f);
 *
 * series.append(1.0f); // series: (-1.0, 1.0)
 * series.append(2.0f); // series: (-1.0, 1.0), (0.0, 1.0)
 * series.append(3.0f); // series: (-1.0, 1.0), (0.0, 1.0), (1.0, 2.0)
 * \endcode
 */
void IntervalDataSeries::append(float value)
{
    const auto binCenter = _firstBinStart + (static_cast<float>(nbSamples()) + 0.5f) * _binWidth;
    _data.append(QPointF(binCenter, value));
}

/*!
 * \brief Returns the integral over the series.
 *
 * If valueType() == IntervalDataSeries::BinIntegral, this is the same as sum().
 */
float IntervalDataSeries::integral() const
{
    auto sum = this->sum();

    if(_valueType == MeanValue) // multiply with bin width to get integral
        sum *= _binWidth;

    return sum;
}

/*!
 * \brief Returns the integral over the series with each bin value being weighted by the
 * corresponding factor in \a weights.
 *
 * The weight vector must contain at least as many elements as there are samples in the series.
 *
 * If valueType() == IntervalDataSeries::BinIntegral, this is the same as the weighted sum.
 */
float IntervalDataSeries::integral(const std::vector<float>& weights) const
{
    Q_ASSERT(weights.size() >= nbSamples());

    // compute weighted sum, i.e.: value(i) * weigths(i)
    auto productSum = this->sum(weights);

    if(_valueType == MeanValue) // multiply with bin width to get integral
        productSum *= _binWidth;

    return productSum;
}

/*!
 * \brief Normalizes the values in the series such that the integral will be one afterwards.
 */
void IntervalDataSeries::normalizeByIntegral()
{
    const auto intgr = integral();

    if(qFuzzyIsNull(intgr))
    {
        qWarning("Trying to normalize data series with integral 0. Skipped normalization.");
        return;
    }

    for(auto& pt : _data)
        pt.ry() /= intgr;
}

/*!
 * \brief Returns a copy of the series in which the values have been normalized such that the
 * integral will be one.
 */
IntervalDataSeries IntervalDataSeries::normalizedByIntegral() const
{
    IntervalDataSeries ret(*this);
    ret.normalizeByIntegral();
    return ret;
}

void IntervalDataSeries::setValue(uint sampleNb, float value)
{
    _data[sampleNb].setY(value);
}

/*!
 * \brief Sets the values passed by \a values as bin values for this series.
 *
 * This replaces any previous data.
 *
 * \code
 * auto series = IntervalDataSeries(1.0f, -1.5f);
 *
 * series.append(1.0f); // series: (-1.0, 1.0)
 * series.append(2.0f); // series: (-1.0, 1.0), (0.0, 1.0)
 *
 * series.setValues({ 42.0f, 66.6f, 133.7f }); // series: (-1.0, 42.0), (0.0, 66.6), (1.0, 133.7)
 * \endcode
 */
void IntervalDataSeries::setValues(const std::vector<float>& values)
{
    _data.clear();

    append(values);
}

/*!
 * \brief Returns the boundaries of the interval covered by the bin with index \a binNb.
 *
 * This returns the lower and upper boundary of the interval covered by the bin with index \a binNb
 * as a SamplingRange.
 *
 * Example:
 * \code
 *  IntervalDataSeries series(10.0f);
 *  series.append(13.37f);
 *  series.append(42.0f);
 *  series.append(6.66f);
 *
 *  const auto bounds = series.binBounds(1);
 *
 *  qInfo() << QString("Bin [%1, %2] has value: %3").arg(bounds.start())
 *                                                  .arg(bounds.end())
 *                                                  .arg(series.value(1));
 *
 *  // output: "Bin [10, 20] has value: 42"
 * \endcode
 */
SamplingRange IntervalDataSeries::binBounds(uint binNb) const
{
    const auto binCenter = _data.at(binNb).x();
    return { binCenter - 0.5f * _binWidth, binCenter + 0.5f * _binWidth };
}

/*!
 * \brief Returns the bin width (or *x* distance between two consecutive sampling points).
 */
float IntervalDataSeries::binWidth() const
{
    return _binWidth;
}

/*!
 * \brief Returns the centroid of the series.
 *
 * Example:
 * \code
 *  // create an IntervalDataSeries with bin width 1 starting at 0.0 (default value)
 *  IntervalDataSeries series(1.0f);
 *  series.append(0.0f);
 *  series.append(1.0f);
 *  series.append(2.0f);
 *  series.append(3.0f);
 *  series.append(4.0f);
 *
 *  qInfo() << series.centroid();   // output: 3.5
 * \endcode
 */
float IntervalDataSeries::centroid() const
{
    float sum = 0.0f;

    for(const auto& pt : _data)
        sum += float(pt.x()) * float(pt.y());

    return sum / integral();
}

/*!
 * \brief Returns the range (of *x*-values) covered by all bins of this series.
 *
 * Example:
 * \code
 *  // create an IntervalDataSeries with bin width 1 starting at 0.0 (default value)
 *  IntervalDataSeries series(1.0f);
 *  series.append(32.5f);   // this is bin [0, 1]
 *  series.append(2.6f);    // this is bin [1, 2]
 *  series.append(456.9f);  // this is bin [2, 3]
 *
 *  qInfo() << series.samplingRange().toPair();   // output: std::pair(0,3)
 * \endcode
 */
SamplingRange IntervalDataSeries::samplingRange() const
{
    const auto halfBinWidth = 0.5f * _binWidth;
    return { _data.front().x() - halfBinWidth, _data.back().x() + halfBinWidth };
}

/*!
 * \brief Returns the type of the values in the series, i.e. whether they represent bin integrals
 * or mean values.
 *
 * This will be BinIntegral in most cases. However, if specified explicitely during creation of an
 * IntervalDataSeries, series can be created with ValueType MeanValue; so consider checking for the
 * actual type with this method if that is of concern for your specific use case.
 */
IntervalDataSeries::ValueType IntervalDataSeries::valueType() const
{
    return _valueType;
}

/*!
 * \brief Computes the sampling points for bins with indices between \a startIdx and \a endIdx.
 *
 * Uses computation scheme that is safe even for small bin widths.
 */
std::vector<float> IntervalDataSeries::samplingPointsForIndices(uint startIdx, uint endIdx) const
{
    std::vector<float> ret;
    ret.reserve(endIdx - startIdx);
    for(uint s = startIdx; s < endIdx; ++s)
    {
        ret.push_back(_firstBinStart + (float(s) + 0.5f) * _binWidth);
    }

    return ret;
}

/*!
 * \brief Clamps all values in this series to the interval specified by \a range.
 *
 * This means that all values smaller than range.start() and values larger than range.end() will be
 * set to zero.
 *
 * Note that a series that used to be normalized beforehand might no longer be normalized after the
 * clamp operation.
 *
 * Example:
 * \code
 * // vector: 0, 1, 2, ...
 * std::vector<float> vec(5);
 * std::iota(vec.begin(), vec.end(), 0.0f);
 *
 * // Interval series with bin width 1, starting at 0.0, with values from vec
 * IntervalDataSeries series(1.0f, 0.0f, vec);
 * qInfo() << series.data();
 * // output: (QPointF(0.5,0), QPointF(1.5,1), QPointF(2.5,2), QPointF(3.5,3), QPointF(4.5,4))
 *
 * // clamp samples to interval [1.5 4.0]
 * series.clampToRange({1.5, 4.0});
 * qInfo() << series.data();
 * // output: (QPointF(0.5,0), QPointF(1.5,0.5), QPointF(2.5,2), QPointF(3.5,3), QPointF(4.5,0))
 * \endcode
 */
void IntervalDataSeries::clampToRange(const SamplingRange& range)
{
    const auto halfBinWidth = 0.5f * _binWidth;

    auto isFullyContained = [&range, halfBinWidth] (float binPos)
    {
        const auto binStart = binPos - halfBinWidth;
        const auto binEnd = binPos + halfBinWidth;
        return binStart >= range.start() && binEnd <= range.end();
    };

    auto isFullyOutside = [&range, halfBinWidth] (float binPos)
    {
        const auto binStart = binPos - halfBinWidth;
        const auto binEnd = binPos + halfBinWidth;
        return binEnd < range.start() || binStart > range.end();
    };

    auto relBinOverlap = [&range, halfBinWidth] (float binPos)
    {
        const auto binStart = binPos - halfBinWidth;
        const auto binEnd = binPos + halfBinWidth;
        const auto absoluteOverlap = std::min(binEnd, range.end()) -
                                     std::max(binStart, range.start());
        return absoluteOverlap / (2.0f * halfBinWidth);
    };

    for(auto& pt : _data)
    {
        const auto binPos = float(pt.x());

        if(isFullyContained(binPos))
            continue;
        else if(isFullyOutside(binPos))
            pt.ry() = 0.0;
        else
            pt.ry() = pt.y() * relBinOverlap(binPos);
    }
}

} // namespace CTL

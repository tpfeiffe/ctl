#ifndef CTL_NUMERICALINTEGRATIONDECORATOR_H
#define CTL_NUMERICALINTEGRATIONDECORATOR_H

#include "abstractdatamodel.h"
#include "copyableuniqueptr.h"

namespace CTL {

/*!
 * \brief The NumericalIntegrationDecorator class is a decorator for any (non-integrable) data
 * model that provides integration functionality through Simpson's rule integration.
 *
 * This class can be used to turn any (non-integrable) data model into an
 * AbstractIntegrableDataModel subtype. Integration is carried out numerically using Simpson's rule.
 * The accuracy can be determined via the number of sub-intervals to be used in integration.
 * Set this value either during construction or through setNbIntervals().
 */
class NumericalIntegrationDecorator : public AbstractIntegrableDataModel
{
    CTL_TYPE_ID(1000)

    // implementation of abstract interface
    public: float valueAt(float position) const override;
    public: AbstractIntegrableDataModel* clone() const override;
    public: float binIntegral(float position, float binWidth) const override;

public:
    NumericalIntegrationDecorator(AbstractDataModel* model, uint nbIntervals = 1);
    NumericalIntegrationDecorator(std::unique_ptr<AbstractDataModel> model, uint nbIntervals = 1);

    // de-/serialization interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

    // getter / setter
    uint nbIntervals() const;
    void setNbIntervals(uint nbIntervals);

private:
    NumericalIntegrationDecorator() = default;

    AbstractDataModelPtr _model;
    uint _nbIntervals = 1;
};

} // namespace CTL

#endif // CTL_NUMERICALINTEGRATIONDECORATOR_H

#include "backprojectorbase.h"
#include "acquisition/geometryencoder.h"
#include "components/abstractdetector.h"
#include "ocl/clfileloader.h"
#include "ocl/openclconfig.h"
#include "img/voxelvolume.h"

#include <QMetaEnum>

const std::string CL_KERNEL_NAME_NULLIFIER = "nullify_buffer"; //!< name of the OpenCL kernel function (nullifier)
const std::string CL_FILE_NAME_NULLIFIER = "recon/nullifier.cl"; //!< path to .cl file

namespace CTL {
namespace OCL {

/*!
 * \brief Constructs BackprojectorBase with the OpenCL kernel \a backsmearKernel using weighting
 * defined by \a weightingType.
 */
BackprojectorBase::BackprojectorBase(cl::Kernel* backsmearKernel,
                                     WeightingType weightingType)
    : _weightingType(weightingType),
      _kernelBacksmear(backsmearKernel)
{
    initNullifierKernel();

    _zeroVec[0] = 0;
    _zeroVec[1] = 0;
    _zeroVec[2] = 0;
}

/*!
 * \brief Placeholder method to be used to transfer data for processing of \a nbViews views
 * beginning with \a startView.
 *
 * Re-implement this method to transfer any data that is required to process the views from the
 * interval [\a startView, \a startView + \a nbViews). This method is called at the beginning of a
 * new data block, i.e. before the aforementioned views are being processed.
 *
 * Using this segmented data transfer approach can be useful in case the full amount of data (i.e.
 * for all views at a time) would be to extensive (e.g. due to memory constraints).
 */
void BackprojectorBase::transferAdditionalData(uint, uint) {}

/*!
 * \brief Passes the AcquisitionSetup that describes the setting in which projections that shall
 * be backprojected have been acquired.
 *
 * This takes a copy of \a setup to make it available during the time of backprojection. Any
 * backprojection performed after configure() was called, will use the passed \a setup to extract
 * geometry information about the scan. Note that \a setup must be consistent with the projections
 * that shall be backprojected, that means it should contain information that actually corresponds
 * to the projections.
 *
 * Please also note that, in case that a subset of projections (by means of a ProjectionDataView)
 * is used in backprojection later on, the \a setup passed here must still refer to the full
 * projection data set, the ProjectionDataView belongs to.
 */
void BackprojectorBase::configure(const AcquisitionSetup& setup) { _setup = setup; }

/*!
 * \brief Returns the weighting factor for data from detector module \a module in view \a view.
 *
 * This base class implementation considers potential custom weights. If custom weights have been
 * set, this returns the corresponding custom weight for \a view and \a module. Otherwise, returns
 * 1.0.
 *
 * Re-implement in sub-classes to include further weighting factors. If doing so, remember to use
 * this (base class) version in your implementation to preserve custom weight functionality, e.g.:
 * \code
 * class Subclass : public BackprojectorBase
 * {
 * protected:
 *     float weighting(uint view, uint module) const override;
 *
 *     // ...
 * };
 *
 * float Subclass::weighting(uint view, uint module) const
 * {
 *     float cWeight = BackprojectorBase::weighting(view, module);
 *
 *     return cWeight * mySubclassWeightingFactor(view, module);
 * }
 * \endcode
 */
float BackprojectorBase::weighting(uint view, uint module) const
{
    return _customWeights.empty() ? 1.0f
                                  : _customWeights[view][module];
}

/*!
 * \brief Returns a reference-to-const to the current AcquisitionSetup set via configure().
 */
const AcquisitionSetup& BackprojectorBase::setup() const { return _setup; }

/*!
 * \brief Returns a reference-to-const to the OpenCL command queue used by this instance.
 *
 * Note: use the same command queue to schedule tasks in sub-classes to ensure correct processing
 * of events.
 */
const cl::CommandQueue& BackprojectorBase::queue() const { return _queue; }

/*!
 * \brief Returns the weighting type of this instance.
 */
BackprojectorBase::WeightingType BackprojectorBase::weightingType() const { return _weightingType; }

/*!
 * \brief Sets the weighting type of this instance to \a weightType.
 */
void BackprojectorBase::setWeightingType(WeightingType weigthType) { _weightingType = weigthType; }

/*!
 * \brief Sets custom weights to \a weights.
 *
 * An individual weighting factor must be provided for each detector module in each view, thus,
 * requiring a vector of a vector of floats (inner vector: modules, outer vector: views).
 */
void BackprojectorBase::setCustomWeights(ProjectionWeights weights)
{
    _customWeights = std::move(weights);
}

/*!
 * \copybrief AbstractReconstructor::parameter
 *
 * Returns a QVariantMap including the following (key, value)-pairs:
 *
 * - ("weighting type", [string] name of the weighting type),
 * - ("custom weights", [QVariantList] list of the custom weights (concatenated)).
 */
QVariant BackprojectorBase::parameter() const
{
    QVariantMap ret;

    ret.insert(QStringLiteral("weighting type"), metaEnum().valueToKey(int(weightingType())));

    // custom weights
    QVariantList viewWeights;
    viewWeights.reserve(static_cast<int>(_customWeights.size()));
    for(const auto& vec : _customWeights)
        viewWeights.push_back(assist::vectorToVariantList(vec));

    ret.insert(QStringLiteral("custom weights"), viewWeights);

    return ret;
}

/*!
 * \copybrief AbstractReconstructor::setParameter
 *
 * The passed \a parameter must be a QVariantMap containing one or more of the following
 * (key, value)-pairs:
 *
 * - ("weighting type", [string] name of the weighting type),
 * - ("custom weights", [QVariantList] list of the custom weights (concatenated)).
 *
 * Note that it is strongly discouraged to use this method to set individual parameters of the
 * instance. Please consider using individual setter methods for that purpose.
 */
void BackprojectorBase::setParameter(const QVariant& parameter)
{
    const QVariantMap parMap = parameter.toMap();

    if(parMap.contains(QStringLiteral("weighting type")))
    {
        const auto typeStr = parMap.value(QStringLiteral("weighting type")).toString();
        int type = metaEnum().keyToValue(qPrintable(typeStr));
        if(type == -1)
        {
            qWarning() << "BackprojectorBase::setParameter: Incorrect weighting type specified. "
                          " (valid options: [DistanceWeightsOnly | GeometryFactors]). "
                          "Using default value (GeometryFactors).";
            type = 0;
        }

        setWeightingType(WeightingType(type));
    }

    if(parMap.contains(QStringLiteral("custom weights")))
    {
        const auto weightsList = parMap.value(QStringLiteral("custom weights")).toList();
        _customWeights.reserve(weightsList.size());

        for(const auto& viewWeightVar : qAsConst(weightsList))
            _customWeights.push_back(assist::variantListToVector<float>(viewWeightVar.toList()));
    }
}

/*!
 * \brief Backprojects data from \a projections into \a targetVolume and returns \c true on success.
 *
 * Note that the AcquisitionSetup corresponding to \a projections must have been set previously with
 * configure().
 *
 * \a targetVolume serves as an initialization, which means that data backprojected from
 * \a projections will be added to any values already present in \a targetVolume. If \a targetVolume
 * has no memory allocated when passed here, memory allocation will be performed as part of the
 * backprojection step. The volume will be initialized with zero in that case.
 */
bool BackprojectorBase::reconstructToPlain(const ProjectionDataView& projections,
                                           VoxelVolume<float>& targetVolume)
{
    if(!isConfigConsistentWith(projections))
        return false;

    if(!customWeightsValid(projections))
        return false;

    const auto nbViews = projections.nbViews();

    // note that projection matrices are properly normalized for distance weighting (see OpenCL kernel)
    const auto pMats = GeometryEncoder::encodeSubsetGeometry(_setup, projections.viewIds());
    precomputeGeometryFactors(targetVolume.voxelSize().product(), pMats);
    const auto concPMats = pMats.concatenatedStdVector();

    // transfer funtion for view-dependent input data
    auto transferInputData = [&](uint startView)
    {
        const auto viewsToTransfer = std::min(_viewsPerBlock,
                                              nbViews - startView);

        transferProjectionData(projections, startView, viewsToTransfer);
        transferProjectionMatrices(concPMats.data(), startView, viewsToTransfer);

        return startView + viewsToTransfer; // end of transferred views
    };

    try // exception handling
    {
        createCommandQueue();
        setDimensions(projections.dimensions(), targetVolume);

        // create buffers and pinned memory
        createBuffers();
        createPinnedMemory();

        // set kernel arguments
        setFixedKernelArgs();

        // write volume-specific buffers
        writeFixedBuffers();

        auto isInitializedVol = targetVolume.hasData();
        if(!isInitializedVol)
            targetVolume.allocateMemory();

        auto endView = 0u;
        while(endView < nbViews)
        {
            const auto startView = endView;
            endView = transferInputData(startView);
            transferAdditionalData(startView, endView - startView);

            qDebug() << "process views: [" << startView << ',' << endView << ')';

            for(auto z = 0u, nbSlices = targetVolume.nbVoxels().z; z < nbSlices; ++z)
            {
                Q_EMIT notifier()->progress(qreal(z) / nbSlices);

                // set slice buffer to zero
                if(isInitializedVol)
                    transferSlice(targetVolume, z);
                else
                    startNullifierKernel();

                // smear back all views
                for(auto view = startView; view < endView; ++view)
                    startBacksmearKernel(z, view);

                // read result of slice into full volume
                readoutResult(targetVolume, z);
            }

            isInitializedVol = true;
        }

        freeGPUMemory();

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error: " << err.what() << "(" << err.err() << ")";
        return false;
    } catch(const std::bad_alloc& except)
    {
        qCritical() << "Allocation error: " << except.what();
        return false;
    } catch(const std::exception& except)
    {
        qCritical() << "std exception: " << except.what();
        return false;
    }

    return true;
}

/*!
 * \brief Returns \c true if the currently configured setup is consistent with \a projections.
 *
 * Details on what is checked precisely can be found in assist::isConsistentPair().
 */
bool BackprojectorBase::isConfigConsistentWith(const ProjectionDataView& projections) const
{
    if(!assist::isConsistentPair(_setup, projections))
    {
        qCritical() << "BackprojectorBase: Reconstruction aborted.";
        return false;
    }
    return true;
}

/*!
 * \brief Returns \c true if the custom weights set to this instance are consistent with
 * \a projections.
 *
 * This verifies:
 * - weights are either empty, or the following two must apply
 * - outer vector of weights (views) has same size as there are views in \a projections
 * - all inner vectors of weights (modules) have same size as there are modules in \a projections.
 *
 * If a condition is violated, issues a qCritical message and returns \c false.
 */
bool BackprojectorBase::customWeightsValid(const ProjectionDataView& projections) const
{
    if(_customWeights.empty())
        return true;

    if(_customWeights.size() != projections.nbViews())
    {
        qCritical() << "SimpleBackprojector::reconstructToPlain(): Reconstruction aborted. "
                       "Reason: custom weights contain data with a number of elements that "
                       "does not match the number of views";
        return false;
    } else
    {
        for(const auto& viewWeights : _customWeights)
            if(viewWeights.size() != projections.dimensions().nbModules)
            {
                qCritical() << "SimpleBackprojector::reconstructToPlain(): Reconstruction aborted. "
                               "Reason: custom weights contain data with a number of elements that "
                               "does not match the number of modules";
                return false;
            }
    }

    return true;
}

/*!
 * \brief Returns a pointer to the cl::Kernel object of this instances OpenCL backprojection kernel.
 */
cl::Kernel *BackprojectorBase::backsmearKernel() const { return _kernelBacksmear; }

/*!
 * \brief Creates the OpenCL command queue.
 *
 * This creates a command queue on the first device queried from OCL::OpenCLConfig in its current
 * configuration.
 */
void BackprojectorBase::createCommandQueue()
{
    auto& oclConfig = OCL::OpenCLConfig::instance();
    if(!oclConfig.isValid())
        throw std::runtime_error("BackprojectorBase::createCommandQueue: "
                                 "OpenCLConfig is not valid.");

    // create command queue on first device
    _queue = cl::CommandQueue(oclConfig.context(), oclConfig.devices().front());
}

/*!
 * \brief Creates OpenCL buffers for volume corner, voxel size, projection matrices and the slice
 * data.
 */
void BackprojectorBase::createBuffers()
{
    // context and number of used devices
    auto& context = OpenCLConfig::instance().context();

    const size_t nbVoxelInSlice = _nbVoxels[0] * _nbVoxels[1];

    _volCornerBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, sizeof(float) * 3);
    _voxSizeBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, sizeof(float) * 3);
    _pMatBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_HOST_WRITE_ONLY, sizeof(float) * 12 * _nbModules * _viewsPerBlock);
    _sliceBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(float) * nbVoxelInSlice);
}

/*!
 * \brief Creates OpenCL 3D image (using pinned memory approach) for projection data.
 */
void BackprojectorBase::createPinnedMemory()
{
    _projBuf.reset(new PinnedImg3DHostWrite(_projBufferDim[0], _projBufferDim[1], _projBufferDim[2], _queue));
}

/*!
 * \brief Cleans up the GPU memory allocated by createBuffers() and createPinnedMemory().
 */
void BackprojectorBase::freeGPUMemory()
{
    _volCornerBuffer = cl::Buffer{};
    _voxSizeBuffer = cl::Buffer{};
    _pMatBuffer = cl::Buffer{};
    _sliceBuffer = cl::Buffer{};

    _projBuf.reset(nullptr);
}

/*!
 * \brief Initializes the OpenCL kernel that nullifies slice data.
 *
 * If any OpenCL exception occurs, this instance reports the exception (of the cl::Error) as a
 * qWarning and throws an \c std::runtime_error with the message "OpenCL error".
 */
void BackprojectorBase::initNullifierKernel()
{
    try // OCL exception catching
    {
        auto& oclConfig = OCL::OpenCLConfig::instance();
        // general checks
        if(!oclConfig.isValid())
            throw std::runtime_error("OpenCLConfig is not valid");

        // check if required kernels are already provided
        if(!oclConfig.kernelExists(CL_KERNEL_NAME_NULLIFIER))
        {
            // load source code from file
            ClFileLoader clFile(CL_FILE_NAME_NULLIFIER);
            if(!clFile.isValid())
                throw std::runtime_error(CL_FILE_NAME_NULLIFIER + "\nis not readable");
            const auto clSourceCode = clFile.loadSourceCode();

            // add kernel to OCLConfig
            oclConfig.addKernel(CL_KERNEL_NAME_NULLIFIER, clSourceCode);
        }

        _kernelNullifier = oclConfig.kernel(CL_KERNEL_NAME_NULLIFIER);
        if(_kernelNullifier == nullptr)
            throw std::runtime_error("kernel pointer (nullifier) not valid");

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error: " << err.what() << "(" << err.err() << ")";
        throw std::runtime_error("OpenCL error");
    }
}

/*!
 * \brief Reads out the data current present in the slice buffer.
 */
void BackprojectorBase::readoutResult(VoxelVolume<float>& targetVolume, uint z)
{
    const size_t nbVoxelInSlice = _nbVoxels[0] * _nbVoxels[1];
    _queue.enqueueReadBuffer(_sliceBuffer, CL_TRUE, 0, sizeof(float) * nbVoxelInSlice,
                             targetVolume.rawData() + z * nbVoxelInSlice);
}

/*!
 * \brief Sets all relevant dimension spec for this instance based on the projection dimensions
 * \a projDim and the specifications of the volume \a targetVolume.
 *
 * In detail, this extracts:
 * - from \a projections:
 *      * number channels,
 *      * number rows,
 *      * number modules,
 * - from \a targetVolume:
 *      * number voxels (all 3 dim.),
 *      * voxel size (all 3 dim.),
 *      * volume offset (implicit through VoxelVolume<float>::cornerVoxel().
 *
 * This also determines the number of views that can be computed in one block (see viewsPerBlock()).
 */
void BackprojectorBase::setDimensions(const ProjectionData::Dimensions& projDim,
                                      const VoxelVolume<float>& targetVolume)
{
    // projections
    _viewsPerBlock = viewsPerBlock(projDim);
    _nbModules = projDim.nbModules;

    _projBufferDim[0] = projDim.nbChannels;
    _projBufferDim[1] = projDim.nbRows;
    _projBufferDim[2] = _viewsPerBlock * _nbModules;

    _bytesLocPMatMem = sizeof(float) * 12 * _nbModules;

    // volume
    const auto volCorner = targetVolume.cornerVoxel();

    _nbVoxels  = { targetVolume.nbVoxels().x, targetVolume.nbVoxels().y, targetVolume.nbVoxels().z };
    _voxelSize = { targetVolume.voxelSize().x, targetVolume.voxelSize().y, targetVolume.voxelSize().z };
    _corner    = { volCorner.x(), volCorner.y(), volCorner.z() };
}

/*!
 * \brief Sets all fixed (i.e. contant for the entire backprojection operation) kernel arguments.
 */
void BackprojectorBase::setFixedKernelArgs()
{
    _kernelNullifier->setArg(0, _sliceBuffer);

    /*
     * kernel void backprojector
     * ( uint z,                              --> arg 0
     *   uint viewNb,                         --> arg 1
     *   uint nbModules,                      --> arg 2
     *   __constant float3* volCorner_mm,     --> arg 3
     *   __constant float3* voxelSize_mm,     --> arg 4
     *   __global const float4* pMatsGlobal,  --> arg 5
     *   __local float4* pMatsLoc             --> arg 6
     *   __global float* sliceBuf,            --> arg 7
     *   __read_only image3d_t proj, ... )    --> arg 8
     */
    _kernelBacksmear->setArg(2, _nbModules);
    _kernelBacksmear->setArg(3, _volCornerBuffer);
    _kernelBacksmear->setArg(4, _voxSizeBuffer);
    _kernelBacksmear->setArg(5, _pMatBuffer);
    _kernelBacksmear->setArg(6, _bytesLocPMatMem, nullptr);
    _kernelBacksmear->setArg(7, _sliceBuffer);
    _kernelBacksmear->setArg(8, _projBuf->devImage());
}

/*!
 * \brief Sets the backprojection kernel to be used to \a backsmearKernel.
 */
void BackprojectorBase::setBacksmearKernel(cl::Kernel* backsmearKernel)
{
    _kernelBacksmear = backsmearKernel;
}

/*!
 * \brief Sets the slice index and view id argument of the backprojection kernel and starts it.
 */
void BackprojectorBase::startBacksmearKernel(uint slice, uint view)
{
    _kernelBacksmear->setArg(0, slice);
    _kernelBacksmear->setArg(1, view % _viewsPerBlock);

    _queue.enqueueNDRangeKernel(*_kernelBacksmear, cl::NullRange,
                                cl::NDRange(_nbVoxels[0], _nbVoxels[1]));
}

/*!
 * \brief Starts the nullifier kernel.
 */
void BackprojectorBase::startNullifierKernel()
{
    _queue.enqueueNDRangeKernel(*_kernelNullifier, cl::NullRange,
                                cl::NDRange(_nbVoxels[0] * _nbVoxels[1]));
}

/*!
 * \brief Writes (blocking) all data into the fixed (i.e. contant for the entire backprojection
 * operation) OpenCL buffers.
 */
void BackprojectorBase::writeFixedBuffers()
{
    float voxSize[3]   = { float(_voxelSize.get<0>()),
                           float(_voxelSize.get<1>()),
                           float(_voxelSize.get<2>()) };
    float volCorner[3] = { float(_corner.get<0>()),
                           float(_corner.get<1>()),
                           float(_corner.get<2>()) };

    _queue.enqueueWriteBuffer(_volCornerBuffer, true, 0, sizeof(float) * 3, volCorner);
    _queue.enqueueWriteBuffer(_voxSizeBuffer, true, 0, sizeof(float) * 3, voxSize);
}

/*!
 * \brief Writes (non-blocking) projection matrices into the corresponding OpenCL buffer.
 *
 * Matrix data is accessed for \a nbViews beginning at view index \a startView from the array
 * \a src. Note that \a src must point to the start of the projection matrices (view 0) and must be
 * large enough to cover all \a nbViews matrices that shall be transferred.
 */
void BackprojectorBase::transferProjectionMatrices(const float* src, uint startView, uint nbViews)
{
    _queue.enqueueWriteBuffer(_pMatBuffer, CL_FALSE, 0,
                              _bytesLocPMatMem * nbViews,
                              src + 12 * _nbModules * startView);
}

/*!
 * \brief Writes (non-blocking) data of slice \a z from \a targetVolume into the corresponding
 * OpenCL buffer.
 */
void BackprojectorBase::transferSlice(const VoxelVolume<float>& targetVolume, uint z)
{
    const auto pixelPerSlice = targetVolume.dimensions().x * targetVolume.dimensions().y;
    _queue.enqueueWriteBuffer(_sliceBuffer, CL_FALSE, 0,
                              sizeof(float) * pixelPerSlice,
                              targetVolume.rawData() + z * pixelPerSlice);
}

/*!
 * \brief Writes (non-blocking) projection data into the corresponding OpenCL buffer.
 *
 * Data is transferred for \a nbViews beginning at view index \a startView. Note that \a projections
 * must contain at least \a startView + \a nbViews views.
 */
void BackprojectorBase::transferProjectionData(const ProjectionDataView& projections,
                                               uint startView, uint nbViews)
{
    auto destPtr = _projBuf->hostPtr();
    const auto nbModules = projections.viewDimensions().nbModules;
    const auto elementsPerModule = projections.viewDimensions().nbChannels * projections.viewDimensions().nbRows;

    for(auto view = 0u; view < nbViews; ++view)
        for(auto module = 0u; module < nbModules; ++module)
        {
            const auto weight = weighting(startView + view, module);
            auto srcPtr = projections.view(startView + view).module(module).rawData();

            std::generate_n(destPtr, elementsPerModule, [&srcPtr, weight]() {
                return (*srcPtr++) * weight;
            });

            destPtr += elementsPerModule;
        }

    _projBuf->transferPinnedMemToDev(false);
}

/*!
 * \brief Computes how many views of data with dimensions given by \a projDim can be processed
 * within one block (i.e. without intermediate data transfers).
 *
 * The maximum number of views that can be processed is (potentially) limited by two aspects:
 * 1. maximum memory allocation size
 * 2. maximum (3D) image depth
 *
 * This estimates the largest possible number of views that satisfies both constraints of the OpenCL
 * device that is in use in this instance. Throws an \c std::runtime_error if not even one single
 * projection view can be handled.
 */
uint BackprojectorBase::viewsPerBlock(const ProjectionData::Dimensions& projDim)
{
    const auto& device = OCL::OpenCLConfig::instance().devices().front();
    auto maxMemAlloc = device.getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>();
    auto maxDim3 = device.getInfo<CL_DEVICE_IMAGE3D_MAX_DEPTH>();
    qDebug() << "maxMem: " << maxMemAlloc;
    qDebug() << "maxDim3: " << maxDim3;

    auto nbBlocks = 1u;
    auto viewsPerBlock = projDim.nbViews;
    size_t reqMemoryForProj, projBufferDim2;
    const auto bytesPerModule = sizeof(float) * projDim.nbChannels * projDim.nbRows;

    while(true)
    {
        // recalculate buffer specs
        projBufferDim2 = projDim.nbModules * viewsPerBlock; // total number of modules
        reqMemoryForProj = projBufferDim2 * bytesPerModule; // total req. memory (bytes)

        // check if buffer specs are compatible
        if(projBufferDim2 > maxDim3 || reqMemoryForProj > maxMemAlloc)
        {
            viewsPerBlock = static_cast<uint>(std::ceil(viewsPerBlock / 2.0));
            ++nbBlocks;

            if(viewsPerBlock <= 1u) // prevent endless loop
                throw std::runtime_error("BackprojectorBase: Unable to process dataset. "
                                         "Projections do not fit in cl::Image3D (too many modules)!");
        }
        else
            break;
    }

    qDebug() << "fullProjDim: " << projBufferDim2;
    qDebug() << "reqMemoryForProj: " << reqMemoryForProj;
    qDebug() << "reqNbBlocks: " << nbBlocks;

    return viewsPerBlock;
}

/*!
 * \brief QMetaEnum object used to decode/encode subset order enum values.
 */
QMetaEnum BackprojectorBase::metaEnum()
{
    const auto& mo = BackprojectorBase::staticMetaObject;
    const auto idx = mo.indexOfEnumerator("WeightingType");
    return mo.enumerator(idx);
}

/*!
 * \brief Computes (and returns) geometry factors for all modules in \a modPMats.
 *
 * Factors are scaled w.r.t. the volume of an individual voxel, given by \a volumeOfSingleVoxel.
 */
std::vector<float> BackprojectorBase::computeGeometryFactors(float volumeOfSingleVoxel,
                                                             const SingleViewGeometry& modPMats)
{
    auto ret = std::vector<float>(modPMats.nbModules());

    std::transform(modPMats.begin(), modPMats.end(), ret.begin(),
                   [volumeOfSingleVoxel](const mat::ProjectionMatrix& pMat) {
                       const auto K = pMat.intrinsicMatK();
                       return volumeOfSingleVoxel * static_cast<float>(K.get<0,0>() * K.get<1,1>());
                    });

    return ret;
}

/*!
 * \brief Pre-computes geometry factors for all projection matrices in \a pMats.
 *
 * Factors are scaled w.r.t. the volume of an individual voxel, given by \a volumeOfSingleVoxel.
 */
void BackprojectorBase::precomputeGeometryFactors(float volumeOfSingleVoxel,
                                                  const FullGeometry& pMats)
{
    _precompGeomFactors.resize(pMats.nbViews());

    std::transform(pMats.begin(), pMats.end(), _precompGeomFactors.begin(),
                   [volumeOfSingleVoxel](const SingleViewGeometry& modPMats) {
                       return computeGeometryFactors(volumeOfSingleVoxel, modPMats);
                   });
}

/*!
 * \brief Returns a (pre-computed) geometry factor for detector module \a module in view \a view.
 */
float BackprojectorBase::precomputedGeometryFactor(uint view, uint module) const
{
    Q_ASSERT(!_precompGeomFactors.empty());
    return _precompGeomFactors[view][module];
}

} // namespace OCL

namespace assist {

/*!
 * \brief Returns \c true if the specifications in \a setup are consistent with \a projections.
 *
 * In detail, this verifies that:
 * - \a setup is valid (see AcquisitionSetup::isValid()),
 * - the number of detector modules in \a setup is the same as in \a projections,
 * - the view dimensions of the detector in \a setup are the same as in \a projections, and
 * - \a projections contains no view id larger than the number of views in the configured setup.
 *
 * Returns \c false if any of the conditions is violated (also issues a corresponding qWarning
 * message).
 */
bool isConsistentPair(const AcquisitionSetup& setup, const ProjectionDataView& projections)
{
    if(!setup.isValid())
    {
        qCritical() << "assist::isConsistentPair: Inconsistent pair of setup and projections. "
                       "Reason: invalid AcquisitionSetup. Did you forget to call configure()?";
        return false;
    }

    if(projections.viewDimensions().nbModules != setup.system()->detector()->nbDetectorModules())
    {
        qCritical() << "assist::isConsistentPair: Inconsistent pair of setup and projections. "
                       "Reason: projection data view contains data with a different number of "
                       "modules than in the configured setup.";
        return false;
    }

    if(setup.system()->detector()->viewDimensions() != projections.viewDimensions())
    {
        qCritical() << "assist::isConsistentPair: Inconsistent pair of setup and projections. "
                       "Reason: projection data view contains data with a different projection size "
                       "(ie. number of pixels) than the detector component in the configured setup.";
        return false;
    }

    const auto nbViewsInSetup = setup.nbViews();

    if(std::any_of(projections.viewIds().cbegin(), projections.viewIds().cend(),
                   [nbViewsInSetup] (uint viewID) { return viewID >= nbViewsInSetup; } ))
    {
        qCritical() << "assist::isConsistentPair: Inconsistent pair of setup and projections. "
                       "Reason: projection data view contains a view ID that exceeds the number of "
                       "views in the configured setup.";
        return false;
    }

    return true;
}

} // namespace assist

} // namespace CTL

/*! \file */

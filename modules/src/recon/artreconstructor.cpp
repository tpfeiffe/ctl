#include "artreconstructor.h"
#include "regularizers.h"

#include "simplebackprojector.h"
#include "components/abstractdetector.h"
#include "img/spectralvolumedata.h"
#include "processing/errormetrics.h"
#include "projectors/raycasterprojector.h"
#include "projectors/spectraleffectsextension.h"
#include "projectors/sfpprojector.h"
#include "recon/sfpbackprojector.h"

#include <limits>
#include <QElapsedTimer>
#include <QMetaEnum>
#include <QTime>

namespace CTL {

static float computeProjectionNorm(const ProjectionDataView& projections);
static float computeBackprojectedProjectionNorm(const ProjectionDataView& projections,
                                                const AcquisitionSetup& setup,
                                                const VoxelVolume<float>::Dimensions& dimensions,
                                                const VoxelVolume<float>::VoxelSize& voxelSize);

DECLARE_SERIALIZABLE_TYPE(ARTReconstructor)

/*!
 * \brief Constructs an ARTReconstructor that uses \a forwardProjector and \a backprojector.
 *
 * The ARTReconstructor takes ownership of \a forwardProjector and \a backprojector.
 */
ARTReconstructor::ARTReconstructor(AbstractProjector* forwardProjector,
                                   AbstractReconstructor* backprojector)
    : _fp(forwardProjector)
    , _bp(backprojector)
    , _regularizer(new IdentityRegularizer)
    , _customSubsetGen(nullptr)
    , _defaultSubsetGen(new DefaultSubsetGenerator)
    , _activeSubsetGen(_defaultSubsetGen.get())
{
    if(!_fp)
        throw std::runtime_error("ARTReconstructor::setForwardProjector: Forward projector "
                                 "must not be nullptr.");
    if(!_bp)
        throw std::runtime_error("ARTReconstructor::setBackprojector: Backprojector "
                                 "must not be nullptr.");
}

/*!
 * \brief Constructs an ARTReconstructor that uses the default forward and backprojector.
 *
 * Forward and backprojector default to OCL::RayCasterProjector and OCL::SimpleBackprojector,
 * respectively.
 */
ARTReconstructor::ARTReconstructor()
    : ARTReconstructor(new OCL::RayCasterProjector, new OCL::SimpleBackprojector)
{
}

/*!
 * \brief Constructs an ARTReconstructor that uses \a forwardProjector and \a backprojector.
 */
ARTReconstructor::ARTReconstructor(std::unique_ptr<AbstractProjector> forwardProjector,
                                   std::unique_ptr<AbstractReconstructor> backprojector)
    : ARTReconstructor(forwardProjector.release(), backprojector.release())
{
}

/*!
 * \brief Reconstructs projection data \a projections into \a targetVolume and returns \c true
 * on success.
 *
 * Note that the AcquisitionSetup corresponding to \a projections must have been set previously with
 * configure(). The requirements on the setup (i.e. acquisition geometry) are described in the
 * detailed class description and in isApplicableTo().
 *
 * \a targetVolume serves as an initialization, which means that data reconstructed from
 * \a projections will be added to any values already present in \a targetVolume. If \a targetVolume
 * has no memory allocated when passed here, memory allocation will be performed as part of the
 * reconstruction and the volume is initialized with zero.
 *
 * A rough breakdown of the reconstruction routine can be found in reconstructData().
 */
bool ARTReconstructor::reconstructToPlain(const ProjectionDataView& projections,
                                          VoxelVolume<float>& targetVolume)
{
    qInfo() << "Start iterative reconstruction (plain volume).";

    // consistency checks
    if(!consistencyChecks(projections))
        return false;

    SpectralVolumeData recVol(std::move(targetVolume));
    bool recoOk = reconstructData(projections, recVol, *_fp, *_bp);
    targetVolume = std::move(recVol);

    return recoOk;
}

/*!
 * \brief Reconstructs projection data \a projections into \a targetVolume and returns \c true
 * on success.
 *
 * This implementation provides a means to perform a reconstruction considering the polyenergetic
 * spectrum of the system defined in the configured acquisition setup. For that purpose, it uses
 * the material information provided in \a targetVolume, i.e. it reconstructs a volume consisting
 * of the material that is specified in that volume. To enable polyenergetic simulation within the
 * iterative scheme, the forward projector of this instance will be wrapped in a
 * SpectralEffectsExtension. Consequently, please do not use a forward projector that already
 * includes this extension as a custom forward projector in this context.
 *
 * If \a targetVolume is passed with values in density domain (see
 * SpectralVolumeData::isDensityVolume()), it will be transformed to attenuation domain at a
 * reference energy of 50 keV before it is passed to the actual reconstruction routine. In that
 * case, it will be transformed back to density domain at the end of the reconstruction process.
 *
 * Please also check reconstructToPlain() for some more detail on the procedures.
 */
bool ARTReconstructor::reconstructToSpectral(const ProjectionDataView& projections,
                                             SpectralVolumeData& targetVolume)
{
    qInfo() << "Start iterative reconstruction (spectral).";

    // consistency checks
    if(!consistencyChecks(projections))
        return false;

    // return value
    bool recoOk;

    // transform representation of volume data to attenuation domain
    const auto wasDensity = targetVolume.isDensityVolume();
    if(wasDensity)
    {
        constexpr auto refEnergy = 50.0f;
        targetVolume.transformToAttenuationCoeff(refEnergy);
    }

    // decorate forward projector with SpectralEffectsExtension (temporarily takes over ownership)
    SpectralEffectsExtension fp{ _fp.get() };
    try
    {
        recoOk = reconstructData(projections, targetVolume, fp, *_bp);
    } catch(...)
    {
        fp.release();
        throw;
    }
    fp.release();

    // transform back to original domain if required
    if(wasDensity)
        targetVolume.transformToDensity();

    return recoOk;
}

// actual reconstruction routine
/*!
 * \brief Performs the iterative reconstruction of \a projections into \a targetVolume using \a fp
 * and \a bp as forward and backprojector, respectively.
 *
 * Workflow: Assume a forward projector \c  FP, backprojector \c  BP, subset generator \c  SubGen, a
 * regularizer \c Regul, and an initial volume \c estimate
 *
 * \code
 *  if useRelaxEstimate then relax = computeRelaxationEstimate
 *
 *  it = 0
 *  do
 *       subsets = SubGen.generateSubsets(it)
 *
 *       foreach subset in subsets
 *          configure FP for views in subset
 *
 *          simProj = FP.project(estimate)
 *          diff    = relax * (simProj - subset)
 *
 *          estimate = BP.reconstruct(diff, estimate)
 *
 *          if useRegularization then Regul.filter(estimate)
 *          if usePositivity     then apply positivity constraint
 *
 *       end foreach
 *
 *       ++it
 *
 *  while (!stoppingCriterionReached)
 * \endcode
 */
bool ARTReconstructor::reconstructData(const ProjectionDataView& projections,
                                       SpectralVolumeData& targetVolume,
                                       AbstractProjector& fp,
                                       AbstractReconstructor& bp)
{
    QElapsedTimer timer;
    timer.start();

    if(!targetVolume.hasData())
        targetVolume.fill(0.0f);

    _activeSubsetGen->setData(projections, _setup);

    // estimate relaxation if required
    if(_useRelaxationEstimation)
        _relax = computeRelaxationEstimate(_setup, targetVolume, *_fp, *_bp, 1u);

    // stopping criteria preparations
    const auto inputProjNorm = isStoppingCriterionEnabled(RelativeProjectionError)
        ? computeProjectionNorm(projections)
        : 0.0f;

    const auto normalEqNormalization = isStoppingCriterionEnabled(NormalEquationSatisfied)
        ? computeBackprojectedProjectionNorm(projections, _setup, targetVolume.dimensions(), targetVolume.voxelSize())
          * (_relax / _setup.nbViews())
        : 0.0f;

    SpectralVolumeData oldVol(VoxelVolume<float>{ 0, 0, 0 });
    float curProjError = std::numeric_limits<float>::max();

    // start main iteration loop
    for(uint it = 0; ; ++it)
    {
        qDebug() << "Iteration " << it;
        if(_stoppingCriteria & MaximumNbIterations)
            Q_EMIT notifier()->progress(qreal(it) / _maxNbIterations);

        // create subsets for current iteration
        const auto subsets = _activeSubsetGen->generateSubsets(it);

        // error metrics for stopping criteria
        if(isStoppingCriterionEnabled(VolumeDomainChange | NormalEquationSatisfied))
            oldVol = targetVolume;
        float oldProjError = curProjError;
        curProjError = 0.0f;

        for(const auto& subsetData : subsets)
        {
            qDebug() << "subset: " << subsetData.viewIds();

            const auto subsetSetup = _setup.subset(subsetData.viewIds());

            // forward projection
            const auto proj = fp.configureAndProject(subsetSetup, targetVolume);
            auto projError = subsetData - proj;

            if(isStoppingCriterionEnabled(ProjectionErrorChange | RelativeProjectionError))
                curProjError += std::inner_product(projError.cbegin(), projError.cend(),
                                                   projError.cbegin(), 0.0f);

            // backprojection
            projError *= _relax / subsetData.nbViews();
            targetVolume = bp.configureAndReconstruct<VoxelVolume<float>>(subsetSetup,
                                                                          projError,
                                                                          std::move(targetVolume));

            // regularization
            if(_useRegularization)
                _regularizer->filter(targetVolume);

            // positivity constraint
            if(_usePositivityConstraint)
                assist::enforcePositivity(targetVolume);
        }
        curProjError = std::sqrt(curProjError);

        if(stoppingCriterionReached(oldVol, targetVolume, oldProjError, curProjError, inputProjNorm,
                                    normalEqNormalization * subsets.size(), timer.elapsed(), it))
            break;
    }

    qDebug() << "IterativeReconstruction: Total time for reconstruction:"
             << QTime(0,0).addMSecs(timer.elapsed()).toString(QStringLiteral("hh:mm:ss"));

    return true;
}

/*!
 * \brief Sets the maximum number of iterations to \a nbIt and enables the corresponding stopping
 * criterion if \a enableCriterion = \c true.
 */
void ARTReconstructor::setMaxNbIterations(uint nbIt, bool enableCriterion)
{
    _maxNbIterations = nbIt;

    setStoppingCriterionEnabled(ARTReconstructor::MaximumNbIterations, enableCriterion);
}

/*!
 * \brief Sets the maximum runtime for the reconstruction to \a seconds and enables the
 * corresponding stopping criterion if \a enableCriterion = \c true.
 */
void ARTReconstructor::setMaxTime(float seconds, bool enableCriterion)
{
    _terminateMaxTime = static_cast<uint>(seconds * 1000.0f);

    setStoppingCriterionEnabled(ARTReconstructor::MaximumTime, enableCriterion);
}

/*!
 * \brief Sets the minimum (relative) change in volume domain to \a minRelativeChange and enables
 * the corresponding stopping criterion if \a enableCriterion = \c true.
 */
void ARTReconstructor::setMinChangeInVolumeDomain(float minRelativeChange, bool enableCriterion)
{
    _terminateVolChange = minRelativeChange;

    setStoppingCriterionEnabled(ARTReconstructor::VolumeDomainChange, enableCriterion);
}

/*!
 * \brief Sets the minimum (relative) change in projection domain to \a minRelativeChange and
 * enables the corresponding stopping criterion if \a enableCriterion = \c true.
 */
void ARTReconstructor::setMinChangeInProjectionError(float minRelativeChange, bool enableCriterion)
{
    _terminateProjErrChange = minRelativeChange;

    setStoppingCriterionEnabled(ARTReconstructor::ProjectionErrorChange, enableCriterion);
}

/*!
 * \brief Sets the minimum (relative) projection error to \a minRelativeError and enables the
 * corresponding stopping criterion if \a enableCriterion = \c true.
 */
void ARTReconstructor::setMinRelativeProjectionError(float minRelativeError, bool enableCriterion)
{
    _terminateProjErrRel = minRelativeError;

    setStoppingCriterionEnabled(ARTReconstructor::RelativeProjectionError, enableCriterion);
}

/*!
 * \brief Sets the minimum (relative) tolerance w.r.t the normal equation to \a relativeTol and
 * enables the corresponding stopping criterion if \a enableCriterion = \c true.
 */
void ARTReconstructor::setNormalEqTolerance(float relativeTol, bool enableCriterion)
{
    _terminateNormalEqTol = relativeTol;

    setStoppingCriterionEnabled(ARTReconstructor::NormalEquationSatisfied, enableCriterion);
}

/*!
 * \brief Sets the use of the positivity constraint to \a enabled.
 *
 * If enabled, the positivity constraint is applied at the end of each iteration and sets all values
 * in the current estimate that are negative to the value 0.
 */
void ARTReconstructor::setPositivityConstraintEnabled(bool enabled)
{
    _usePositivityConstraint = enabled;
}

/*!
 * \brief Sets the use of the regularizer to \a enabled.
 *
 * If enabled, the regularizer is applied to the current volume estimate at the end of each
 * iteration. The regularizer can be set through setRegularizer().
 */
void ARTReconstructor::setRegularizationEnabled(bool enabled)
{
    _useRegularization = enabled;
}

/*!
 * \brief Sets the use of an estimation for the relaxation parameter to \a enabled.
 *
 * If enabled, an estimation for the relaxation parameter is computed at the beginning of a
 * reconstruction by means of computeRelaxationEstimate().
 */
void ARTReconstructor::setRelaxationEstimationEnabled(bool enabled)
{
    _useRelaxationEstimation = enabled;
}

/*!
 * \brief Sets the relaxation parameter to \a relax.
 *
 * This also disables the use of an estimation for the relaxation parameter. To re-enable it, please
 * see setRelaxationEstimationEnabled().
 */
void ARTReconstructor::setRelaxation(float relax)
{
    _relax = relax;
    _useRelaxationEstimation = false;
}

/*!
 * \brief Computes an estimation for the relaxation parameter and sets it for this instance.
 *
 * The relaxation parameter will be computed based on the currently configured acquisition setup
 * (see configure()) and the dimensions of \a targetVolume using \a nbPowerIter iterations of the
 * power method.
 */
void ARTReconstructor::setRelaxationByEstimation(const VoxelVolume<float>& targetVolume,
                                                 uint nbPowerIter)
{
    if(!_setup.isValid())
        throw std::runtime_error("ARTReconstructor::computeRelaxationEstimate():"
                                 "Estimation aborted. Reason: invalid AcquisitionSetup. "
                                 "Did you forget to call configure()?");

    setRelaxationByEstimation(_setup, targetVolume, nbPowerIter);
}

/*!
 * \brief Computes an estimation for the relaxation parameter and sets it for this instance.
 *
 * The relaxation parameter will be computed based on the acquisition setup \a setup and the
 * dimensions of \a targetVolume using \a nbPowerIter iterations of the power method.
 *
 * Note that this does not configure the reconstructor with the passed \a setup. Use configure()
 * separately for this purpose.
 */
void ARTReconstructor::setRelaxationByEstimation(const AcquisitionSetup& setup,
                                                 const VoxelVolume<float>& targetVolume,
                                                 uint nbPowerIter)
{
    const auto relax
        = computeRelaxationEstimate(setup, targetVolume, *_fp, *_bp, nbPowerIter);
    qDebug() << "ARTReconstructor: estimated relaxation parameter:" << relax;

    setRelaxation(relax);
}

/*!
 * \brief Sets the regularizer to \a regularizer and enables regularization if
 * \a enableRegularization = \c true.
 *
 * This instance takes ownership of \a regularizer.
 */
void ARTReconstructor::setRegularizer(AbstractVolumeFilter* regularizer,
                                      bool enableRegularization)
{
    setRegularizer(std::unique_ptr<AbstractVolumeFilter>(regularizer), enableRegularization);
}

/*!
 * \brief Sets the regularizer to \a regularizer and enables regularization if
 * \a enableRegularization = \c true.
 */
void ARTReconstructor::setRegularizer(std::unique_ptr<AbstractVolumeFilter> regularizer,
                                      bool enableRegularization)
{
    if(!regularizer)
        throw std::runtime_error("ARTReconstructor::setRegularizer: "
                                 "Regularizer must not be nullptr.");

    _regularizer = std::move(regularizer);
    setRegularizationEnabled(enableRegularization);
}

/*!
 * \brief Returns \c true if the default subset generator is in use.
 */
bool ARTReconstructor::isDefaultSubsetGeneratorInUse() const
{
    return _activeSubsetGen == _defaultSubsetGen.get();
}

/*!
 * \brief Sets the stopping criteria to be checked after each iteration to \a criteria.
 *
 * \a criteria may be a combination of multiple stopping criteria flags.
 */
void ARTReconstructor::setStoppingCriteria(int criteria)
{
    _stoppingCriteria = criteria;
}

/*!
 * \brief Sets the use of the stopping criterion \a criterion to \a enabled.
 *
 * Note: does not influence the status of other stopping criteria than \a criterion.
 */
void ARTReconstructor::setStoppingCriterionEnabled(ARTReconstructor::StoppingCriterion criterion,
                                                   bool enabled)
{
    if(enabled)
        setStoppingCriteria(_stoppingCriteria | criterion);
    else
        setStoppingCriteria(_stoppingCriteria & ~criterion);
}

/*!
 * \brief Returns a reference to the default subset generator of this instance.
 *
 * Use this reference to change settings of the default subset generator.
 *
 * Please note that using this method (and changing settings) does not automatically imply that the
 * default subset generator will be used for subset generation. If a custom generator has been set
 * previously, this might still be active. Re-enable the use of the default generator with
 * useDefaultSubsetGenerator() if desired. You may check whether or not the default generator is
 * currently used with isDefaultSubsetGeneratorInUse().
 */
DefaultSubsetGenerator& ARTReconstructor::defaultSubsetGenerator() const
{
    return *_defaultSubsetGen;
}

/*!
 * \brief Returns \c true if a custom subset generator is currently used to create subsets.
 */
bool ARTReconstructor::isCustomSubsetGeneratorInUse() const
{
    return !isDefaultSubsetGeneratorInUse();
}

/*!
 * \brief Sets the subset generator to be used for subset creation to \a generator.
 *
 * This instance takes ownership of \a generator. This replaces any previously set generator.
 * Automatically enables use of the passed generator for subset creation.
 * To re-enable the use of the default generator, call useDefaultSubsetGenerator() if desired.
 * \a generator must not be \c nullptr; throws an \c std::runtime_error otherwise.
 */
void ARTReconstructor::setSubsetGenerator(AbstractSubsetGenerator* generator)
{
    setSubsetGenerator(std::unique_ptr<AbstractSubsetGenerator>(generator));
}

/*!
 * \brief Sets the subset generator to be used for subset creation to \a generator.
 *
 * This replaces any previously set generator.
 * Automatically enables use of the passed generator for subset creation.
 * To re-enable the use of the default generator, call useDefaultSubsetGenerator() if desired.
 * \a generator must not be \c nullptr; throws an \c std::runtime_error otherwise.
 */
void ARTReconstructor::setSubsetGenerator(std::unique_ptr<AbstractSubsetGenerator> generator)
{
    if(!generator)
        throw std::runtime_error("ARTReconstructor::setSubsetGenerator: "
                                 "Generator must not be nullptr.");

    _customSubsetGen = std::move(generator);
    _activeSubsetGen = _customSubsetGen.get();
}

/*!
 * \brief Sets the forward projector to be used to \a projector.
 *
 * This replaces any previously set projector.
 * \a projector must not be \c nullptr; throws an \c std::runtime_error otherwise.
 */
void ARTReconstructor::setForwardProjector(std::unique_ptr<AbstractProjector> projector)
{
    setForwardProjector(projector.release());
}

/*!
 * \brief Sets the backprojector to be used to \a projector.
 *
 * This replaces any previously set backprojector.
 * \a projector must not be \c nullptr; throws an \c std::runtime_error otherwise.
 */
void ARTReconstructor::setBackprojector(std::unique_ptr<AbstractReconstructor> projector)
{
    setBackprojector(projector.release());
}

/*!
 * \brief Sets the forward projector to be used to \a projector.
 *
 * This instance takes ownership of \a projector. This replaces any previously set projector.
 * \a projector must not be \c nullptr; throws an \c std::runtime_error otherwise.
 */
void ARTReconstructor::setForwardProjector(AbstractProjector* projector)
{
    if(!projector)
        throw std::runtime_error("ARTReconstructor::setForwardProjector: Forward projector "
                                 "must not be nullptr.");
    _fp.reset(projector);
}

/*!
 * \brief Sets the backprojector to be used to \a projector.
 *
 * This instance takes ownership of \a projector. This replaces any previously set backprojector.
 * \a projector must not be \c nullptr; throws an \c std::runtime_error otherwise.
 */
void ARTReconstructor::setBackprojector(AbstractReconstructor* projector)
{
    if(!projector)
        throw std::runtime_error("ARTReconstructor::setBackprojector: Backprojector "
                                 "must not be nullptr.");
    _bp.reset(projector);
}

/*!
 * \brief Enables the use of the (previously set) custom subset generator.
 *
 * Note that a valid (i.e. no \c nullptr) custom subset generator must be set in advance (see
 * setSubsetGenerator()); throws an \c std::runtime_error otherwise.
 */
void ARTReconstructor::useCustomSubsetGenerator()
{
    if(!_customSubsetGen)
        throw std::runtime_error("ARTReconstructor::useCustomSubsetGenerator: "
                                 "Custom subset generator not set (nullptr).");
    _activeSubsetGen = _customSubsetGen.get();
}

/*!
 * \brief (Re-)Enables the use of the default subset generator.
 *
 * Note that by default, the default subset generator is in use automatically. A call to this
 * method should therefore only become necessary if a custom subset generator has been set, and the
 * default subset generator shall be re-enabled.
 */
void ARTReconstructor::useDefaultSubsetGenerator()
{
    _activeSubsetGen = _defaultSubsetGen.get();
}

/*!
 * \brief Returns \c true if the positivity constraint is enabled.
 */
bool ARTReconstructor::isPositivityConstraintEnabled() const
{
    return _usePositivityConstraint;
}

/*!
 * \brief Returns \c true if automatic estimation of a suitable relaxation parameter is enabled.
 */
bool ARTReconstructor::isRelaxationEstimationEnabled() const
{
    return _useRelaxationEstimation;
}

/*!
 * \brief Returns \c true if use of the regularizer is enabled.
 */
bool ARTReconstructor::isRegularizationEnabled() const
{
    return _useRegularization;
}

/*!
 * \brief Returns \c true if the stopping criterion \a criteria is enabled.
 *
 * \a criteria may be a combination of multiple stopping criteria flags. In that case, \c true is
 * returned if any of the flags that are part of \a criteria is enabled.
 */
bool ARTReconstructor::isStoppingCriterionEnabled(int criteria) const
{
    return (_stoppingCriteria & criteria) != 0;
}

/*!
 * \brief Returns the relaxation parameter.
 *
 * This method can be used, e.g. to query the relaxation parameter after automatic estimation.
 */
float ARTReconstructor::relaxation() const
{
    return _relax;
}

/*!
 * \brief Returns the combination of stopping criteria flags that are currently enabled.
 */
int ARTReconstructor::stoppingCriteria() const
{
    return _stoppingCriteria;
}

/*!
 * \brief Returns a reference to the currently set regularizer.
 *
 * By default (i.e. if no custom regularizer has been set; see setRegularizer()) this is a
 * IdentityRegularizer, which has no effect when applied to volume data.
 */
AbstractVolumeFilter& ARTReconstructor::regularizer() const
{
    return *_regularizer;
}

/*!
 * \brief Returns a reference to the current custom subset generator of this instance.
 *
 * This requires that a valid (i.e. no \c nullptr) custom subset generator has been set.
 *
 * Use this reference to change settings of the custom subset generator.
 *
 * Please note that using this method (and changing settings) does not automatically imply that the
 * custom subset generator will be used for subset generation. After setting a custom subset
 * generator, its use will automatically be enabled. However, if the default generator has been
 * re-enable manually since then, you need to switch back to the custom generator using
 * useCustomSubsetGenerator() if desired. You may check whether or not the custom generator is
 * currently used with isCustomSubsetGeneratorInUse().
 */
AbstractSubsetGenerator& ARTReconstructor::customSubsetGenerator() const
{
    if(!_customSubsetGen)
        throw std::runtime_error("ARTReconstructor::customSubsetGenerator: "
                                 "Custom subset generator not set (nullptr).");
    return *_customSubsetGen;
}

/*!
 * \brief Returns a reference to the subset generator that is currently enabled.
 *
 * Use this reference to change settings of the currently enabled subset generator.
 *
 * See also customSubsetGenerator() and defaultSubsetGenerator().
 */
AbstractSubsetGenerator& ARTReconstructor::subsetGenerator() const
{
    return *_activeSubsetGen;
}

/*!
 * \brief Computes an estimation for a suitable relaxation parameter using the power method.
 *
 * If \a forwardProjector or \a backprojector is \c nullptr, default projectors will be used.
 * These are OCL::RayCasterProjector and OCL::SimpleBackprojector, respectively.
 */
float ARTReconstructor::computeRelaxationEstimate(const AcquisitionSetup& setup,
                                                  const VoxelVolume<float>& targetVolume,
                                                  std::shared_ptr<AbstractProjector> forwardProjector,
                                                  std::shared_ptr<AbstractReconstructor> backprojector,
                                                  uint nbPowerIter)
{
    return computeRelaxationEstimate(setup, targetVolume,
                                     forwardProjector.get(), backprojector.get(), nbPowerIter);
}

/*!
 * \brief Computes an estimation for a suitable relaxation parameter using the power method.
 *
 * If \a forwardProjector or \a backprojector is \c nullptr, default projectors will be used.
 * These are OCL::RayCasterProjector and OCL::SimpleBackprojector, respectively.
 */
float ARTReconstructor::computeRelaxationEstimate(const AcquisitionSetup& setup,
                                                  const VoxelVolume<float>& targetVolume,
                                                  AbstractProjector* forwardProjector,
                                                  AbstractReconstructor* backprojector,
                                                  uint nbPowerIter)
{
    auto fp = forwardProjector;
    auto bp = backprojector;

    std::unique_ptr<AbstractProjector> defaultFP;
    std::unique_ptr<AbstractReconstructor> defaultBP;
    if(!fp)
    {
        defaultFP = makeProjector<OCL::RayCasterProjector>();
        fp = defaultFP.get();
    }
    if(!bp)
    {
        defaultBP = makeReconstructor<OCL::SimpleBackprojector>();
        bp = defaultBP.get();
    }

    return computeRelaxationEstimate(setup, targetVolume, *fp, *bp, nbPowerIter);
}

/*!
 * Performs the power method in order to estimate and return an appropriate relaxation parameter
 * for the iterative reconstruction
 *
 * \f$x\mapsto x-\omega A^{T}(Ax-b)\f$
 *
 * Note that \f$\omega\f$ is not returned directly. Instead, the product with `nbViews` of the
 * \a `setup` is returned, i.e. the returned values is `relax`\f$=\omega*\f$`nbViews`.
 * This allows using the returned value `relax` also for an estimate of the relaxation parameter
 * in (ordered) subsets of the \a setup by computing `relax`/`nbViewsInSubset`.
 * The forward operator \f$A\f$ is specified by \a fp and \f$A^{T}\f$ by \a bp.
 *
 * The power method itself approximates the largest eigenvalue \f$\mu\f$ of \f$A^{T}A\f$ in an
 * iterative scheme. This eigenvalue \f$\mu\f$ is used to estimate the relaxation parameter as
 * \f$\omega=1/\mu\f$.
 * To obtain \f$\mu\f$, the eigenvector \f$v\f$ of \f$A^{T}A\f$ that corresponds to \f$\mu\f$ is
 * approximated.
 * An initial guess for this (normalized) eigenvector \f$v\f$ is set as a constant vector
 * (all entries are equal). Then \f$A^{T}A\f$ is applied to this vector \a nbPowerIter times
 * (with subsequent normalization in each iteration). If the procedure has converged, the
 * eigenvalue can be computed using the norm of \f$A^{T}Av\f$ (since \f$A^{T}Av=\mu v\f$).
 *
 * For a common CT \a setup, \a nbPowerIter = 1 is already sufficient to obtain an appropriate
 * estimate for the relaxation parameter. However, there may be cases, such as projection
 * truncation, where the initial guess for the eigenvector \f$v\f$ is insufficient so that more
 * than one iteration is required.
 *
 */
float ARTReconstructor::computeRelaxationEstimate(const AcquisitionSetup& setup,
                                                  const VoxelVolume<float>& targetVolume,
                                                  AbstractProjector& fp,
                                                  AbstractReconstructor& bp,
                                                  uint nbPowerIter)
{
    if(!setup.isValid())
        throw std::runtime_error("ARTReconstructor::computeRelaxationEstimate():"
                                 "Estimation aborted. Reason: invalid AcquisitionSetup.");

    auto ret = 1.0e-6f;

    VoxelVolume<float> unitVol(targetVolume.dimensions(), targetVolume.voxelSize());
    unitVol.fill(1.0f / std::sqrt(float(targetVolume.totalVoxelCount())));
    auto norm = 1.0f;

    for(auto iter = 0u; iter < nbPowerIter; ++iter)
    {
        // forward and back project
        unitVol = bp.configureAndReconstruct(setup, ProjectionDataView::dangling(fp.configureAndProject(setup, unitVol)),
                  VoxelVolume<float>{ targetVolume.dimensions(), targetVolume.voxelSize() });

        // compute norm with double precision
        norm = float(std::sqrt(std::inner_product(unitVol.cbegin(), unitVol.cend(),
                                                  unitVol.cbegin(), 0.0)));

        // normalize
        unitVol *= 1.0f / norm;

        // relaxation parameter (times view number)
        ret = setup.nbViews() / norm;
        qDebug() << "power method iteration" << iter + 1 << "gives relaxation parameter:" << ret;
    }

    return ret;
}

/*!
 * \brief QMetaEnum object used to decode/encode subset order enum values.
 */
QMetaEnum ARTReconstructor::metaEnum()
{
    const auto& mo = ARTReconstructor::staticMetaObject;
    const auto idx = mo.indexOfEnumerator("StoppingCriteria");
    return mo.enumerator(idx);
}

/*!
 * \brief Returns \c true if \a projections are consistent with the AcquisitionSetup previously
 * passed to configure().
 *
 * In particular, this validates whether:
 * - The configured setup is valid (see AcquisitionSetup::isValid()),
 * - the number of detector modules in \a projections is the same as in the configured setup
 * - \a projections contains no view id larger than the number of views in the configured setup.
 *
 * Returns \c true is all abovementioned criteria are satisfied.
 * Returns \c false if any of them is not fulfilled; note that criteria are checked in the order
 * they are listed above. Issues a warning message if a criterion is not fulfilled and returns
 * immediately afterwards, i.e. criteria appearing later in the list will not be checked.
 *
 * Additionally, this also checks whether at least one stopping criterion is enabled (see
 * hasAnyStoppingCriterion()). Issues a warning otherwise, but still returns \c true if all other
 * checks were successful.
 */
bool ARTReconstructor::consistencyChecks(const ProjectionDataView& projections) const
{
    const auto nbViewsInSetup = _setup.nbViews();
    const auto nbModules = projections.viewDimensions().nbModules;
    const auto& viewIds = projections.viewIds();

    if(!_setup.isValid())
    {
        qCritical() << "ARTReconstructor: Reconstruction aborted. "
                       "Reason: invalid AcquisitionSetup. Did you forget to call configure()?";
        return false;
    }
    if(nbModules != _setup.system()->detector()->nbDetectorModules())
    {
        qCritical() << "ARTReconstructor: Reconstruction aborted. "
                       "Reason: projection data view contains data with a different number of "
                       "modules than in the configured setup.";
        return false;
    }
    if(std::any_of(viewIds.cbegin(), viewIds.cend(),
                   [nbViewsInSetup] (uint viewID) { return viewID >= nbViewsInSetup; } ))
    {
        qCritical() << "ARTReconstructor: Reconstruction aborted. "
                       "Reason: projection data view contains a view ID that exceeds the number of "
                       "views in the configured setup.";
        return false;
    }
    if(!hasAnyStoppingCriterion())
    {
        qWarning() << "ARTReconstructor: No stopping criterion set. "
                      "Reconstruction process will not terminate.";
    }

    return true;
}

/*!
 * \brief Returns \c true if any of the available stopping criteria is enabled.
 *
 * Note that if this returns \c false, any subsequent call to a reconstruction method will not
 * terminate.
 */
bool ARTReconstructor::hasAnyStoppingCriterion() const
{
    return (_stoppingCriteria & ARTReconstructor::AllStoppingCriteria) != 0;
}

/*!
 * \brief Returns \c true if \a newVol and \a oldVol differ by less than the corresponding
 * stopping criterion value.
 *
 * This compares the relative L2 norm of \a oldVol and \a newVol with the value set through
 * setMinChangeInVolumeDomain().
 */
bool ARTReconstructor::terminateByVolumeChange(const VoxelVolume<float>& oldVol,
                                               const VoxelVolume<float>& newVol) const
{
    const auto change = static_cast<float>(metric::rL2(oldVol, newVol));
    qDebug() << "VolumeDomainChange:" << change << "| threshold:" << _terminateVolChange;

    if(change < _terminateVolChange)
    {
        qInfo() << "ARTReconstructor: Terminated due to volume change below "
                   "tolerance level (change: " + QString::number(change) + " | "
                   "threshold: " + QString::number(_terminateVolChange) + ").";
        return true;
    }

    return false;
}

/*!
 * Approximatively evaluates whether the normal equation
 * \f$A^{T}(Ax-b)=0\f$
 * is satisfied relative to a reference norm, which is the norm of backprojected projections
 * \f$A^{T}b\f$, where \f$b\f$ are the input projections and \f$A\f$ the system matrix.
 * This is implemented by keeping track of the change in the volume domain after each iteration.
 * For the particular case of having only one subset including all projections, it checks whether
 * \f$\frac{\left\Vert A^{T}(Ax-b)\right\Vert }{\left\Vert A^{T}b\right\Vert }<\text{tol}\f$
 * is satisfied.
 *
 * Note that the difference to the criterion `VolumeDomainChange` is that the norm between the
 * current and old volume will be devided by the relaxation parameter and thus, this measure is
 * independent from the relaxation, whereas `VolumeDomainChange` is not. Likewise, a division by
 * the number of subsets is performed in order to have a criterion independent from the used ordered
 * subset scheme.
 */
bool ARTReconstructor::terminateByNormalEqTol(const VoxelVolume<float>& oldVol,
                                              const VoxelVolume<float>& newVol,
                                              float normalization) const
{
    const auto change = static_cast<float>(metric::L2(oldVol, newVol)) / normalization;
    qDebug() << "NormalEquationSatisfied:" << change << "| threshold:" << _terminateNormalEqTol;

    if(change < _terminateNormalEqTol)
    {
        qInfo() << "ARTReconstructor: Terminated due satisfaction of the normal equation "
                   "(relative error: " + QString::number(change) + " | "
                   "threshold: " + QString::number(_terminateNormalEqTol) + ").";
        return true;
    }

    return false;
}

/*!
 * \brief Returns \c true if the relative difference between \a oldProjError and \a newProjError is
 * less than the corresponding stopping criterion value.
 *
 * This compares (\a oldProjError - \a newProjError) / \a oldProjError against the value set
 * through setMinChangeInProjectionError().
 */
bool ARTReconstructor::terminateByProjErrorChange(float oldProjError, float newProjError) const
{
    const auto change = std::abs(newProjError - oldProjError) / oldProjError;
    qDebug() << "ProjectionErrorChange:" << change << "| threshold:" << _terminateProjErrChange;

    if(change < _terminateProjErrChange)
    {
        qInfo() << "ARTReconstructor: Terminated due to change in projection error below "
                   "tolerance level (change: " + QString::number(change) + " | "
                   "threshold: " + QString::number(_terminateProjErrChange) + ").";
        return true;
    }

    return false;
}

/*!
 * \brief Returns \c true if \a msSpent is greater than the corresponding stopping criterion value.
 *
 * This compares against the value set through setMaxTime().
 */
bool ARTReconstructor::terminateByComputationTime(int msSpent) const
{
    qDebug() << "time spent:" << msSpent << "| threshold:" << _terminateMaxTime;

    if(uint(msSpent) > _terminateMaxTime)
    {
        qInfo() << "ARTReconstructor: Terminated due to maximum computation time reached "
                   "(time: " + QString::number(msSpent) + "ms | "
                   "threshold: " + QString::number(_terminateMaxTime) + "ms).";
        return true;
    }

    return false;
}

/*!
 * \brief Returns \c true if \a iteration is greater or equal to the corresponding stopping
 * criterion value.
 *
 * This compares against the value set through setMaxNbIterations().
 */
bool ARTReconstructor::terminateByIterationCount(uint iteration) const
{
    qDebug() << "iteration count:" << iteration + 1 << "| threshold:" << _maxNbIterations;

    if(iteration + 1 >= _maxNbIterations)
    {
        qInfo() << "ARTReconstructor: Terminated due to maximum number of iterations reached "
                   "(threshold: " + QString::number(_maxNbIterations) + ").";
        return true;
    }

    return false;
}

/*!
 * \brief Returns \c true if the ratio between \a projError and \a inputProjNorm is less than the
 * corresponding stopping criterion value.
 *
 * This compares against the value set through setMinRelativeProjectionError().
 */
bool ARTReconstructor::terminateByProjError(float projError, float inputProjNorm) const
{
    const auto relError = projError / inputProjNorm;
    qDebug() << "RelativeProjectionError:" << relError << "| threshold:" << _terminateProjErrRel;

    if(relError < _terminateProjErrRel)
    {
        qInfo() << "ARTReconstructor: Terminated due relative projection error below "
                   "tolerance level (rel. error: " + QString::number(relError) + " | "
                   "threshold: " + QString::number(_terminateProjErrRel) + ").";
        return true;
    }

    return false;
}

/*!
 * \brief Returns \c true if any of the enabled stopping criteria is hit.
 */
bool ARTReconstructor::stoppingCriterionReached(const VoxelVolume<float>& oldVol,
                                                const VoxelVolume<float>& newVol,
                                                float oldProjError,
                                                float newProjError,
                                                float inputProjNorm,
                                                float normalEqNormalization,
                                                int msSpent,
                                                uint iteration) const
{
    if(_stoppingCriteria & ARTReconstructor::MaximumNbIterations)
        if(terminateByIterationCount(iteration))
            return true;
    if(_stoppingCriteria & ARTReconstructor::MaximumTime)
        if(terminateByComputationTime(msSpent))
            return true;
    if(_stoppingCriteria & ARTReconstructor::ProjectionErrorChange)
        if(terminateByProjErrorChange(oldProjError, newProjError))
            return true;
    if(_stoppingCriteria & ARTReconstructor::VolumeDomainChange)
        if(terminateByVolumeChange(oldVol, newVol))
            return true;
    if(_stoppingCriteria & ARTReconstructor::RelativeProjectionError)
        if(terminateByProjError(newProjError, inputProjNorm))
            return true;
    if(_stoppingCriteria & ARTReconstructor::NormalEquationSatisfied)
        if(terminateByNormalEqTol(oldVol, newVol, normalEqNormalization))
            return true;

    return false;
}

/*!
 * \brief Passes the AcquisitionSetup that describes the setting in which projections that shall
 * be reconstructed have been acquired.
 *
 * This takes a copy of \a setup to make it available during the time of reconstruction. Any
 * reconstruction performed after configure() was called, will use the passed \a setup to extract
 * geometry information about the scan. Note that \a setup must be consistent with the projections
 * that shall be reconstructed, that means it should contain information that actually corresponds
 * to the projections.
 *
 * Please also note that, in case that a subset of projections (by means of a ProjectionDataView)
 * is used in reconstruction later on, the \a setup passed here must still refer to the full
 * projection data set, the ProjectionDataView belongs to.
 */
void ARTReconstructor::configure(const AcquisitionSetup& setup)
{
    _setup = setup;
}

/*!
 * \copybrief AbstractReconstructor::parameter
 *
 * Returns a QVariantMap including the following (key, value)-pairs:
 *
 * - ("forward projector", [QVariant] full QVariant of the forward projector),
 * - ("backprojector", [QVariant] full QVariant of the backprojector),
 * - ("regularizer", [QVariant] full QVariant of the regularizer),
 * - ("default subset generator", [QVariant] full QVariant of the default subset generator),
 * - ("custom subset generator", [QVariant] full QVariant of the custom subset generator),
 * - ("max nb iterations", [uint] maximum number of iterations (stopping criterion)),
 * - ("use positivity constraint", [bool] whether or not positivity constraint is applied),
 * - ("use regularization", [bool] whether or not positivity constraint is applied),
 * - ("use relaxation estimation", [bool] whether or not relaxation parameter estimation is used),
 * - ("relaxation parameter", [float] relaxation parameter value),
 * - ("terminate normal eq tol", [float] minimum normal equation tolerance (stopping criterion)),
 * - ("terminate volume change", [float] minimum volume change (stopping criterion)),
 * - ("terminate proj error change", [float] minimum projection error change(stopping criterion)),
 * - ("terminate relative proj error",[float] minimum projection error (stopping criterion)),
 * - ("terminate max time", [uint] maximum time (in ms) for reconstruction (stopping criterion)),
 * - ("active stopping criteria", [string] combination of flags of enabled stopping criteria
 * - ("uses default subset generator", [bool] whether or not the default subset generator is used).
 */
QVariant ARTReconstructor::parameter() const
{
    QVariantMap ret = AbstractReconstructor::parameter().toMap();

    // complex types
    ret.insert(QStringLiteral("forward projector"), _fp->toVariant());
    ret.insert(QStringLiteral("backprojector"), _bp->toVariant());
    ret.insert(QStringLiteral("regularizer"), _regularizer->toVariant());
    ret.insert(QStringLiteral("default subset generator"), _defaultSubsetGen->toVariant());
    if(_customSubsetGen)
        ret.insert(QStringLiteral("custom subset generator"), _customSubsetGen->toVariant());

    // simple parameters
    ret.insert(QStringLiteral("max nb iterations"), _maxNbIterations);
    ret.insert(QStringLiteral("use positivity constraint"), _usePositivityConstraint);
    ret.insert(QStringLiteral("use regularization"), _useRegularization);
    ret.insert(QStringLiteral("use relaxation estimation"), _useRelaxationEstimation);
    ret.insert(QStringLiteral("relaxation parameter"), _relax);
    ret.insert(QStringLiteral("terminate normal eq tol"), _terminateNormalEqTol);
    ret.insert(QStringLiteral("terminate volume change"), _terminateVolChange);
    ret.insert(QStringLiteral("terminate proj error change"), _terminateProjErrChange);
    ret.insert(QStringLiteral("terminate relative proj error"), _terminateProjErrRel);
    ret.insert(QStringLiteral("terminate max time"), _terminateMaxTime);
    ret.insert(QStringLiteral("active stopping criteria"),
               QString(metaEnum().valueToKeys(_stoppingCriteria)));
    ret.insert(QStringLiteral("uses default subset generator"), isDefaultSubsetGeneratorInUse());

    return ret;
}

/*!
 * \copybrief AbstractReconstructor::setParameter
 *
 * The passed \a parameter must be a QVariantMap containing the following (key, value)-pairs
 * (note: no support for setting individual parameters here!):
 *
 * - ("forward projector", [QVariant] full QVariant of the forward projector),
 * - ("backprojector", [QVariant] full QVariant of the backprojector),
 * - ("regularizer", [QVariant] full QVariant of the regularizer),
 * - ("default subset generator", [QVariant] full QVariant of the default subset generator),
 * - ("custom subset generator", [QVariant] full QVariant of the custom subset generator),
 * - ("max nb iterations", [uint] maximum number of iterations (stopping criterion)),
 * - ("use positivity constraint", [bool] whether or not positivity constraint is applied),
 * - ("use regularization", [bool] whether or not positivity constraint is applied),
 * - ("use relaxation estimation", [bool] whether or not relaxation parameter estimation is used),
 * - ("relaxation parameter", [float] relaxation parameter value),
 * - ("terminate normal eq tol", [float] minimum normal equation tolerance (stopping criterion)),
 * - ("terminate volume change", [float] minimum volume change (stopping criterion)),
 * - ("terminate proj error change", [float] minimum projection error change(stopping criterion)),
 * - ("terminate relative proj error",[float] minimum projection error (stopping criterion)),
 * - ("terminate max time", [uint] maximum time (in ms) for reconstruction (stopping criterion)),
 * - ("active stopping criteria", [string] combination of flags of enabled stopping criteria
 * - ("uses default subset generator", [bool] whether or not the default subset generator is used).
 *
 * Note that it is strongly discouraged to use this method to set individual parameters of the
 * instance. Please consider using individual setter methods for that purpose.
 */
void ARTReconstructor::setParameter(const QVariant& parameter)
{
    const auto parMap = parameter.toMap();

    // complex types
    const auto fpVariant = parMap.value(QStringLiteral("forward projector"));
    {
        auto parsedObj = SerializationHelper::parseProjector(fpVariant);

        if(!parsedObj)
            throw std::runtime_error("ARTReconstructor::setParameter(): "
                                     "Could not successfully parse forward projector object.");

        setForwardProjector(parsedObj);
    }

    const auto bpVariant = parMap.value(QStringLiteral("backprojector"));
    {
        auto parsedObj = SerializationHelper::parseReconstructor(bpVariant);

        if(!parsedObj)
            throw std::runtime_error("ARTReconstructor::setParameter(): "
                                     "Could not successfully parse backprojector object.");

        setBackprojector(parsedObj);
    }

    const auto regVariant = parMap.value(QStringLiteral("regularizer"));
    {
        auto parsedObj = SerializationHelper::parseMiscObject(regVariant);

        if(!dynamic_cast<AbstractVolumeFilter*>(parsedObj))
        {
            delete parsedObj;
            throw std::runtime_error("ARTReconstructor::setParameter(): "
                                     "Could not successfully parse regularizer object.");
        }
        setRegularizer(static_cast<AbstractVolumeFilter*>(parsedObj));
    }

    const auto defSubGenVariant = parMap.value(QStringLiteral("default subset generator"));
    {
        auto parsedObj = SerializationHelper::parseMiscObject(defSubGenVariant);

        if(!dynamic_cast<DefaultSubsetGenerator*>(parsedObj))
        {
            delete parsedObj;
            throw std::runtime_error("ARTReconstructor::setParameter(): "
                                     "Could not successfully parse default subset generator object.");
        }
        _defaultSubsetGen.reset(static_cast<DefaultSubsetGenerator*>(parsedObj));
    }

    if(parMap.contains(QStringLiteral("custom subset generator")))
    {
        const auto custSubGenVariant = parMap.value(QStringLiteral("custom subset generator"));
        {
            auto parsedObj = SerializationHelper::parseMiscObject(custSubGenVariant);

            if(!dynamic_cast<AbstractSubsetGenerator*>(parsedObj))
            {
                delete parsedObj;
                throw std::runtime_error("ARTReconstructor::setParameter(): "
                                         "Could not successfully parse custom subset generator object.");
            }
            setSubsetGenerator(static_cast<AbstractSubsetGenerator*>(parsedObj));
        }
    }


    // simple parameters
    _maxNbIterations         = parMap.value(QStringLiteral("max nb iterations")).toUInt();
    _usePositivityConstraint = parMap.value(QStringLiteral("use positivity constraint")).toBool();
    _useRegularization       = parMap.value(QStringLiteral("use regularization")).toBool();
    _useRelaxationEstimation = parMap.value(QStringLiteral("use relaxation estimation")).toBool();
    _relax                   = parMap.value(QStringLiteral("relaxation parameter")).toFloat();
    _terminateNormalEqTol    = parMap.value(QStringLiteral("terminate normal eq tol")).toFloat();
    _terminateVolChange      = parMap.value(QStringLiteral("terminate volume change")).toFloat();
    _terminateProjErrChange  = parMap.value(QStringLiteral("terminate proj error change")).toFloat();
    _terminateProjErrRel     = parMap.value(QStringLiteral("terminate relative proj error")).toFloat();
    _terminateMaxTime        = parMap.value(QStringLiteral("terminate max time")).toUInt();

    const auto typeStr = parMap.value(QStringLiteral("active stopping criteria")).toString();
    int type = metaEnum().keysToValue(qPrintable(typeStr));
    if(type == -1)
    {
        qWarning() << "ARTReconstructor::setParameter: Incorrect stopping criteria flags specified."
                      " (valid options: [NoStoppingCriterion | MaximumNbIterations | MaximumTime"
                      " | ProjectionErrorChange | VolumeDomainChange | RelativeProjectionError"
                      " | NormalEquationSatisfied | AllStoppingCriteria]). Combinations can be"
                      " created with '|' separator. Using default value (MaximumNbIterations).";
        type = MaximumNbIterations;
    }

    setStoppingCriteria(type);

    const auto useDefSubGen = parMap.value(QStringLiteral("uses default subset generator")).toBool();
    useDefSubGen ? useDefaultSubsetGenerator() : useCustomSubsetGenerator();
}

/*!
 * \copydoc AbstractReconstructor::toVariant
 *
 * In addition to any base class information, this introduces a (key,value)-pair:
 * ("#", [string] this class' name).
 */
QVariant ARTReconstructor::toVariant() const
{
    QVariantMap ret = AbstractReconstructor::toVariant().toMap();

    ret.insert(QStringLiteral("#"), "ARTReconstructor");

    return ret;
}

/*!
 * \brief Constructs an instance of ARTReconstructorSFP and sets the footprint type of both the
 * forward as well as backprojector to \a footprintType.
 */
ARTReconstructorSFP::ARTReconstructorSFP(FootprintType footprintType)
    : ARTReconstructor(makeProjector<OCL::SFPProjector>(OCL::SFPProjector::FootprintType(footprintType)),
                       makeReconstructor<OCL::SFPBackprojector>(OCL::SFPBackprojector::FootprintType(footprintType)))
{
}

/*!
 * \brief Constructs an instance of ARTReconstructorSFP and sets the footprint type of the forward
 *  and backprojector to \a footprintTypeFP and \a footprintTypeBP, respectively.
 */
ARTReconstructorSFP::ARTReconstructorSFP(FootprintType footprintTypeFP,
                                         FootprintType footprintTypeBP)
    : ARTReconstructor(makeProjector<OCL::SFPProjector>(OCL::SFPProjector::FootprintType(footprintTypeFP)),
                       makeReconstructor<OCL::SFPBackprojector>(OCL::SFPBackprojector::FootprintType(footprintTypeBP)))
{
}

namespace assist {

/*!
 * \brief Enforces positivity in \a volume.
 *
 * This sets all values in \a volume that are negative to zero.
 */
void enforcePositivity(VoxelVolume<float>& volume)
{
    std::for_each(volume.begin(), volume.end(), [] (float& vox)
    {
        if(vox < 0.0f)
            vox = 0.0f;
    });
}

} // namespace assist

/*!
 * \brief Computes the L2 norm of \a projections.
 */
float computeProjectionNorm(const ProjectionDataView& projections)
{
    auto ret = 0.0f;

    for(auto v = 0u, nbViews = projections.nbViews(); v < nbViews; ++v)
        ret += std::inner_product(projections.view(v).cbegin(), projections.view(v).cend(),
                                  projections.view(v).cbegin(), 0.0f);

    return std::sqrt(ret);
}

/*!
 * \brief Computes the L2 norm of the backprojection of \a projections.
 *
 * Backprojection is performed with OCL::SimpleBackprojector using the passed \a setup and the
 * volume specifications \a dimensions and \a voxelSize. Note that this needs to allocate memory for
 * the volume.
 */
float computeBackprojectedProjectionNorm(const ProjectionDataView& projections,
                                         const AcquisitionSetup& setup,
                                         const VoxelVolume<float>::Dimensions& dimensions,
                                         const VoxelVolume<float>::VoxelSize& voxelSize)
{
    auto backproj = OCL::SimpleBackprojector{}.configureAndReconstruct(
        setup, projections, VoxelVolume<float>{ dimensions, voxelSize });

    return std::sqrt(
        std::inner_product(backproj.cbegin(), backproj.cend(), backproj.cbegin(), 0.0f));
}

/*!
 * \enum ARTReconstructor::StoppingCriterion
 * Enumeration for stopping criteria that can be used in ARTReconstructor. The enumeration values
 * can be used as flags, i.e. they can be combined via bitwise OR operation (`crit1 | crit2 | ...`)
 * to create arbitrary combinations of criteria to enable (for enabling see setStoppingCriteria()).
 */

/*!
 * \var ARTReconstructor::NoStoppingCriterion
 *
 * Using no stopping criterion at all. Note that reconstructions performed with this stopping
 * criterion mode will not terminate.
 */

/*!
 * \var ARTReconstructor::MaximumNbIterations
 *
 * Reconstruction will terminate after a defined total number of iterations has been performed.
 * A single iteration includes processing all subsets associated with that particular iteration.
 * The maximum number of iterations to check for when using this criterion can be set via
 * setMaxNbIterations().
 */

/*!
 * \var ARTReconstructor::MaximumTime
 *
 * Reconstruction will terminate after a defined total time for reconstruction has passed.
 * Note that stopping criteria are evaluated at the end of a full iteration (i.e. after all subsets
 * have been processed). Therefore, the specified time may be exceeded by a more or less extensive
 * amount, depending on the required duration for a single iteration. In other words,
 * reconstruction stops after the first full iteration at which total time exceeds the threshold.
 * The maximum reconstruction time to check for when using this criterion can be set via
 * setMaxNbIterations().
 */

/*!
 * \var ARTReconstructor::ProjectionErrorChange
 *
 * Reconstruction will terminate when the relative change in the projection error from one
 * iteration to the other falls below a defined threshold.
 * Assume that \f$ \Delta P_{old}\f$ is the projection error of the previous iteration and
 * \f$ \Delta P_{new}\f$ that of the current one, and given a threshold of \f$ \Delta P_{min}\f$,
 * the stopping criterion is reached if
 *
 * \f$
 * \frac{|\Delta P_{new} - \Delta P_{old}|}
 *      {\Delta P_{old}} < \Delta P_{min}
 * \f$
 *
 * The minimum relative change in projection error (i.e. threshold \f$ \Delta P_{min} \f$) to check
 * for when using this criterion can be set via setMinChangeInProjectionError().
 */

/*!
 * \var ARTReconstructor::VolumeDomainChange
 *
 * Reconstruction will terminate when the relative change in the volume domain from one
 * iteration to the other falls below a defined threshold.
 * Assume that \f$ V_{old} \f$ is the volume estimate of the previous iteration and
 * \f$ V_{new} \f$ that of the current one, and given a threshold of \f$ \Delta V_{min}\f$,
 * the stopping criterion is reached if
 *
 * \f$
 * \frac{\left\Vert V_{new} - V_{old} \right\Vert_{L2}}
 *      {\left\Vert V_{new} \right\Vert_{L2}} < \Delta V_{min}
 * \f$
 *
 * The minimum relative change in projection error (i.e. threshold \f$ \Delta V_{min} \f$) to check
 * for when using this criterion can be set via setMinChangeInVolumeDomain().
 */

/*!
 * \var ARTReconstructor::RelativeProjectionError
 *
 * Reconstruction will terminate when the relative projection error in an iteration (w.r.t. the norm
 * of the original projections) falls below a defined threshold.
 * Assume that \f$ \ P_{sim}\f$ are the simulated projections of the current iteration and
 * \f$ P_{orig}\f$ are the original ('measured') data, and given a threshold of
 * \f$ \Delta P_{min}\f$, the stopping criterion is reached if
 *
 * \f$
 * \frac{\left\Vert P_{sim} - P_{orig} \right\Vert_{L2}}
 *      {\left\Vert P_{orig} \right\Vert_{L2}} < \Delta P_{min}
 * \f$
 *
 * The minimum relative projection error (i.e. threshold \f$ \Delta P_{min} \f$) to check for when
 * using this criterion can be set via setMinRelativeProjectionError().
 */

/*!
 * \var ARTReconstructor::NormalEquationSatisfied
 *
 * Reconstruction will terminate when the relative deviation from fulfilling the normal equation
 * in an iteration falls below a defined threshold.
 *
 * Full details on the criterion can be found in terminateByNormalEqTol().
 *
 * The threshold to check for when using this criterion can be set via setNormalEqTolerance().
 */

/*!
 * \var ARTReconstructor::AllStoppingCriteria
 *
 * Convenience enumeration value that contains all flags. Thus, enables all stopping criteria
 * simultaneously if used.
 */
} // namespace CTL

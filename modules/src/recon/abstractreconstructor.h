#ifndef CTL_ABSTRACTRECONSTRUCTOR_H
#define CTL_ABSTRACTRECONSTRUCTOR_H

#include "img/projectiondataview.h"
#include "io/serializationinterface.h"
#include <memory>

namespace CTL {

class AbstractDynamicVolumeData;
class AcquisitionSetup;
class CompositeVolume;
class SparseVoxelVolume;
class SpectralVolumeData;
template <typename> class VoxelVolume;

/*!
 * \class ReconstructorNotifier
 *
 * \brief Helper class that can emit signals during calculations of a certain reconstructor.
 *
 * This class uses Qt's signal/slot principle to allow communication of a reconstructor class with
 * other program parts. To do so, connect the signals of this object with the desired receiver
 * objects.
 *
 * The notifier can conveniently be connected to the MessageHandler of the CTL using
 * connectToMessageHandler() or directly from a reconstructor instance through
 * AbstractReconstructor::connectNotifierToMessageHandler().
 *
 * The ReconstructorNotifier offers the following Q_SIGNALS:
 * - progress(qreal): intended to be emitted an update on the progress (1.0 meaning finished),
 * - information(QString): used to communicate status information.
 */
class ReconstructorNotifier : public QObject
{
public:
    void connectToMessageHandler(bool includeProgress);

    Q_OBJECT
Q_SIGNALS:
    void progress(qreal portionCompleted);
    void information(QString info);

private:
    QMetaObject::Connection _progressConnection;
};

/// \class AbstractReconstructor
///
/// \brief The AbstractReconstructor class defined the interface for reconstruction types.
///
/// Note: the beginning of this section deals with usage of reconstruction classes. Information
/// on sub-classing AbstractReconstructor can be found at the end of this description.
///
/// In the CTL, a reconstruction type is defined as any class that provides an algorithm to transform
/// data from the projection domain to the volume domain. Note that, in common practical terms, this
/// includes both straightforward backprojection methods as well as full reconstruction routines (
/// e.g. FDK, ART).
///
/// Generally, two options to reconstruct can be used:
/// 1. Reconstructing "into" an already existing volume through reconstructTo().
/// 2. Requesting the reconstructed volume by value through reconstruct().
///
/// In both cases, an AcquisitionSetup object containing a setup that describes all required details
/// on the acquisition setting used to acquire the projections must be passed to configure() before
/// any reconstruction can be performed. It might also be useful to check whether or not the specific
/// reconstruction class is able to handle your input data first via isApplicableTo().
///
/// When using version 1., reconstructed values are _added_ to the input volume. In doing so, the
/// input volume simultaneously serves as initialization as well as final output---the
/// reconstruction is carried out _in place_. As for version 2., there is also a volume object
/// required as input for the routine. This volume, however, only serves the purpose of initializing
/// the result as well as specifying its properties, such as dimensions and potential further
/// information (e.g. material). The result will then be returned as a separate volume object.
///
/// Depending on the capabilities of the specific sub-class you are using, you may use the
/// reconstruction methods for one or more different input volume data types (i.e. plain
/// VoxelVolume<float>, SpectralVolumeData, AbstractDynamicVolumeData sub-classes, SparseVoxelVolume,
/// and/or CompositeVolume). If the corresponding method for a particular volume type is not
/// re-implemented by the reconstructor, it will return \c std::runtime_error when called.
///
/// AbstractReconstructor provides a selection of pre-implemented convenience methods that rely on
/// the virtual interface. These include:
/// - reconstructTo(): templated method that combines all individual reconstruction methods,
/// - reconstruct(): reconstruction call wrapper that returns the reconstructed volume by value,
/// - configureAndReconstructTo(): convenience method that combines calls to configure() and
/// reconstructTo() into a single call, and
/// - configureAndReconstruct(): convenience method that combines calls to configure() and
/// reconstruct() into a single call.
///
/// It is possible to force the use of a particular reconstruction method when using special volume
/// types through use of an explicit \c reconstructToXYZ() call or by specifying the target type as
/// template parameter for reconstructTo() or reconstruct(). Note: you can only enforce usage of
/// methods on the superclasses of a volume.
///
/// Example: explicitely using the method for plain VoxelVolume<float> on SpectralVolumeData input
/// \code
///  AbstractReconstructor* reconstructor /*= new SomeActualReconstructor*/;
///
///  // define a 'SpectralVolumeData' volume
///  auto volume = SpectralVolumeData::cube(100, 1.0f, 0.0f,
///                                         database::attenuationModel(database::Composite::Water));
///
///  AcquisitionSetup setup;
///  // specify details of the setup, according to the desired simulation setting or used measurement equipment
///  // ...
///
///  auto projections = ProjectionData::dummy();
///  // simulate projections or load some real measurement data here instead
///
///  // assuming 'reconstructor' does not provide a reconstructToSpectral() implementation
///  try {
///      reconstructor->configureAndReconstructTo(setup, projections, volume);
///  } catch (std::exception& err) {
///      qInfo() << "Error:" << err.what();
///  }
///  // output: Error: No reconstruction method for SpectralVolumeData implemented.
///
///  // instead, force 'reconstructor' to apply its VoxelVolume<float> algorithm to 'volume'
///  reconstructor->configureAndReconstructTo<VoxelVolume<float>>(setup, projections, volume);
///
///  // alternative:
///  // pass the acquisition setup and use 'reconstructToPlain()'
///  reconstructor->configure(setup);
///  reconstructor->reconstructToPlain(projections, volume);
/// \endcode
///
/// ### Sub-classing
/// AbstractReconstructor is a moveable but non-copyable type.
///
/// The concept of the interface defined by AbstractReconstructor is fairly similar to the one found
/// in AbstractProjector. Sub-classes need to :
/// 1. Implement configure(const AcquisitionSetup&) - This method takes an AcquisitionSetup
/// object containing a setup that describes all available details on the acquisition setting used to
/// acquire the data that will later be reconstructed. Use it to extract all relevant data needed for
/// your reconstruction method.
/// 2. Override at least on of the actual (virtual) reconstruction methods:
///    a) reconstructToPlain(),
///    b) reconstructToSpectral(),
///    c) reconstructToDynamic(),
///    d) reconstructToComposite(),
///    e) reconstructToSparse(),
/// to provide the desired reconstruction routine.
///
/// Additionally, you might want to re-implement
/// \code bool isApplicableTo(const AcquisitionSetup&) \endcode
/// such that it inspects the passed setup and returns \c true / \c false depending on whether or
/// not the contained acquisition can be handled by your implementation (e.g. by means of verifying
/// potential geometry constraints). By default, isApplicableTo() always returns \a true.
///
/// To enable de-/serialization of sub-class objects, also reimplement the parameter() and
/// setParameter() methods. parameter() should stores all parameters of your class in a QVariant;
/// setParameter() must parse an input QVariant and read from it all values defining you instance.
/// Additionally, call the macro #DECLARE_SERIALIZABLE_TYPE(YourNewClassName) within the .cpp file of
/// your new class (substitute "YourNewClassName" with the actual class name). Objects of the new
/// class can then be de-/serialized with any of the serializer classes (see also
/// AbstractSerializer).
///
/// Alternatively, the de-/serialization interface methods toVariant() and fromVariant() can be
/// reimplemented directly. This might be required in some highly specific situations.
/// For the majority of cases, using the parameter() / setParameter() approach should be sufficient
/// and is recommended.
class AbstractReconstructor : public SerializationInterface
{
    CTL_TYPE_ID(0)

    public:virtual void configure(const AcquisitionSetup& setup) = 0;

public:
    AbstractReconstructor() = default;
    AbstractReconstructor(const AbstractReconstructor&) = delete;
    AbstractReconstructor(AbstractReconstructor&&) = default;
    AbstractReconstructor& operator=(const AbstractReconstructor&) = delete;
    AbstractReconstructor& operator=(AbstractReconstructor&&) = default;
    ~AbstractReconstructor() override = default;

    virtual bool isApplicableTo(const AcquisitionSetup& setup) const; // { return true; }

    // reconstruction methods
    virtual bool reconstructToPlain(const ProjectionDataView& projections,
                                    VoxelVolume<float>& targetVolume);
    virtual bool reconstructToSpectral(const ProjectionDataView& projections,
                                       SpectralVolumeData& targetVolume);
    virtual bool reconstructToDynamic(const ProjectionDataView& projections,
                                      AbstractDynamicVolumeData& targetVolume);
    virtual bool reconstructToComposite(const ProjectionDataView& projections,
                                        CompositeVolume& targetVolume);
    virtual bool reconstructToSparse(const ProjectionDataView& projections,
                                     SparseVoxelVolume& targetVolume);

    template <class VolumeType>
    bool reconstructTo(const ProjectionDataView& projections, VolumeType& targetVolume);

    template <class VolumeType>
    bool configureAndReconstructTo(const AcquisitionSetup& setup,
                                   const ProjectionDataView& projections,
                                   VolumeType& targetVolume);

    // value returner
    template <class VolumeType,
              class = typename std::enable_if<!std::is_lvalue_reference<VolumeType>::value>::type>
    Q_REQUIRED_RESULT
    VolumeType reconstruct(const ProjectionDataView& projections,
                           const VolumeType& initialVolume,
                           bool* ok = nullptr);

    template <class VolumeType,
              class = typename std::enable_if<!std::is_lvalue_reference<VolumeType>::value>::type>
    Q_REQUIRED_RESULT
    VolumeType reconstruct(const ProjectionDataView& projections,
                           VolumeType&& initialVolume,
                           bool* ok = nullptr);

    template <class VolumeType,
              class = typename std::enable_if<!std::is_lvalue_reference<VolumeType>::value>::type>
    Q_REQUIRED_RESULT
    VolumeType configureAndReconstruct(const AcquisitionSetup& setup,
                                       const ProjectionDataView& projections,
                                       const VolumeType& initialVolume,
                                       bool* ok = nullptr);

    template <class VolumeType,
              class = typename std::enable_if<!std::is_lvalue_reference<VolumeType>::value>::type>
    Q_REQUIRED_RESULT
    VolumeType configureAndReconstruct(const AcquisitionSetup& setup,
                                       const ProjectionDataView& view,
                                       VolumeType&& initialVolume,
                                       bool* ok = nullptr);

    // SerializationInterface interface
    virtual QVariant parameter() const;
    virtual void setParameter(const QVariant& parameter);

    void fromVariant(const QVariant& variant) override; // { }
    QVariant toVariant() const override; // { return QVariant(); }

    virtual ReconstructorNotifier* notifier();
    void connectNotifierToMessageHandler(bool includeProgress = false);

private:
    std::unique_ptr<ReconstructorNotifier> _notifier{
        new ReconstructorNotifier
    }; //!< The notifier object used for signal emission.
};

template <>
bool AbstractReconstructor::reconstructTo(const ProjectionDataView& projections,
                                          VoxelVolume<float>& targetVolume);
template <>
bool AbstractReconstructor::reconstructTo(const ProjectionDataView& projections,
                                          SpectralVolumeData& targetVolume);
template <>
bool AbstractReconstructor::reconstructTo(const ProjectionDataView& projections,
                                          CompositeVolume& targetVolume);
template <>
bool AbstractReconstructor::reconstructTo(const ProjectionDataView& projections,
                                          SparseVoxelVolume& targetVolume);
/*!
 * Implementation for all remaining input types. See reconstructTo().
 *
 * Asserts that the type is convertible to a AbstractDynamicVolumeData and (if successful) calls
 * reconstructToDynamic().
 */
template <class DynamicVolumeType>
bool AbstractReconstructor::reconstructTo(const ProjectionDataView& projections,
                                          DynamicVolumeType& targetVolume)
{
    static_assert(std::is_convertible<DynamicVolumeType*, AbstractDynamicVolumeData*>::value,
        "Method call defaulted to dynamic case, because the passed volume type is not covered "
        "by any available specialization. Consider explicitly providing the required base type "
        "(e.g. reconstruct[To]<SpectralVolumeData>()).");

    return reconstructToDynamic(projections, targetVolume);
}

/*!
 * Specialization for VoxelVolume<float> input. See configureAndReconstructTo().
 */
template <>
bool AbstractReconstructor::configureAndReconstructTo(const AcquisitionSetup& setup,
                                                      const ProjectionDataView& projections,
                                                      VoxelVolume<float>& targetVolume);
/*!
 * Specialization for SpectralVolumeData input. See configureAndReconstructTo().
 */
template <>
bool AbstractReconstructor::configureAndReconstructTo(const AcquisitionSetup& setup,
                                                      const ProjectionDataView& projections,
                                                      SpectralVolumeData& targetVolume);
/*!
 * Specialization for CompositeVolume input. See configureAndReconstructTo().
 */
template <>
bool AbstractReconstructor::configureAndReconstructTo(const AcquisitionSetup& setup,
                                                      const ProjectionDataView& projections,
                                                      CompositeVolume& targetVolume);
/*!
 * Specialization for SparseVoxelVolume input. See configureAndReconstructTo().
 */
template <>
bool AbstractReconstructor::configureAndReconstructTo(const AcquisitionSetup& setup,
                                                      const ProjectionDataView& projections,
                                                      SparseVoxelVolume& targetVolume);
/*!
 * Implementation for all remaining input types. See configureAndReconstructTo().
 *
 * Asserts that the type is convertible to a AbstractDynamicVolumeData and (if successful) calls
 * reconstructToDynamic() (preceded by configure()).
 */
template <class DynamicVolumeType>
bool AbstractReconstructor::configureAndReconstructTo(const AcquisitionSetup& setup,
                                                      const ProjectionDataView& projections,
                                                      DynamicVolumeType& targetVolume)
{
    static_assert(std::is_convertible<DynamicVolumeType*, AbstractDynamicVolumeData*>::value,
        "Method call defaulted to dynamic case, because the passed volume type is not covered "
        "by any available specialization. Consider explicitly providing the required base type "
        "(e.g. reconstruct[To]<SpectralVolumeData>()).");

    configure(setup);
    return reconstructToDynamic(projections, targetVolume);
}

/*!
 * \copydoc AbstractReconstructor::reconstruct(const ProjectionDataView&,const VolumeType&,bool*)
 */
template <class VolumeType, class>
VolumeType AbstractReconstructor::reconstruct(const ProjectionDataView& projections,
                                              const VolumeType& initialVolume,
                                              bool* ok)
{
    VolumeType ret(initialVolume);

    const auto recoOk = reconstructTo(projections, ret);

    if(ok) *ok = recoOk;

    return ret;
}

/*!
 * \copydoc AbstractReconstructor::reconstruct(const ProjectionDataView&,VolumeType&&,bool*)
 */
template <class VolumeType, class>
VolumeType AbstractReconstructor::reconstruct(const ProjectionDataView& projections,
                                              VolumeType&& initialVolume,
                                              bool* ok)
{
    VolumeType ret(std::move(initialVolume));

    const auto recoOk = reconstructTo(projections, ret);

    if(ok) *ok = recoOk;

    return ret;
}

/*!
 * \copydoc AbstractReconstructor::configureAndReconstruct(const AcquisitionSetup&,const ProjectionDataView&,const VolumeType&,bool*)
 */
template <class VolumeType, class>
VolumeType AbstractReconstructor::configureAndReconstruct(const AcquisitionSetup& setup,
                                                          const ProjectionDataView& projections,
                                                          const VolumeType& initialVolume,
                                                          bool* ok)
{
    configure(setup);
    return reconstruct(projections, initialVolume, ok);
}

/*!
 * \copydoc AbstractReconstructor::configureAndReconstruct(const AcquisitionSetup&,const ProjectionDataView&,VolumeType&&,bool*)
 */
template <class VolumeType, class>
VolumeType AbstractReconstructor::configureAndReconstruct(const AcquisitionSetup& setup,
                                                          const ProjectionDataView& projections,
                                                          VolumeType&& initialVolume,
                                                          bool* ok)
{
    configure(setup);
    return reconstruct(projections, std::move(initialVolume), ok);
}

/*!
 * \fn bool AbstractReconstructor::reconstructTo(const ProjectionDataView& projections, VolumeType& targetVolume)
 *
 * \brief Convenience method wrapping up all `reconstructToXXX()` methods.
 *
 * Directs the call to the correct `reconstructToXXX()` method, according to the type of
 * \a targetVolume.
 */

/*!
 * \fn Q_REQUIRED_RESULT VolumeType AbstractReconstructor::reconstruct(const ProjectionDataView& projections, const VolumeType& initialVolume, bool* ok = nullptr)
 *
 * \brief Reconstructs \a projections into a new volume object initialized with \a initialVolume
 * and returns the result.
 *
 * This method provides a means to perform reconstructions with the result being returned by
 * value (as opposed to in-place reconstruction carried out with reconstructTo()). Creates a
 * volume initialized by \a initialVolume. Note that this implies that the new volume has the
 * same specifications (e.g. dimensions) as \a initialVolume.
 * Specific sub-classes of AbstractReconstructor might also allow use of \a initialVolume which
 * does not have any memory allocated. In these cases it might be interpreted as a template for
 * the specifications of the new volume only (usually combined with zero-initialization). Refer
 * to the documentation of actual reconstruction sub-classes for details on this aspect.
 *
 * The result of the performed reconstructTo() call will be placed in \a ok. You may use it to
 * check for reconstruction success as reported by the actual reconstruction routine.
 */

/*!
 * \fn bool AbstractReconstructor::configureAndReconstructTo(const AcquisitionSetup& setup, const ProjectionDataView& projections, VolumeType& targetVolume)
 *
 * \brief Combines the calls to configure() and reconstructTo().
 *
 * This convenience method combines the calls to configure() and reconstructTo() into a single
 * call. Returns the result of `reconstructTo`.
 *
 * Using this method is recommended over individual calls of configure() and reconstructTo()
 * directly after one another, because it may help you avoid mistakes. In particular, it prevents
 * potentially missing the call to configure() before calling reconstructTo(), e.g. after
 * changing settings of the reconstructor (which usually requires a re-`configure`).
 *
 * Note that this method usually changes the state of the reconstructor due to the `configure`
 * step.
 *
 * \sa configure(), reconstructTo()
 */

/*!
 * \fn Q_REQUIRED_RESULT VolumeType AbstractReconstructor::reconstruct(const ProjectionDataView& projections, VolumeType&& initialVolume, bool* ok = nullptr)
 *
 * \brief Reconstructs \a projections into a new volume object initialized with \a initialVolume
 * and returns the result.
 *
 * Overload for r-value input of \a initialVolume.
 * See reconstruct(const ProjectionDataView&,const VolumeType&,bool*).
 */

/*!
 * \fn Q_REQUIRED_RESULT VolumeType AbstractReconstructor::configureAndReconstruct(const AcquisitionSetup& setup, const ProjectionDataView& projections, const VolumeType& initialVolume, bool* ok = nullptr)
 *
 * \brief Combines the calls to configure() and reconstruct().
 *
 * This convenience method combines the calls to configure() and reconstruct() into a single
 * call. Returns the result of `reconstruct` (i.e. the reconstructed volume). The result of the
 * (internally) performed reconstructTo() call will be placed in \a ok. You may use it to
 * check for reconstruction success as reported by the actual reconstruction routine.
 *
 * Using this method is recommended over individual calls of configure() and reconstruct()
 * directly after one another, because it may help you avoid mistakes. In particular, it prevents
 * potentially missing the call to configure() before calling reconstruct(), e.g. after
 * changing settings of the reconstructor (which usually requires a re-`configure`).
 *
 * Note that this method usually changes the state of the reconstructor due to the `configure`
 * step.
 *
 * \sa configure(), reconstruct()
 */

/*!
 * \fn Q_REQUIRED_RESULT VolumeType AbstractReconstructor::configureAndReconstruct(const AcquisitionSetup& setup, const ProjectionDataView& view, VolumeType&& initialVolume, bool* ok = nullptr)
 *
 * \brief Combines the calls to configure() and reconstruct().
 *
 * Overload for r-value input of \a initialVolume.
 * See configureAndReconstruct(const AcquisitionSetup&,const ProjectionDataView&,const VolumeType&,bool*).
 */


// factory function `makeReconstructor`
template <typename ReconstructorType, typename... ConstructorArguments>
auto makeReconstructor(ConstructorArguments&&... arguments) ->
    typename std::enable_if<std::is_convertible<ReconstructorType*, AbstractReconstructor*>::value,
                            std::unique_ptr<ReconstructorType>>::type
{
    return std::unique_ptr<ReconstructorType>(new ReconstructorType(
                                              std::forward<ConstructorArguments>(arguments)...));
}

} // namespace CTL

/*! \file */
///@{

/*!
* \fn std::unique_ptr<ReconstructorType> CTL::makeReconstructor(ConstructorArguments&&... arguments)
* \relates AbstractReconstructor
*
* Global (free) make function that creates a new reconstructor from possible constructor \a arguments.
* The component is returned as a `std::unique_ptr<ReconstructorType>`, whereas `ReconstructorType` is
* the template argument of this function that needs to be specified.
*
* Example:
* \code
* auto reconstructor = makeReconstructor<SimpleBackprojectorCPU>(SimpleBackprojectorCPU::DistanceWeightsOnly);
* \endcode
*/
///@}

#endif // CTL_ABSTRACTRECONSTRUCTOR_H

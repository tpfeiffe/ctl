#ifndef CTL_BACKPROJECTORBASE_H
#define CTL_BACKPROJECTORBASE_H

#include "abstractreconstructor.h"
#include "acquisition/acquisitionsetup.h"
#include "mat/matrix_types.h"
#include "ocl/pinnedmem.h"

namespace CTL {

class SingleViewGeometry;
class FullGeometry;

namespace OCL {

/*!
 * \class BackprojectorBase
 *
 * \brief The BackprojectorBase class is a helper class from which OpenCL implementations of
 * cone-beam backprojections can be derived
 *
 * In its current state, it only implements `configure` and `reconstructToPlain` of the
 * AbstractReconstructor. The implementor of a derived class needs to provide at least a
 * `cl::Kernel` that implements the backprojection method, see the examples
 * "recon/simple_backprojector.cl" and "recon/backprojection_tr.cl".
 * Optionally, two function can be overridden
 *   - `void transferAdditionalData(uint startView, uint nbViews)`
 *   - `float weighting(uint view, uint module) const`.
 *
 * The first one allows to transfer additional data to the OpenCL device if required and the second
 * allows to weight a specific module in a view by a factor.
 *
 * The backprojection \f$b\f$ of cone-beam projections \f$g_{\lambda}(\boldsymbol{\hat{\gamma}})\f$,
 * where \f$\boldsymbol{\hat{\gamma}}\f$ denotes a ray direction, is analytically given by
 *
 * \f$
 * b(\boldsymbol{r})=\int_{\Lambda}M_{\lambda}(\boldsymbol{r})\;
 * g_{\lambda}\big(\frac{\boldsymbol{r}-\boldsymbol{c}_{\lambda}}{\left\Vert \boldsymbol{r}
 * -\boldsymbol{c}_{\lambda}\right\Vert }\big)\,\text{d}\lambda\,,\;
 * M_{\lambda}(\boldsymbol{r})=\frac{1}{\left\langle \boldsymbol{r}
 * -\boldsymbol{c}_{\lambda}\,,\,\boldsymbol{\hat{w}}_{\lambda}\right\rangle ^{2}}\,.
 * \f$
 *
 * Here, \f$\lambda\in\Lambda\subset\mathbb{R}\f$ denotes a path parameter describing the position
 * \f$\boldsymbol{c}_{\lambda}\f$ of the X-ray source on the trajectory.
 * The weighting factor \f$M_{\lambda}(\boldsymbol{r})\f$ (magnification) amplifies the
 * backprojected values located near the X-ray source \f$\boldsymbol{c}_{\lambda}\f$.
 * The unit vector \f$\boldsymbol{\hat{w}}_{\lambda}\f$ points from the X-ray source perpendicular
 * towards a detector module (principal ray).
 * We refer to this `WeightingType` as `DistanceWeightsOnly`,
 * which is typically used in analytical reconstruction methods (see the scoped enumeration
 * `BackprojectorWeighting`).
 *
 * Besides `DistanceWeightsOnly`, we assume that a derived class also implements a second
 * `WeightingType` called `GeometryFactors`. Compared to `DistanceWeightsOnly`, an additional
 * constant factor is included, which is determined by the discretisation of the input projections
 * and the target volume. Precisely, the magnification factor is replaced by
 *
 * \f$
 * \tilde{M}_{\lambda}(\boldsymbol{r})=\frac{V_{\text{vox}}}{A_{\text{pix}}}\frac{D^2}{\left\langle
 * \boldsymbol{r}-\boldsymbol{c}_{\lambda}\,,\,\boldsymbol{\hat{w}}_{\lambda}\right\rangle ^{2}}\,,
 * \f$
 *
 * with
 * \f$V_{\text{vox}}\f$ - volume of a single voxel,
 * \f$A_{\text{pix}}\f$ - area of a single detector pixel, and
 * \f$D\f$              - the distance between X-ray source and detector module.
 * This `WeightingType` is commonly required for iterative/algebraic reconstruction techniques.
 * The so established backprojector consitutes the transposed operator of the discretized cone-beam
 * forward projection (commonly expressed by the transposed system matrix \f$A^{\text{T}}\f$).
 * It means, when backprojecting ones-projections (all projection values equal to one), all entries
 * of the system matrix (geometry factors) that contribute to a specific voxel at \f$\boldsymbol{r}\f$
 * are added up to obtain \f$b(\boldsymbol{r})\f$.
 * Note that \f$M_{\lambda}\f$ can be converted to \f$\tilde{M}_{\lambda}\f$ by using a projection
 * matrix:
 *
 * \f$
 * \tilde{M}_{\lambda}=V_{\text{vox}}K_{11}K_{22}\,M_{\lambda}\,,
 * \f$
 *
 * where \f$K_{ii}\f$ is the \f$i\f$th diagonal element of the calibration matrix,
 * see ProjectionMatrix::intrinsicMatK().
 *
 * \relates CTL::BackprojectorWeighting
 */
class BackprojectorBase : public AbstractReconstructor
{
    CTL_TYPE_ID(1)
    Q_GADGET

    // abstract interface
    public: void configure(const AcquisitionSetup& setup) override;

public:
    using ProjectionWeights = std::vector<std::vector<float>>;
    enum class WeightingType { DistanceWeightsOnly, GeometryFactors };
    Q_ENUM(WeightingType)

    // AbstractReconstructor interface
    bool reconstructToPlain(const ProjectionDataView& projections,
                            VoxelVolume<float>& targetVolume) override;

    bool isConfigConsistentWith(const ProjectionDataView& projections) const;
    bool customWeightsValid(const ProjectionDataView& projections) const;

    const AcquisitionSetup& setup() const;
    WeightingType weightingType() const;
    void setWeightingType(WeightingType weigthType);
    void setCustomWeights(ProjectionWeights weights);

    // SerializationInterface interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

protected:
    BackprojectorBase(cl::Kernel* backsmearKernel, WeightingType weightingType);

    virtual void transferAdditionalData(uint startView, uint nbViews);
    virtual float weighting(uint view, uint module) const;

    // reset kernel
    void setBacksmearKernel(cl::Kernel* backsmearKernel);

    cl::Kernel* backsmearKernel() const;
    const cl::CommandQueue& queue() const;

    float precomputedGeometryFactor(uint view, uint module) const;

    static uint viewsPerBlock(const ProjectionData::Dimensions& projDim);
    static QMetaEnum metaEnum();

private:
    // the setup (copied via `configure`)
    AcquisitionSetup _setup;

    // weighting type
    WeightingType _weightingType;

    // precomputed geometry factors
    ProjectionWeights _precompGeomFactors;

    // custom weights
    ProjectionWeights _customWeights;

    // OCL related
    cl::CommandQueue _queue;

    cl::Kernel* _kernelBacksmear;
    cl::Kernel* _kernelNullifier;

    // regular buffers
    cl::Buffer _volCornerBuffer;
    cl::Buffer _voxSizeBuffer;
    cl::Buffer _pMatBuffer;
    cl::Buffer _sliceBuffer;

    // pinned memory
    std::unique_ptr<PinnedImg3DHostWrite> _projBuf;

    // dimension info
    std::array<uint, 3> _nbVoxels{ 0, 0, 0 };
    Vector3x1 _voxelSize{ 0.0 };
    Vector3x1 _corner{ 0.0 };
    cl::size_t<3> _zeroVec, _projBufferDim;
    size_t _bytesLocPMatMem;
    uint _nbModules, _viewsPerBlock;

    // helper functions
    void createBuffers();
    void createCommandQueue();
    void createPinnedMemory();
    void freeGPUMemory();
    void initNullifierKernel();
    void readoutResult(VoxelVolume<float>& targetVolume, uint z);
    void setDimensions(const ProjectionData::Dimensions& projDim,
                       const VoxelVolume<float>& targetVolume);
    void setFixedKernelArgs();
    void startBacksmearKernel(uint slice, uint view);
    void startNullifierKernel();
    void transferProjectionData(const ProjectionDataView& projections,
                                uint startView, uint nbViews);
    void transferProjectionMatrices(const float* src, uint startView, uint nbViews);
    void transferSlice(const VoxelVolume<float>& targetVolume, uint z);
    void writeFixedBuffers();

    void precomputeGeometryFactors(float volumeOfSingleVoxel, const FullGeometry& pMats);
    static std::vector<float> computeGeometryFactors(float volumeOfSingleVoxel,
                                                     const SingleViewGeometry& modPMats);
};

} // namespace OCL

/*! \enum BackprojectorWeighting
 *  \brief Enumeration to represent two types of weighting within a backprojector:
 *  `DistanceWeightsOnly` and `GeometryFactors`
 *  \relates OCL::BackprojectorBase
 */
using BackprojectorWeighting = OCL::BackprojectorBase::WeightingType;

namespace assist {
bool isConsistentPair(const AcquisitionSetup& setup, const ProjectionDataView& projections);

/*!
 * \brief Converts data from \a vector into a QVariantList.
 */
template<typename T>
QVariantList vectorToVariantList(const std::vector<T>& vector)
{
    QVariantList ret;
    ret.reserve(static_cast<int>(vector.size()));

    for(const auto& val : vector)
        ret.append(val);

    return ret;
}

/*!
 * \brief Converts data from \a list vector into an std::vector<T>.
 *
 * This uses qvariant_cast<T> of values in \a list to convert them to the target type.
 */
template<typename T>
std::vector<T> variantListToVector(const QVariantList& list)
{
    std::vector<T> ret;
    ret.reserve(list.size());

    for(const auto& val : list)
        ret.push_back(qvariant_cast<T>(val));

    return ret;
}
}

} // namespace CTL

/*! \file */

#endif // CTL_BACKPROJECTORBASE_H

#ifndef CTL_SIMPLESUBSETGENERATOR_H
#define CTL_SIMPLESUBSETGENERATOR_H

#include "abstractsubsetgenerator.h"

#include <QObject>

namespace CTL {

/*!
 * \brief The SimpleSubsetGenerator class provides an implementation of subset generation featuring
 * a few simple options.
 *
 * This class implements the AbstractFixedSizeSubsetGenerator interface. The number of subsets that
 * shall be generated can be specified in the constructor or later on through setNbSubsets().
 *
 * All provided subset generation routines in this class are independent of the acquisition
 * geometry. Instead, simple strategies (such as random selection) or basic assumptions on the
 * geometry (e.g. 360 degree circle) are used. Four different 'creation patterns' are available for
 * the generation of subsets:
 * - Random        - randomly selects views for a subset
 * - Adjacent      - puts adjacent views into a subset
 * - Orthogonal180 - selects sets of most orthogonal views, assuming the total scan range was 180 degrees
 * - Orthogonal360 - selects sets of most orthogonal views, assuming the total scan range was 360 degrees
 *
 * The creation pattern (or the order in which views are selected for participation in a subset) can
 * be specified in the constructor or later on through setOrder().
 * All creation patterns ensure that each view appears only once in the entire set of subsets.
 *
 * Example:
 * \code
 *  // create dummy projection data (sizes entirely arbitrary, and irrelevant here)
 *  ProjectionData dummy(1,1,1);
 *  dummy.allocateMemory(36);   // 'dummy' now contains 36 views
 *
 *  // create a subset generator; set the number of subsets to 4 and the pattern to 'Orthogonal360'
 *  SimpleSubsetGenerator subsetGen;
 *  subsetGen.setNbSubsets(4);
 *  subsetGen.setOrder(SimpleSubsetGenerator::Orthogonal360);
 *
 *  // pass the dummy projection data (note: it contains 36 views)
 *  subsetGen.setProjections(dummy);
 *
 *  // generate subsets (note: iteration '0' is arbitrary and has no effect here)
 *  const auto subsets = subsetGen.generateSubsets(0);
 *
 *  // print the ids of the views included in the two subsets
 *  for(const auto& subset : subsets)
 *      qInfo() << subset.viewIds();
 *
 *      // output:
 *      // std::vector(18, 27, 22, 31, 25, 34, 20, 29, 21)
 *      // std::vector(30, 19, 28, 26, 35, 23, 32, 24, 33)
 *      // std::vector(0, 9, 4, 13, 7, 16, 2, 11, 3)
 *      // std::vector(12, 1, 10, 8, 17, 5, 14, 6, 15)
 * \endcode
 */
class SimpleSubsetGenerator : public AbstractFixedSizeSubsetGenerator
{
    Q_GADGET
    CTL_TYPE_ID(1010)

    protected: std::vector<ProjectionDataView> generateSubsetsImpl(uint iteration) const override;

public:
    enum Order { Random, Adjacent, Orthogonal180, Orthogonal360 };
    Q_ENUM(Order)

    explicit SimpleSubsetGenerator(uint nbSubsets = 1, Order subsetOrder = Random);

    void setOrder(Order order);

    // SerializationInterface interface
    void fromVariant(const QVariant& variant) override;
    QVariant toVariant() const override;

private:
    Order _order = Random;

    std::vector<ProjectionDataView>
    makeRandomSubsets(const std::vector<uint>& subsetSizes) const;
    std::vector<ProjectionDataView>
    makeAdjacentSubsets(const std::vector<uint>& subsetSizes) const;
    std::vector<ProjectionDataView>
    makeOrthogonalSubsets(const std::vector<uint>& subsetSizes) const;

    static QMetaEnum metaEnum();
};

} // namespace CTL

#endif // CTL_SIMPLESUBSETGENERATOR_H

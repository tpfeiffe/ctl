#include "simplebackprojector.h"
#include "acquisition/geometryencoder.h"
#include "components/abstractdetector.h"
#include "img/voxelvolume.h"
#include "ocl/clfileloader.h"
#include "ocl/openclconfig.h"

const std::string CL_KERNEL_NAME_BACKSMEAR = "simple_backprojector"; //!< name of the OpenCL kernel function
const std::string CL_FILE_NAME = "recon/simple_backprojector.cl"; //!< path to .cl file

namespace CTL {
namespace OCL {

DECLARE_SERIALIZABLE_TYPE(SimpleBackprojector)

/*!
 * \brief Constructs a SimpleBackprojector instance with weighting type \a weightType.
 *
 * For more details on the weighting, please refer to the documentation of BackprojectorBase.
 */
SimpleBackprojector::SimpleBackprojector(WeightingType weightType)
    : BackprojectorBase(initOpenCLKernels(), weightType)
{
}

/*!
 * \brief Returns the weighting factor for data from \a module in \a view.
 *
 * Uses BackprojectorBase::weighting() to request a potential custom weighting factor for \a view,
 * \a module. If weighting type is DistanceWeightsOnly, the custom weight is the final return value.
 * If weighting type is GeometryFactors, the weighting factor will be multiplied by the result
 * of `precomputedGeometryFactor(view, module)` to achieve a geometry factor based weighting.
 *
 * \sa precomputedGeometryFactor()
 */
float SimpleBackprojector::weighting(uint view, uint module) const
{
    const auto customWeight = BackprojectorBase::weighting(view, module);

    return weightingType() == WeightingType::GeometryFactors
            ? customWeight * precomputedGeometryFactor(view, module)
            : customWeight;
}

/*!
 * \brief Initializes all OpenCL kernels and returns a pointer to the used kernel.
 *
 * This adds kernels for all three footprint types to the OpenCLConfig; then requests the kernel
 * that will be executed.
 */
cl::Kernel* SimpleBackprojector::initOpenCLKernels() const
{
    try // OCL exception catching
    {
        auto& oclConfig = OpenCLConfig::instance();
        // general checks
        if(!oclConfig.isValid())
            throw std::runtime_error("OpenCLConfig is not valid");

        // check if required kernels are already provided
        if(!oclConfig.kernelExists(CL_KERNEL_NAME_BACKSMEAR))
        {
            // load source code from file
            ClFileLoader clFile(CL_FILE_NAME);
            if(!clFile.isValid())
                throw std::runtime_error(CL_FILE_NAME + "\nis not readable");
            const auto clSourceCode = clFile.loadSourceCode();

            // add kernel to OCLConfig
            oclConfig.addKernel(CL_KERNEL_NAME_BACKSMEAR, clSourceCode);
        }

        // Create and return default kernel
        if(auto kernel = oclConfig.kernel(CL_KERNEL_NAME_BACKSMEAR))
            return kernel;
        else
            throw std::runtime_error("SimpleBackprojector: kernel pointer not valid");

    } catch(const cl::Error& err)
    {
        qCritical() << "OpenCL error: " << err.what() << "(" << err.err() << ")";
        throw std::runtime_error("OpenCL error");
    }
}

/*!
 * \copydoc BackprojectorBase::reconstructToPlain
 */
bool SimpleBackprojector::reconstructToPlain(const ProjectionDataView& projections,
                                             VoxelVolume<float>& targetVolume)
{
    // set additional kernel arguments
    /*
     *   ...,
     *   uint subSamplingFactor, --> arg 9
     */
    backsmearKernel()->setArg(9, _volumeSubsamplingFactor);

    return BackprojectorBase::reconstructToPlain(projections, targetVolume);
}

/*!
 * \copybrief AbstractReconstructor::parameter
 *
 * Returns a QVariantMap including the following (key, value)-pair:
 *
 * - ("volume subsample factor", [uint] sub-sampling factor for the volume).
 */
QVariant SimpleBackprojector::parameter() const
{
    QVariantMap ret = BackprojectorBase::parameter().toMap();

    ret.insert(QStringLiteral("volume subsample factor"), _volumeSubsamplingFactor);

    return ret;
}

/*!
 * \copybrief AbstractReconstructor::setParameter
 *
 * The passed \a parameter must be a QVariantMap containing the following (key, value)-pair:
 *
 * - ("volume subsample factor", [uint] sub-sampling factor for the volume).
 *
 * Note that it is strongly discouraged to use this method to set individual parameters of the
 * instance. Please consider using individual setter methods for that purpose.
 */
void SimpleBackprojector::setParameter(const QVariant& parameter)
{
    BackprojectorBase::setParameter(parameter);

    const QVariantMap parMap = parameter.toMap();

    if(parMap.contains(QStringLiteral("volume subsample factor")))
        setVolumeSubsamplingFactor(parMap.value(QStringLiteral("volume subsample factor")).toUInt());
}

/*!
 * \copydoc BackprojectorBase::toVariant
 *
 * In addition to any base class information, this introduces a (key,value)-pair:
 * ("#", [string] this class' name).
 */
QVariant SimpleBackprojector::toVariant() const
{
    QVariantMap ret = BackprojectorBase::toVariant().toMap();

    ret.insert(QStringLiteral("#"), SimpleBackprojector::staticMetaObject.className());

    return ret;
}

/*!
 * \brief Sets the volume sub-sampling factor to \a volumeSubsamplingFactor.
 *
 * During backprojection, each voxel in the target volume will be sub-divided in
 * \a volumeSubsamplingFactor sub-voxels in each dimension, thus, ending up with
 * \a volumeSubsamplingFactor³ sub-voxels per original voxel. For the purpose of backprojction,
 * these sub-voxels are treated as if they were regular voxels (i.e. they undergo the same
 * processing algorithm). The end result (i.e. of the original voxel) will be the average of all
 * sub-voxels' results.
 *
 * This can increase accuracy, however, it goes along with substantially increased computational
 * effort.
 *
 * \a volumeSubsamplingFactor must be larger than zero.
 */
void SimpleBackprojector::setVolumeSubsamplingFactor(uint volumeSubsamplingFactor)
{
    if(!volumeSubsamplingFactor)
        throw std::runtime_error("SimpleBackprojector::setVolumeSubsamplingFactor: sub-sampling "
                                 "factor must not be zero!");
    _volumeSubsamplingFactor = volumeSubsamplingFactor;
}

} // namespace OCL
} // namespace CTL

#ifndef CTL_SFPBACKPROJECTOR_H
#define CTL_SFPBACKPROJECTOR_H

#include "backprojectorbase.h"

namespace CTL {

class FullGeometry;

namespace OCL {

/*!
 * \class SFPBackprojector
 * \brief The SFPBackprojector class is an OpenCL implementation of the separable footprint approach
 * for backprojection.
 *
 * The SFPBackprojector implements a backprojection operation (i.e. a transformation from projection
 * to volume domain) based on the separable footprint approach.
 *
 * The separable footprint method was proposed by:
 *     Authors:  Long, Fessler and Balter
 *     Title:    3D Forward and Back-Projection for X-Ray CT Using Separable Footprints
 *     Journal:  IEEE Trans Med Imaging, 29(11): 1839–1850, 2010
 *
 * This class implements the two original footprint shapes as proposed by Long et al., namely the
 * trapezoid-rectangle (TR) and trapezoid-trapezoid (TT) variants. In addition to classical TR and
 * TT, a more flexible, yet computationally more expensive, version is provided with the
 * "Generic TT" footprint. All approaches use the A1 amplitude approximation (i.e. based solely on
 * the detector coordinates) as proposed by Long et al.
 * The footprint shape can be set during construction (default: TR) and changed later on through
 * setFootprintType().
 *
 * ![Comparison of the three footprint shapes (TR, TT, Generic TT) and the corresponding image created with ray casting (RC).](sfp_footprint-shapes.png)
 *
 * For more details on usage of reconstructors in general, please also refer to the documentation
 * of AbstractReconstructor.
 */
class SFPBackprojector : public BackprojectorBase
{
    CTL_TYPE_ID(11)
    Q_GADGET

public:
    enum FootprintType { TR, TT, TT_Generic };
    Q_ENUM(FootprintType)

    explicit SFPBackprojector(FootprintType footprintType = TR,
                              WeightingType weightType = BackprojectorWeighting::GeometryFactors);

    // AbstractReconstructor interface
    bool reconstructToPlain(const ProjectionDataView& projections,
                            VoxelVolume<float>& targetVolume) override;

    void setFootprintType(FootprintType footprintType);
    FootprintType footprintType() const;

    // SerializationInterface interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;
    QVariant toVariant() const override;

protected:
    // BackprojectorBase interface
    void transferAdditionalData(uint startView, uint nbViews) override;
    float weighting(uint view, uint module) const override;

private:
    FootprintType _fpType;
    QVector<float> _angleCorrAzi; //!< angle correction azimuthal (fan)
    QVector<float> _angleCorrPol; //!< angle correction polar (cone)
    cl::Buffer _aziBuffer;
    cl::Buffer _polBuffer;

    cl::Kernel* initOpenCLKernels(FootprintType footprintType) const;

    // angle corrections
    void createAngleCorrBuffers();
    void performAnisotropyCorrection(const QSize& nbPixels,
                                     const FullGeometry& pMats,
                                     mat::Matrix<3,1> voxelSize);
    static QVector<float> determineAzimutCorrection(const QSize& nbPixels,
                                                    const CTL::FullGeometry& pMats);
    static QVector<float> determineThetaCorrection(const QSize& nbPixels,
                                                   const CTL::FullGeometry& pMats);
    static float phi(  uint s, const mat::ProjectionMatrix& pMat, uint nbRows);
    static float theta(uint t, const mat::ProjectionMatrix& pMat, uint nbChannels);

    static QMetaEnum metaEnum();
};

} // namespace OCL
} // namespace CTL

#endif // CTL_SFPBACKPROJECTOR_H

#include "abstractdetector.h"
#include "genericdetector.h"

namespace CTL {

std::unique_ptr<GenericDetector> AbstractDetector::toGeneric() const
{
    return GenericDetector::fromOther(*this);
}

} // namespace CTL

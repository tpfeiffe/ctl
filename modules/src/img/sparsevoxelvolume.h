#ifndef CTL_SPARSEVOXELVOLUME_H
#define CTL_SPARSEVOXELVOLUME_H

#include <array>

#include "voxelvolume.h"

typedef unsigned int uint;

namespace CTL {

template <typename> class Range;

class SparseVoxelVolume
{
public:
    class SingleVoxel;

    using VoxelSize  = VoxelVolume<float>::VoxelSize;
    using Dimensions = VoxelVolume<float>::Dimensions;
    using Offset     = VoxelVolume<float>::Offset;

public:

    typedef std::vector<SingleVoxel>::iterator iterator;
    typedef std::vector<SingleVoxel>::const_iterator const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    // ctors
    SparseVoxelVolume(const VoxelSize& voxelSize);
    SparseVoxelVolume(const VoxelSize& voxelSize, std::vector<SingleVoxel> data);

    // voxel iterators
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    void addVoxel(const SingleVoxel& voxel);
    void addVoxel(float x, float y, float z, float val);
    std::array<Range<float>, 3> boundingBox() const;
    const std::vector<SingleVoxel>& data() const;
    std::vector<SingleVoxel>& data();
    uint nbVoxels() const;
    void removeVoxel(uint i);
    float sparsityLevel() const;
    float sparsityLevel(const Dimensions& referenceDimension) const;
    const SingleVoxel& voxel(uint i) const;
    SingleVoxel& voxel(uint i);
    const VoxelSize& voxelSize() const;

    // conversion to VoxelVolume
    void paintToVoxelVolume(VoxelVolume<float>& volume) const;
    VoxelVolume<float> toVoxelVolume() const;
    VoxelVolume<float> toVoxelVolume(const Offset& offset) const;
    VoxelVolume<float> toVoxelVolume(const Dimensions& dimension,
                                     const Offset& offset = { 0.0f, 0.0f, 0.0f }) const;

private:
    std::vector<SingleVoxel> _data;
    VoxelSize _voxelSize;
};

class SparseVoxelVolume::SingleVoxel
{
public:
    SingleVoxel(float x, float y, float z, float val)
        : _x(x), _y(y), _z(z), _value(val)
    { }

    float x() const { return _x; }
    float y() const { return _y; }
    float z() const { return _z; }
    float  value() const { return _value; }
    float& value()       { return _value; }

    operator       float&()       { return _value; }
    operator const float&() const { return _value; }

private:
    float _x;
    float _y;
    float _z;
    float _value;
};

} // namespace CTL

#endif // CTL_SPARSEVOXELVOLUME_H

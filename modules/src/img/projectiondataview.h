#ifndef CTL_PROJECTIONDATAVIEW_H
#define CTL_PROJECTIONDATAVIEW_H

#include <limits>
#include <vector>

#include "projectiondata.h"

namespace CTL {

/*!
 * \brief The ProjectionDataView class is a read-only wrapper for a reference to ProjectionData.
 *
 * This class provides a read-only view on an existent ProjectionData object. ProjectionDataView can
 * be used to efficiently pass around read access to projections without memory overhead. An
 * important feature of the view is its ability to manage subsets of the projections included in the
 * original ProjectionData object. By providing a vector of projection indices that shall be
 * included in the subset, a ProjectionDataView can be created and used in a very similar fashion as
 * an actual ProjectionData object that would contain the specified projections. An example of this
 * is shown in the following:
 * \code
 *  ProjectionData fullProjections(10, 10, 1);
 *  fullProjections.allocateMemory(10);
 *
 *  for(uint v = 0; v < fullProjections.nbViews(); ++v)
 *      fullProjections.view(v).fill(static_cast<float>(v));
 *
 *  // this creates a ProjectionDataView on all projections in 'fullProjections'
 *  ProjectionDataView projView(fullProjections);
 *
 *  // se can see this, e.g., through the viewIds
 *  qInfo() << projView.viewIds();              // output: std::vector(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
 *
 *  // we can create a subset that includes every other view, like this:
 *  ProjectionDataView subset(fullProjections, { 0, 2, 4, 6, 8 });
 *  qInfo() << subset.viewIds();                // output: std::vector(0, 2, 4, 6, 8)
 *
 *  // when we address the second view in 'subset' we are automatically directed to viewId 2 in the original data
 *  qInfo() << subset.view(1).max();            // output: 2 (because we are in viewId 2)
 * \endcode
 *
 * Subsets of ProjectionDataView can also be created through the subset() method. This allows us,
 * for example, to further restrict an already existing subset. If we continue our above example and
 * select only the second and fourth view that is managed in `subset`, we get:
 * \code
 *  // note that the ids we pass here refer to the ids in 'subset' (i.e. 0 to 4) not in the original data
 *  auto moreRestrictedSubset = subset.subset( { 1, 3 } );
 *  qInfo() << moreRestrictedSubset.viewIds();  // output: std::vector(2, 6)
 * \endcode
 */
class ProjectionDataView
{
public:
    ProjectionDataView(const ProjectionData& projections, std::vector<uint> viewIds);
    ProjectionDataView(const ProjectionData& projections);
    ProjectionDataView();
    ProjectionDataView(ProjectionData&& projections) = delete;
    ProjectionDataView(ProjectionData&& projections, std::vector<uint> viewIds) = delete;

    // setter
    void resetData(const ProjectionData& projections, std::vector<uint> viewIds);
    void resetData(ProjectionData&& projections, std::vector<uint> viewIds) = delete;

    // factory for invalid (empty) view, which does not refer to any data
    static ProjectionDataView invalidView();
    // factory for dangling view, for direct use of the view by another function (single expression)
    static ProjectionDataView dangling(ProjectionData&& projections);
    static ProjectionDataView dangling(ProjectionData&& projections, std::vector<uint> viewIds);

    // data access
    const SingleViewData& first() const;
    const SingleViewData& view(uint i) const;
    ProjectionData::Dimensions dimensions() const;
    SingleViewData::Dimensions viewDimensions() const;

    // other methods
    ProjectionData combined(const ModuleLayout& layout = ModuleLayout()) const;
    bool isValid() const;
    float max() const;
    float min() const;
    uint nbViews() const;
    std::vector<float> toVector() const;

    // extended interface
    bool containsView(uint viewId) const;
    void shuffle();
    void shuffle(uint seed);
    ProjectionDataView shuffled() const;
    ProjectionDataView shuffled(uint seed) const;
    ProjectionDataView subset(const std::vector<uint>& idsInView) const;
    ProjectionDataView subset(uint pos = 0, uint count = std::numeric_limits<uint>::max()) const;
    const std::vector<uint>& viewIds() const;

    // conversion operations
    explicit operator ProjectionData() const;
    ProjectionData dataCopy() const;

    // comparison operators
    bool operator==(const ProjectionDataView& other) const;
    bool operator!=(const ProjectionDataView& other) const;

    // arithmetic operators
    ProjectionData operator+(const ProjectionDataView& other) const;
    ProjectionData operator-(const ProjectionDataView& other) const;
    ProjectionData operator*(float factor) const;
    ProjectionData operator/(float divisor) const;

private:
    template <class Function>
    void parallelExecution(const Function& f) const;

    const ProjectionData* _dataPtr = nullptr;
    std::vector<uint> _viewIds;
};

} // namespace CTL

#endif // CTL_PROJECTIONDATAVIEW_H

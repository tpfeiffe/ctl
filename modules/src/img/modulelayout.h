#ifndef CTL_MODULELAYOUT_H
#define CTL_MODULELAYOUT_H

#include <vector>

typedef unsigned int uint; //!< Qt style alias for unsigned int.

namespace CTL {

/*!
 * \class ModuleLayout
 *
 * \brief Simple class that holds the layout of a multi module detector.
 *
 * The ModuleLayout class stores the layout of a detector that consists of multiple individual
 * flat panel modules. That means it holds the information about the arrangement of the individual
 * modules on the entire detector unit. For means of simplicity, this is limited to arrangement
 * patterns with a rectangular grid shape.
 *
 * A ModuleLayout is required if you want to combine projection data of individual modules to a
 * single projection. This can be done using SingleViewData::combined().
 *
 * To define a layout, the number of rows and columns of the grid need to be specified. Then, for
 * each position on the grid, the index of the flat panel module that is located at that spot must
 * be defined. This information is stored internally in a std::vector<int> with row-major order.
 * Negative module indices can be used to define gaps in the layout (see also
 * SingleViewData::combined()).
 *
 * For simple arrangements, the convenience factory method canonicLayout() can be used to easily
 * create the corresponding ModuleLayout.
 */
class ModuleLayout
{
public:
    ModuleLayout(uint nbRows = 0, uint nbCols = 0);

    int& operator()(uint row, uint col);
    const int& operator()(uint row, uint col) const;

    uint columns() const;
    uint rows() const;
    uint nbNonEmptyModules() const;

    bool isEmpty() const;
    bool hasUniqueIndices() const;
    int largestModuleIdx() const;
    std::vector<uint> uniqueIndices() const;

    static ModuleLayout canonicLayout(uint nbRows, uint nbCols, bool rowMajorOrder = true);

private:
    uint _rows; //!< The number of rows in the layout.
    uint _cols; //!< The number of columns in the layout.
    std::vector<int> _layout; //!< The internal data vector.
};

} // namespace CTL

/*! \file */

#endif // CTL_MODULELAYOUT_H

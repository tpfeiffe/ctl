#include "modulelayout.h"

#include <algorithm>
#include <iterator>

namespace CTL {

/*!
 * Constructs a ModuleLayout object for a module arrangement with \a nbRows rows and \a nbCols
 * columns. The entire layout is initialized with index -1. Use operator() to assign module
 * indices to the layout positions.
 *
 * For simple layouts, consider using canonicLayout() for easy construction.
 *
 * \code
 * // setting up a linear layout with five modules by hand
 * ModuleLayout linearLayout(1,5);
 * for(uint col = 0; col < linearLayout.columns(); ++col)
 *     linearLayout(0, col) = col;
 * // The resulting layout is: | 0 || 1 || 2 || 3 || 4 |
 * // The same result can be achieved with canonicLayout(1,5).
 *
 * // arrange eight modules in a 3x3 square layout that has a gap in the center
 * ModuleLayout squareLayout(3, 3);
 * squareLayout(0,0) = 0, squareLayout(0,1) = 1, squareLayout(0,2) = 2;     // first row
 * squareLayout(1,0) = 3;                                                   // center row
 * // squareLayout(1,1); <- this is the gap (already initialized with -1)   // center row
 * squareLayout(1,2) = 4;                                                   // center row
 * squareLayout(2,0) = 5, squareLayout(2,1) = 6, squareLayout(2,2) = 7;     // last row
 * // The resulting layout is:
 * // | 0 || 1 || 2 |
 * //  -------------
 * // | 3 ||   || 4 |
 * //  -------------
 * // | 5 || 6 || 7 |
 * \endcode
 *
 * \sa canonicLayout().
 */
ModuleLayout::ModuleLayout(uint nbRows, uint nbCols)
    : _rows(nbRows)
    , _cols(nbCols)
    , _layout(nbRows * nbCols, -1)
{
}

/*!
 * Returns a (modifiable) reference to the module index at layout position [\a row, \a col].
 * \code
 * // setting up a linear layout with five modules
 * ModuleLayout linearLayout = ModuleLayout::canonicLayout(1, 6);   // layout: | 0 || 1 || 2 || 3 || 4 || 5 |
 *
 * // replace the odd numbered modules by their predecessors
 * for(uint m = 1; m < linearLayout.columns(); m += 2)
 *      linearLayout(0, m) = m-1;                                   // layout: | 0 || 0 || 2 || 2 || 4 || 4 |
 *
 * // remove all modules at these positions
 * for(uint m = 1; m < linearLayout.columns(); m += 2)
 *      linearLayout(0, m) = -1;                                    // layout: | 0 ||   || 2 ||   || 4 ||   |
 * \endcode
 */
int& ModuleLayout::operator()(uint row, uint col) { return _layout[row * _cols + col]; }

/*!
 * Returns a constant reference to the module index at layout position [\a row, \a col].
 */
const int& ModuleLayout::operator()(uint row, uint col) const { return _layout[row * _cols + col]; }

/*!
 * Returns the number of columns in the layout.
 */
uint ModuleLayout::columns() const { return _cols; }

/*!
 * Returns the number of rows in the layout.
 */
uint ModuleLayout::rows() const { return _rows; }

/*!
 * \brief Returns the number of entries that is larger than zero.
 *
 * This refers to the number of modules not considered a gap (negative index value).
 */
uint ModuleLayout::nbNonEmptyModules() const
{
    return static_cast<uint>(std::count_if(_layout.cbegin(), _layout.cend(),
                                           [] (int idx) { return idx >= 0; }));
}

/*!
 * Returns true if either the number of rows or columns in this layout is zero.
 */
bool ModuleLayout::isEmpty() const { return (_rows == 0) || (_cols == 0); }

/*!
 * \brief Returns \c true if all indices in this instance appear at maximum once.
 */
bool ModuleLayout::hasUniqueIndices() const
{
    return uniqueIndices().size() == nbNonEmptyModules();
}

/*!
 * \brief Returns the largest module index in this layout or -1 if the layout is empty.
 */
int ModuleLayout::largestModuleIdx() const
{
    if(_layout.empty())
        return -1;

    return *std::max_element(_layout.cbegin(), _layout.cend());
}

/*!
 * \brief Returns a set (i.e. no repetitions) of all indices appearing in this layout. Does not
 * list negative values (i.e. gaps) if they should appear in the layout.
 */
std::vector<uint> ModuleLayout::uniqueIndices() const
{
    std::vector<uint> nonZeroIds;
    nonZeroIds.reserve(_layout.size());
    std::copy_if(_layout.cbegin(), _layout.cend(), std::back_inserter(nonZeroIds),
                 [] (int idx) { return idx >= 0; });

    std::sort(nonZeroIds.begin(), nonZeroIds.end());
    auto last = std::unique(nonZeroIds.begin(), nonZeroIds.end());
    nonZeroIds.erase(last, nonZeroIds.end());

    return nonZeroIds;
}

// clang-format off
/*!
 * Constructs and returns a ModuleLayout object for a module arrangement with \a nbRows rows
 * and \a nbCols columns. The layout is initialized with increasing module index across the layout.
 * By default, this will be done in row mayor order. To change this behavior to column major
 * order, set \a rowMajorOrder to \c false.
 *
 * \code
 * // setting up a linear layout (e.g. for cylindrical detectors) with ten modules
 * ModuleLayout linearLayout = ModuleLayout::canonicLayout(1, 10);
 * for(uint col = 0; col < linearLayout.columns(); ++col)
 *     std::cout << linearLayout(0, col) << " ";                        // output: 0 1 2 3 4 5 6 7 8 9
 *
 * // setting up a square layout with 3x3 modules in column major order.
 * ModuleLayout squareLayout = ModuleLayout::canonicLayout(3, 3, false);
 * for(uint row = 0; row < squareLayout.rows(); ++row){                 // output:
 *      for(uint col = 0; col < squareLayout.columns(); ++col)          // 0 3 6
 *          std::cout << squareLayout(row, col) << " ";                 // 1 4 7
 *      std::cout << std::endl;                                         // 2 5 8
 * }
 * \endcode
 */
// clang-format on
ModuleLayout ModuleLayout::canonicLayout(uint nbRows, uint nbCols, bool rowMajorOrder)
{
    ModuleLayout ret(nbRows, nbCols);
    int idx = 0;
    if(rowMajorOrder) // row major order
        for(uint r = 0; r < nbRows; ++r)
            for(uint c = 0; c < nbCols; ++c)
                ret(r, c) = idx++;
    else // column major order
        for(uint c = 0; c < nbCols; ++c)
            for(uint r = 0; r < nbRows; ++r)
                ret(r, c) = idx++;

    return ret;
}

} // namespace CTL

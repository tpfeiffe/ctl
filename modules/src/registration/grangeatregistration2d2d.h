#ifndef CTL_GRANGEATREGISTRATION2D2D_H
#define CTL_GRANGEATREGISTRATION2D2D_H

#include "mat/homography.h"
#include "mat/projectionmatrix.h"
#include "processing/errormetrics.h"
#include <nlopt.hpp>

namespace CTL {

namespace imgproc {
class AbstractErrorMetric;
} // namespace imgproc

namespace OCL {
class IntermedGen2D2D;
} // namespace OCL

namespace NLOPT {

class GrangeatRegistration2D2D
{
public:
    mat::Homography3D optimize(const std::vector<Chunk2D<float>>&        sourceImages,
                               const std::vector<mat::ProjectionMatrix>& sourcePMats,
                               const std::vector<Chunk2D<float>>&        targetImages,
                               const std::vector<mat::ProjectionMatrix>& targetPMats);

    const imgproc::AbstractErrorMetric* metric() const;
    void setMetric(const imgproc::AbstractErrorMetric* metric);

    double angluarSamplingLevel() const;
    float maxLineDistance() const;
    float subSamplingLevel() const;
    bool truncationSieveEnabled() const;
    float truncationThreshold() const;

    void setMaxLineDistance(float maxLineDistance);
    void setSubSamplingLevel(float subSamplingLevel);
    void setTruncationSieve(float extinctionThreshold, bool enabled = true);
    void setAngluarSamplingLevel(double angluarSamplingLevel);
    void toggleSubsampling(bool enabled);
    void toggleTruncationSieve(bool enabled);

    nlopt::opt& optObject();

private:
    nlopt::opt _opt{ nlopt::algorithm::LN_SBPLX, 6u };
    const imgproc::AbstractErrorMetric* _metric = &metric::L2;
    double _angluarSamplingLevel = 1.0; //! portion of angular samples
    float _maxLineDistance = 1.0f; //!< relative maximum of `s`
    float _subSamplingLevel = 1.0f; //!< portion of a random subset of samples
    float _truncationThreshold = 0.1f; //!< extinction values at the detector border above this value mean truncation
    bool _useTruncationSieve = false; //!< flag to exclude truncated detector lines using `_truncationThreshold`

    OCL::IntermedGen2D2D buildIntermedGenerator(double angularIncrement) const;
    mat::Homography3D performOptimization();
};

} // namespace NLOPT
} // namespace CTL

#endif // CTL_GRANGEATREGISTRATION2D2D_H

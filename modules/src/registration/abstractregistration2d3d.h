#ifndef CTL_ABSTRACTREGISTRATION2D3D_H
#define CTL_ABSTRACTREGISTRATION2D3D_H

#include <vector>

namespace nlopt {
class opt;
}

namespace CTL {

template <typename T> class Chunk2D;
namespace OCL {
class VolumeResampler;
}
namespace mat {
class Homography3D;
class ProjectionMatrix;
}
namespace imgproc {
class AbstractErrorMetric;
}

class AbstractRegistration2D3D
{
    public:virtual mat::Homography3D optimize(const Chunk2D<float>& projectionImage,
                                              const OCL::VolumeResampler& volume,
                                              const mat::ProjectionMatrix& pMat) = 0;

    // optional overload for multiview registration (throws expection if not overridden)
    public:virtual mat::Homography3D optimize(const std::vector<Chunk2D<float>>& projectionImages,
                                              const OCL::VolumeResampler& volume,
                                              const std::vector<mat::ProjectionMatrix>& pMats);

    public:virtual const imgproc::AbstractErrorMetric* metric() const = 0;
    public:virtual void setMetric(const imgproc::AbstractErrorMetric* metric) = 0;

    public:virtual float subSamplingLevel() const = 0;
    public:virtual void setSubSamplingLevel(float subSamplingLevel) = 0;

    public:virtual bool truncationSieveEnabled() const = 0;
    public:virtual float truncationThreshold() const = 0;
    public:virtual void setTruncationSieve(float extinctionThreshold, bool enabled = true) = 0;

public:
    virtual ~AbstractRegistration2D3D() = default;

protected:
    AbstractRegistration2D3D() = default;
    AbstractRegistration2D3D(const AbstractRegistration2D3D&) = default;
    AbstractRegistration2D3D(AbstractRegistration2D3D&&) = default;
    AbstractRegistration2D3D& operator=(const AbstractRegistration2D3D&) = default;
    AbstractRegistration2D3D& operator=(AbstractRegistration2D3D&&) = default;
};

class AbstractNloptRegistration2D3D : public AbstractRegistration2D3D
{
    // abstract interface
    public:virtual nlopt::opt& optObject() = 0;

protected:
    AbstractNloptRegistration2D3D() = default;
    AbstractNloptRegistration2D3D(const AbstractNloptRegistration2D3D&) = default;
    AbstractNloptRegistration2D3D(AbstractNloptRegistration2D3D&&) = default;
    AbstractNloptRegistration2D3D& operator=(const AbstractNloptRegistration2D3D&) = default;
    AbstractNloptRegistration2D3D& operator=(AbstractNloptRegistration2D3D&&) = default;
};

} // namespace CTL

#endif // CTL_ABSTRACTREGISTRATION2D3D_H

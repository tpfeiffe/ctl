#include "grangeatregistration2d2d.h"
#include "io/printnloptmsg.h"
#include "processing/consistency.h"

namespace CTL {
namespace NLOPT {

namespace {

struct Data
{
    const std::vector<OCL::ImageResampler>&   sourceIntermedFct;
    std::vector<mat::ProjectionMatrix>        sourcePMats;
    const std::vector<OCL::ImageResampler>&   targetIntermedFct;
    const std::vector<mat::ProjectionMatrix>& targetPMats;
    const Chunk2D<float>::Dimensions&         projSize;
    const imgproc::AbstractErrorMetric&       metric;
    const OCL::IntermedGen2D2D&               gen;
    const std::vector<Chunk2D<float>>&        sourceImages;
    const std::vector<Chunk2D<float>>&        targetImages;
};

std::vector<OCL::ImageResampler> preComputeIntermedFct(const std::vector<Chunk2D<float>>& images,
                                                       const std::vector<mat::ProjectionMatrix>& pMats,
                                                       float maxLineDistance);

bool checkImageDimensions(const std::vector<Chunk2D<float>>& sourceImages,
                          const std::vector<Chunk2D<float>>& targetImages);

double computeAngularIncrement(const mat::ProjectionMatrix& pMat);

double computeInconsistency(const Data& data);

Homography3D euclidianHomoFromParam(const std::vector<double>& x);

void adjustPMats(std::vector<mat::ProjectionMatrix>& pMats, const std::vector<double>& x);

double objFct(const std::vector<double>& x, std::vector<double>& grad, void* data);

} // unnamed namespace

mat::Homography3D GrangeatRegistration2D2D::optimize(const std::vector<Chunk2D<float>>& sourceImages,
                                                     const std::vector<mat::ProjectionMatrix>& sourcePMats,
                                                     const std::vector<Chunk2D<float>>& targetImages,
                                                     const std::vector<mat::ProjectionMatrix>& targetPMats)
{
    if(sourceImages.size() != sourcePMats.size() || targetImages.size() != targetPMats.size())
        throw std::domain_error("Number of projection images does not fit to the number of the "
                                "corresponding projection matrices.");
    if(!checkImageDimensions(sourceImages, targetImages))
        throw std::domain_error("The current implementation requires that all 2D projection images "
                                "have the same size.");

    qInfo("Precompute intermediate functions...");
    const auto sourceIntermedFct = preComputeIntermedFct(sourceImages, sourcePMats, _maxLineDistance);
    const auto targetIntermedFct = preComputeIntermedFct(targetImages, targetPMats, _maxLineDistance);

    const auto angularIncr = std::min(computeAngularIncrement(sourcePMats.front()),
                                      computeAngularIncrement(targetPMats.front())) / _angluarSamplingLevel;
    qDebug() << "anguar incr: " << angularIncr*180./PI << " deg\n";
    const OCL::IntermedGen2D2D gen = buildIntermedGenerator(angularIncr);

    auto data = Data{ sourceIntermedFct, sourcePMats,
                      targetIntermedFct, targetPMats,
                      sourceImages.front().dimensions(), *_metric, gen,
                      sourceImages, targetImages };

    auto consistency = computeInconsistency(data);
    qDebug() << "initial consistency: " << consistency << '\n';

    _opt.set_min_objective(objFct, &data);

    return performOptimization();
}

const imgproc::AbstractErrorMetric* GrangeatRegistration2D2D::metric() const { return _metric; }

void GrangeatRegistration2D2D::setMetric(const imgproc::AbstractErrorMetric* metric)
{
    if(metric == nullptr)
        qWarning("A nullptr to AbstractErrorMetric is ignored.");
    else
        _metric = metric;
}

float GrangeatRegistration2D2D::subSamplingLevel() const { return _subSamplingLevel; }

void GrangeatRegistration2D2D::setSubSamplingLevel(float subSamplingLevel)
{
    _subSamplingLevel = subSamplingLevel;
}

bool GrangeatRegistration2D2D::truncationSieveEnabled() const
{
    return _useTruncationSieve;
}

float GrangeatRegistration2D2D::truncationThreshold() const
{
    return _truncationThreshold;
}

void GrangeatRegistration2D2D::setTruncationSieve(float extinctionThreshold, bool enabled)
{
    _truncationThreshold = extinctionThreshold;
    _useTruncationSieve = enabled;
}

void GrangeatRegistration2D2D::setAngluarSamplingLevel(double angluarSamplingLevel)
{
    _angluarSamplingLevel = angluarSamplingLevel;
}

void GrangeatRegistration2D2D::toggleSubsampling(bool enabled)
{
    if(!enabled)
        _subSamplingLevel = 1.0f;
}

void GrangeatRegistration2D2D::toggleTruncationSieve(bool enabled)
{
    _useTruncationSieve = enabled;
}

nlopt::opt& GrangeatRegistration2D2D::optObject() { return _opt; }

double GrangeatRegistration2D2D::angluarSamplingLevel() const
{
    return _angluarSamplingLevel;
}

float GrangeatRegistration2D2D::maxLineDistance() const
{
    return _maxLineDistance;
}

void GrangeatRegistration2D2D::setMaxLineDistance(float maxLineDistance)
{
    _maxLineDistance = maxLineDistance;
}

OCL::IntermedGen2D2D GrangeatRegistration2D2D::buildIntermedGenerator(double angularIncrement) const
{
    OCL::IntermedGen2D2D ret;

    ret.setAngleIncrement(angularIncrement);
    ret.setMaxLineDistance(_maxLineDistance);
    if(!qFuzzyCompare(_subSamplingLevel, 1.0f))
        ret.setSubsampleLevel(_subSamplingLevel);
    ret.setTruncationThreshold(_truncationThreshold, _useTruncationSieve);

    return ret;
}

mat::Homography3D GrangeatRegistration2D2D::performOptimization()
{
    // perform optimization
    std::vector<double> param(6, 0.0);
    double remainInconsistency;
    auto errCode = _opt.optimize(param, remainInconsistency);

    // output messages
    qDebug() << "remaining inconsistency:" << remainInconsistency;
    printNloptMsg(errCode);

    return euclidianHomoFromParam(param);
}

namespace {

std::vector<OCL::ImageResampler> preComputeIntermedFct(const std::vector<Chunk2D<float>>& images,
                                                       const std::vector<mat::ProjectionMatrix>& pMats,
                                                       float maxLineDistance)
{
    std::vector<OCL::ImageResampler> ret;
    ret.reserve(images.size());

    const auto imgDiag = mat::Matrix<2, 1>(images.front().width(),
                                           images.front().height()).norm();
    const auto sMax = imgDiag * maxLineDistance;
    const auto sRange = SamplingRange(-0.5 * sMax, 0.5 * sMax);
    const auto nbS = static_cast<uint>(std::ceil(sRange.width())); // ~1 pixel line spacing

    const auto muRange = SamplingRange(0.0_deg, 180.0_deg);
    const auto nbMu = static_cast<uint>(std::ceil(nbS * PI_2));

    std::transform(images.begin(), images.end(), pMats.begin(), std::back_inserter(ret),
                   [sRange, nbS, muRange, nbMu] (const Chunk2D<float>& img,
                                                 const mat::ProjectionMatrix& pMat)
    {
        constexpr auto useWeighting = true;
        constexpr auto derivative   = imgproc::CentralDifference;

        return OCL::IntermediateProj{ img, pMat.intrinsicMatK(), useWeighting }
               .sampler(muRange, nbMu, sRange, nbS, derivative);
    });

    return ret;
}

bool checkImageDimensions(const std::vector<Chunk2D<float>>& sourceImages,
                          const std::vector<Chunk2D<float>>& targetImages)
{
    const auto referenceDims = sourceImages.front().dimensions();

    for(auto const& img : sourceImages)
        if(img.dimensions() != referenceDims)
            return false;

    for(auto const& img : targetImages)
        if(img.dimensions() != referenceDims)
            return false;

    return true;
}

//!
//! The angular increment is chosen in a way that adjacent lines close to the principal point on the
//! detector have a distance of (at most) one pixel.
//!
double computeAngularIncrement(const mat::ProjectionMatrix& pMat)
{
    // x << 1 => x ~ asin(x) and x <= asin(x)
    const auto sdd = pMat.focalLength();
    return 1.0 / std::max(sdd.get<0>(), sdd.get<1>());
}

double computeInconsistency(const Data& data)
{
    auto threadTask = [&data](size_t iBegin, size_t iEnd, double& partialSum) {
        for(size_t i = iBegin; i < iEnd; ++i)
            for(size_t j = 0, jEnd = data.targetIntermedFct.size(); j < jEnd; ++j)
            {
                const auto intermedFctPair
                    = data.gen.intermedFctPair(data.sourceIntermedFct[i], data.sourcePMats[i],
                                               data.targetIntermedFct[j], data.targetPMats[j],
                                               data.projSize, data.sourceImages[i], data.targetImages[j]);

                partialSum += intermedFctPair.inconsistency(data.metric);
            }
    };

    const auto nbSourceViews = data.sourceIntermedFct.size();
    auto tp = ThreadPool{std::min(ThreadPool{}.nbThreads(), nbSourceViews)};
    const auto nbThreads = tp.nbThreads();
    auto partialSums = std::vector<double>(nbThreads, 0.0);

    const auto viewsPerThread = nbSourceViews / nbThreads;
    size_t t = 0;
    for(; t < nbThreads - 1; ++t)
        tp.enqueueThread(threadTask, t * viewsPerThread, (t + 1) * viewsPerThread, std::ref(partialSums[t]));
    tp.enqueueThread(threadTask, t * viewsPerThread, nbSourceViews, std::ref(partialSums[t]));
    tp.waitForFinished();

    return std::accumulate(partialSums.cbegin(), partialSums.cend(), 0.0);
}

Homography3D euclidianHomoFromParam(const std::vector<double>& x)
{
    // optimization uses deg internally for rotational degrees of freedom (since "deg ~ mm")
    const auto rotMat = [](Vector3x1 axisDeg) {
        return mat::rotationMatrix(axisDeg * (PI / 180.0));
    };

    return { rotMat({ x[0], x[1], x[2] }), { x[3], x[4], x[5] } };
}

void adjustPMats(std::vector<mat::ProjectionMatrix>& pMats, const std::vector<double>& x)
{
    const auto H = euclidianHomoFromParam(x);
    for(auto& pMat : pMats)
        pMat = pMat * H;
}

double objFct(const std::vector<double>& x, std::vector<double>&, void* data)
{
    // qDebug("objFct call");
    auto dataCopy = *static_cast<const Data*>(data);

    adjustPMats(dataCopy.sourcePMats, x);

    return computeInconsistency(dataCopy);
}

} // unnamed namespace

} // namespace NLOPT
} // namespace CTL

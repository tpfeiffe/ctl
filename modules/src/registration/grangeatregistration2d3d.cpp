#include "grangeatregistration2d3d.h"
#include "io/printnloptmsg.h"
#include "processing/consistency.h"

namespace CTL {
namespace NLOPT {

namespace {

struct DataForSingleViewOptimization
{
    const std::shared_ptr<const std::vector<float>>& projIntermedFct;
    const OCL::VolumeResampler& volumeIntermedResampler;
    const OCL::Radon3DCoordTransform& radon3DCoordTransform;
    const imgproc::AbstractErrorMetric& metric;
};

struct DataForMultiViewOptimization
{
    DataForSingleViewOptimization dataForSingleView(uint viewNb) const;

    const std::vector<std::shared_ptr<const std::vector<float>>>& projIntermedFcts;
    const OCL::VolumeResampler& volumeIntermedResampler;
    const std::vector<OCL::Radon3DCoordTransform>& radon3DCoordTransforms;
    const imgproc::AbstractErrorMetric& metric;
};

const auto MULTI_VIEW_ACCUMULATOR = std::plus<double>{};

float computeDeltaS(const OCL::VolumeResampler& volIntermedFct, const mat::ProjectionMatrix& pMat);
Homography3D euclidianHomoFromParam(const std::vector<double>& x);
double singleViewInconsistency(const DataForSingleViewOptimization& d, const Homography3D& H);
double singleViewObjFct(const std::vector<double>& x, std::vector<double>& grad, void* data);
double multiViewObjFct(const std::vector<double>& x, std::vector<double>& grad, void* data);

} // unnamed namespace

mat::Homography3D GrangeatRegistration2D3D::optimize(const Chunk2D<float>& projectionImage,
                                                     const OCL::VolumeResampler& volumeIntermedResampler,
                                                     const mat::ProjectionMatrix& pMat)
{
    // calculate s spacing in sinogram
    const auto deltaS = computeDeltaS(volumeIntermedResampler, pMat);
    Q_ASSERT(deltaS > 0.0f);
    // initialize IntermedGen2D3D
    auto gen = buildIntermedGenerator(deltaS);

    // calculate initial intermediate functions
    auto initalIntermedFctPair = gen.intermedFctPair(projectionImage, pMat, volumeIntermedResampler);
    qDebug() << "initial inconsistency:" << initalIntermedFctPair.inconsistency(*_metric);

    // initialize transformation of 3d Radon coordinates
    const OCL::Radon3DCoordTransform coordTransform{ gen.lastSampling() };

    // initialize optimization
    DataForSingleViewOptimization data{ initalIntermedFctPair.ptrToFirst(), volumeIntermedResampler, coordTransform,
                                        *_metric };
    _opt.set_min_objective(singleViewObjFct, &data);

    return performOptimization();
}

mat::Homography3D GrangeatRegistration2D3D::optimize(const std::vector<Chunk2D<float>>& projectionImages,
                                                     const OCL::VolumeResampler& volumeIntermedResampler,
                                                     const std::vector<mat::ProjectionMatrix>& pMats)
{
    if(projectionImages.size() != pMats.size())
        throw std::domain_error{ "Number of projectionImages must fit the number of pMats." };

    const auto nbViews = projectionImages.size();

    std::vector<std::shared_ptr<const std::vector<float>>> projectionIntermediateFcts;
    projectionIntermediateFcts.reserve(nbViews);

    std::vector<OCL::Radon3DCoordTransform> coordTransforms;
    coordTransforms.reserve(nbViews);

    auto initialInconsistency = 0.0;

    for(auto i = 0u; i < nbViews; ++i)
    {
        // calculate s spacing in sinogram
        const auto deltaS = computeDeltaS(volumeIntermedResampler, pMats.at(i));
        Q_ASSERT(deltaS > 0.0f);
        // initialize IntermedGen2D3D
        auto gen = buildIntermedGenerator(deltaS);

        // calculate initial intermediate functions
        auto initalIntermedFctPair = gen.intermedFctPair(projectionImages[i], pMats[i], volumeIntermedResampler);
        initialInconsistency = MULTI_VIEW_ACCUMULATOR(initialInconsistency,
                                                      initalIntermedFctPair.inconsistency(*_metric));

        projectionIntermediateFcts.push_back(initalIntermedFctPair.ptrToFirst());

        // initialize transformation of 3d Radon coordinates
        coordTransforms.emplace_back(gen.lastSampling());
    }

    qDebug() << "initial inconsistency:" << initialInconsistency;

    // initialize optimization
    DataForMultiViewOptimization data{ projectionIntermediateFcts, volumeIntermedResampler, coordTransforms,
                                       *_metric };
    _opt.set_min_objective(multiViewObjFct, &data);

    return performOptimization();
}

nlopt::opt& GrangeatRegistration2D3D::optObject() { return _opt; }

float GrangeatRegistration2D3D::maxLineDistance() const
{
    return _maxLineDistance;
}

void GrangeatRegistration2D3D::setMaxLineDistance(float maxLineDistance)
{
    _maxLineDistance = maxLineDistance;
}

OCL::IntermedGen2D3D GrangeatRegistration2D3D::buildIntermedGenerator(float deltaS) const
{
    OCL::IntermedGen2D3D ret;
    ret.setLineSpacing(deltaS);
    ret.setMaxLineDistance(_maxLineDistance); // relative maximum of `s`
    if(_subSamplingLevel != 1.0f)
        ret.setSubsampleLevel(_subSamplingLevel);
    ret.setTruncationThreshold(_truncationThreshold, _useTruncationSieve);

    return ret;
}

mat::Homography3D GrangeatRegistration2D3D::performOptimization()
{
    // perform optimization
    std::vector<double> param(6, 0.0);
    double remainInconsistency;
    auto errCode = _opt.optimize(param, remainInconsistency);

    // output messages
    qDebug() << "remaining inconsistency:" << remainInconsistency;
    printNloptMsg(errCode);

    return euclidianHomoFromParam(param);
}

const imgproc::AbstractErrorMetric* GrangeatRegistration2D3D::metric() const { return _metric; }

void GrangeatRegistration2D3D::setMetric(const imgproc::AbstractErrorMetric* metric)
{
    if(metric == nullptr)
        qWarning("A nullptr to AbstractErrorMetric is ignored.");
    else
        _metric = metric;
}

float GrangeatRegistration2D3D::subSamplingLevel() const { return _subSamplingLevel; }

void GrangeatRegistration2D3D::setSubSamplingLevel(float subSamplingLevel)
{
    _subSamplingLevel = subSamplingLevel;
}

bool GrangeatRegistration2D3D::truncationSieveEnabled() const
{
    return _useTruncationSieve;
}

float GrangeatRegistration2D3D::truncationThreshold() const
{
    return _truncationThreshold;
}

void GrangeatRegistration2D3D::setTruncationSieve(float extinctionThreshold, bool enabled)
{
    _truncationThreshold = extinctionThreshold;
    _useTruncationSieve = enabled;
}

namespace {

float computeDeltaS(const OCL::VolumeResampler& volIntermedFct, const mat::ProjectionMatrix& pMat)
{
    Q_ASSERT(volIntermedFct.volDim().z > 1);
    const auto& dRange = volIntermedFct.rangeDim3();
    const auto deltaD = (dRange.end() - dRange.start()) / (volIntermedFct.volDim().z - 1);
    const auto magnification = float(pMat.magnificationX() + pMat.magnificationY()) * 0.5f;
    return magnification * deltaD;
}

Homography3D euclidianHomoFromParam(const std::vector<double>& x)
{
    // optimization uses deg internally for rotational degrees of freedom (since "deg ~ mm")
    const auto rotMat = [](Vector3x1 axisDeg) {
        return mat::rotationMatrix(axisDeg * (PI / 180.0));
    };

    // x = [r_ t_] = [rx ry rz tx ty tz]
    return { rotMat({ x[0], x[1], x[2] }), { x[3], x[4], x[5] } };
}

double singleViewInconsistency(const DataForSingleViewOptimization& d, const Homography3D& H)
{
    const auto& transformedBuf = d.radon3DCoordTransform.transform(H);
    const IntermediateFctPair intermPair{ d.projIntermedFct,
                                          d.volumeIntermedResampler.sample(transformedBuf),
                                          IntermediateFctPair::VolumeDomain };
    return intermPair.inconsistency(d.metric);
}

double singleViewObjFct(const std::vector<double>& x, std::vector<double>&, void* data)
{
    return singleViewInconsistency(*static_cast<DataForSingleViewOptimization*>(data),
                                   euclidianHomoFromParam(x));
}

double multiViewObjFct(const std::vector<double>& x, std::vector<double>&, void* data)
{
    auto ret = 0.0;

    const auto H = euclidianHomoFromParam(x);
    const auto d = static_cast<DataForMultiViewOptimization*>(data);
    const auto nbViews = d->projIntermedFcts.size();

    for(auto viewNb = 0u; viewNb < nbViews; ++viewNb)
        ret = MULTI_VIEW_ACCUMULATOR(ret, singleViewInconsistency(d->dataForSingleView(viewNb), H));

    return ret;
}

DataForSingleViewOptimization DataForMultiViewOptimization::dataForSingleView(uint viewNb) const
{
    return { projIntermedFcts[viewNb],
             volumeIntermedResampler,
             radon3DCoordTransforms[viewNb],
             metric };
}

} // unnamed namespace

} // namespace NLOPT
} // namespace CTL

// interpolating sampler with `0` as boundary color
constant sampler_t samp = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP | CLK_FILTER_LINEAR;

float3 rotateCoordinate(constant float* rotMat, float3 coord)
{
    return (float3)( dot((float3)(rotMat[0], rotMat[1], rotMat[2]), coord),
                     dot((float3)(rotMat[3], rotMat[4], rotMat[5]), coord),
                     dot((float3)(rotMat[6], rotMat[7], rotMat[8]), coord) );
}

// resample kernel
kernel void resample( constant float2* range1,
                      constant float2* range2,
                      constant float2* range3,
                      constant float* samplePts1,
                      constant float* samplePts2,
                      constant float* samplePts3,
                      constant float* invRotationMat,
                      read_only image3d_t volume,
                      global float* resampledVol)
{
    // Output sample IDs
    const uint sampleID1 = get_global_id(0);
    const uint sampleID2 = get_global_id(1);
    const uint sampleID3 = get_global_id(2);
    // dimension of input image
    const int4 volDim = get_image_dim(volume);

    // scales from unit (defined by range) to voxel number
    const float scaleX = ((float)volDim.x - 1.0f) / ((*range1).s1 - (*range1).s0);
    const float scaleY = ((float)volDim.y - 1.0f) / ((*range2).s1 - (*range2).s0);
    const float scaleZ = ((float)volDim.z - 1.0f) / ((*range3).s1 - (*range3).s0);
    // calculate voxel coordinate of input image for current sample IDs
    float4 voxCoord;
    float3 rotatedSamplePt = rotateCoordinate(invRotationMat,
                                              (float3)(samplePts1[sampleID1],
                                                       samplePts2[sampleID2],
                                                       samplePts3[sampleID3]));
    voxCoord.x = scaleX * (rotatedSamplePt.x - (*range1).s0);
    voxCoord.y = scaleY * (rotatedSamplePt.y - (*range2).s0);
    voxCoord.z = scaleZ * (rotatedSamplePt.z - (*range3).s0);
    voxCoord.w = 0.0f;
    // OpenCL Grid offset
    voxCoord += (float4)0.5f;

    // interpolate value in input image
    const float4 resampledVal = read_imagef(volume, samp, voxCoord);

    // write value to output buffer
    resampledVol[sampleID1 +
                 sampleID2 * get_global_size(0) +
                 sampleID3 * get_global_size(0) * get_global_size(1)] = resampledVal.x;
}

// sample kernel
kernel void sample( constant float2* range1,
                    constant float2* range2,
                    constant float2* range3,
                    constant float* samplePts,
                    read_only image3d_t volume,
                    global float* resampledVol)
{
    // Output sample IDs
    const uint sampleID = get_global_id(0);

    // dimension of input image
    const int4 volDim = get_image_dim(volume);

    // scales from unit (defined by range) to voxel number
    const float scaleX = ((float)volDim.x - 1.0f) / ((*range1).s1 - (*range1).s0);
    const float scaleY = ((float)volDim.y - 1.0f) / ((*range2).s1 - (*range2).s0);
    const float scaleZ = ((float)volDim.z - 1.0f) / ((*range3).s1 - (*range3).s0);
    // calculate voxel coordinate of input image for current sample IDs
    float4 voxCoord;
    voxCoord.x = scaleX * (samplePts[3 * sampleID] - (*range1).s0);
    voxCoord.y = scaleY * (samplePts[3 * sampleID+1] - (*range2).s0);
    voxCoord.z = scaleZ * (samplePts[3 * sampleID+2] - (*range3).s0);
    voxCoord.w = 0.0f;
    // OpenCL Grid offset
    voxCoord += (float4)0.5f;

    // interpolate value in input image
    const float4 resampledVal = read_imagef(volume, samp, voxCoord);

    // write value to output buffer
    resampledVol[sampleID] = resampledVal.x;
}

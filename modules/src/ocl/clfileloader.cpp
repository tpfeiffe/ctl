#include "clfileloader.h"

#include <QCoreApplication>
#include <QFileInfo>
#include <QString>

#include <stdexcept>

static QString determineOpenCLSourceDir();
static void ensureProperEnding(QString& path);

namespace CTL {

QString ClFileLoader::_oclSourceDir;

// constructors
/*!
 * Constructs a ClFileLoader object using \a fileName as the path of the .cl file.
 * \sa setFileName()
 */
ClFileLoader::ClFileLoader(const char* fileName)
    : _fn(fileName)
{
}

/*!
 * Constructs a ClFileLoader object using \a fileName as the path of the .cl file.
 * \sa setFileName()
 */
ClFileLoader::ClFileLoader(std::string fileName)
    : _fn(std::move(fileName))
{
}

/*!
 * Constructs a ClFileLoader object using \a fileName as the path of the .cl file.
 * \sa setFileName()
 */
ClFileLoader::ClFileLoader(const QString& fileName)
    : _fn(fileName.toStdString())
{
}

// setter
/*!
 * \sa setFileName(std::string fileName)
 */
void ClFileLoader::setFileName(const char* fileName) { _fn = fileName; }

/*!
 * (Re)sets the path of the .cl file. The \a fileName shall be passed to this method by the relative
 * path wrt the "cl_src" directory.
 * \sa fileName()
 */
void ClFileLoader::setFileName(std::string fileName) { _fn = std::move(fileName); }

/*!
 * \sa setFileName(std::string fileName)
 */
void ClFileLoader::setFileName(const QString& fileName) { _fn = fileName.toStdString(); }

// getter
/*!
 * Returns the relative path within the "cl_src" directory to the .cl file set by setFileName()
 * or a constructor.
 */
const std::string& ClFileLoader::fileName() const { return _fn; }

/*!
 * Returns true if the .cl file is readable, otherwise false.
 */
bool ClFileLoader::isValid() const
{
    if(_fn.empty())
        return false;

    QFileInfo fi(absoluteOpenCLSourceDir() + QString::fromStdString(_fn));
    return fi.isReadable();
}

/*!
 * Returns the content of the .cl file as a std::string.
 */
std::string ClFileLoader::loadSourceCode() const
{
    QFile file(absoluteOpenCLSourceDir() + QString::fromStdString(_fn));
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        throw std::runtime_error{"ClFileLoader::loadSourceCode: Cannot open " + file.fileName().toStdString()};

    return file.readAll().toStdString();
}

/*!
 * Returns the content of an .cl file specified by an absolute path \a absoluteFilePath.
 * This convenience function allows using OpenCL kernel files that are not located in the "cl_src" folder.
 */
std::string ClFileLoader::loadExternalSourceCode(const char* absoluteFilePath)
{
    return loadExternalSourceCode(QString{ absoluteFilePath });
}

/*!
 * Returns the content of an .cl file specified by an absolute path \a absoluteFilePath.
 * This convenience function allows using OpenCL kernel files that are not located in the "cl_src" folder.
 */
std::string ClFileLoader::loadExternalSourceCode(const std::string& absoluteFilePath)
{
    return loadExternalSourceCode(QString::fromStdString(absoluteFilePath));
}

/*!
 * Returns the content of an .cl file specified by an absolute path \a absoluteFilePath.
 * This convenience function allows using OpenCL kernel files that are not located in the "cl_src" folder.
 */
std::string ClFileLoader::loadExternalSourceCode(const QString& absoluteFilePath)
{
    QFile file(absoluteFilePath);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        throw std::runtime_error{"ClFileLoader::loadExternalSourceCode: Cannot open " + file.fileName().toStdString()};

    return file.readAll().toStdString();
}

/*!
 * Sets the path of the OpenCL source directory to \a path. The filename of an OpenCL C file (.cl)
 * is a relative path w.r.t. this directory. If this function is never called or called with an
 * empty string, a default directory is used, which is the folder "cl_src" in the directory of the
 * current application (the executable).
 *
 * \sa setFileName(const char* path)
 */
void ClFileLoader::setOpenCLSourceDir(const char* path)
{
    setOpenCLSourceDir(QString(path));
}

/*!
 * Sets the path of the OpenCL source directory to \a path. The filename of an OpenCL C file (.cl)
 * is a relative path w.r.t. this directory. If this function is never called or called with an
 * empty string, a default directory is used, which is the folder "cl_src" in the directory of the
 * current application (the executable), or, in case CTL_LIBRARY was defined, the macro
 * OCL_SOURCE_DIR is used.
 *
 * \sa setFileName(const QString& path)
 */
void ClFileLoader::setOpenCLSourceDir(const QString& path)
{
    _oclSourceDir = path;
    ensureProperEnding(_oclSourceDir);
}

/*!
 * Sets the path of the OpenCL source directory to \a path. The filename of an OpenCL C file (.cl)
 * is a relative path w.r.t. this directory. If this function is never called or called with an
 * empty string, a default directory is used, which is the folder "cl_src" in the directory of the
 * current application (the executable), or, in case CTL_LIBRARY was defined, the macro
 * OCL_SOURCE_DIR is used.
 *
 * \sa setFileName(const QString& path)
 */
void ClFileLoader::setOpenCLSourceDir(QString&& path)
{
    _oclSourceDir = std::move(path);
    ensureProperEnding(_oclSourceDir);
}

/*!
 * Sets the path of the OpenCL source directory to \a path. The filename of an OpenCL C file (.cl)
 * is a relative path w.r.t. this directory. If this function is never called or called with an
 * empty string, a default directory is used, which is the folder "cl_src" in the directory of the
 * current application (the executable), or, in case CTL_LIBRARY was defined, the macro
 * OCL_SOURCE_DIR is used.
 *
 * \sa setFileName(std::string path)
 */
void ClFileLoader::setOpenCLSourceDir(const std::string& path)
{
    setOpenCLSourceDir(QString::fromStdString(path));
}

/*!
 * Returns the path to the OpenCL directory, where OpenCL C kernel files are stored.
 * This could be the default path ("cl_src" next to the executable or specified by OCL_SOURCE_DIR)
 * or the path that has been set by setOpenCLSourceDir().
 */
const QString& ClFileLoader::openCLSourceDir()
{
    return absoluteOpenCLSourceDir();
}

/*!
 * Returns the absolute path to the "ocl_src" directory. If no OpenCL source directory has been set,
 * it determines the absolute path "<executable's directory>/cl_src" in a platform-independent way
 * provided that `QCoreApplication` has been instatiated in the main() of the application.
 * In case CTL_LIBRARY was defined (CTL as pre-compiled library), the macro OCL_SOURCE_DIR is used.
 */
const QString& ClFileLoader::absoluteOpenCLSourceDir()
{
    if(_oclSourceDir.isEmpty())
        _oclSourceDir = determineOpenCLSourceDir();

    return _oclSourceDir;
}

} // namespace CTL

QString determineOpenCLSourceDir()
{
#ifdef CTL_LIBRARY
    QString ret = QStringLiteral(OCL_SOURCE_DIR);
    ensureProperEnding(ret);
    return ret;
#else
    QString ret = QCoreApplication::applicationDirPath();
    ensureProperEnding(ret);
    ret += QStringLiteral("cl_src/");
    return ret;
#endif
}

static void ensureProperEnding(QString& path)
{
    if(!path.isEmpty() &&
       !path.endsWith(QStringLiteral("/")) &&
       !path.endsWith(QStringLiteral("\\")))
        path += QStringLiteral("/");
}

#ifndef CTL_OPENCLFUNCTIONS_H
#define CTL_OPENCLFUNCTIONS_H

#include <string>
#include <vector>

namespace CTL {

/*!
 * \class OpenCLFunctions
 *
 * \brief The OpenCLFunctions class provides additional OpenCL C functions
 *
 * Instances of the ClFileLoader class represent collections of OpenCL C functions, which may be
 * used in an OpenCL program and in particular in the implementation of OpenCL kernel code. Such an
 * instance can be passed to the `OCL::OpenCLConfig::addKernel` function in order to equip a kernel
 * with the according additional OpenCL functions. By this approach, additional functions are made
 * available to the OpenCL programmer that are not standardized in OpenCL C.
 *
 * The functions that are available are represented by a list of `static` member functions of the
 * OpenCLFunctions class. This list may be extended during the development of the CTL toolkit.
 * An instance of a OpenCLFunctions object is created using the constructor, eg.
 * \code
 *   OpenCLFunctions myFunctions{ OpenCLFunctions::write_bufferf, OpenCLFunctions::atomic_addf_l };
 * \endcode
 * Here, `myFunctions` represents a set consisting of the two functions `write_bufferf` and
 * `atomic_addf_l`. This object can be passed when creating a kernel:
 * \code
 *   std::string kernelSourceCode = ClFileLoader("path_to/myKernel.cl").loadSourceCode();
 *   OCL::OpenCLConfig::instance().addKernel("myKernelFunctionName", myFunctions, kernelSourceCode);
 * \endcode
 *
 * Note that by using this approach, it is is also possible that the internal implementation of a
 * function provided by OpenCLFunctions uses other functions from OpenCLFunctions, as long as all
 * required functions are passed to the `addKernel` function (all function declarations will be
 * prefixed to the kernel source code, followed by all definitions).
 */
class OpenCLFunctions
{
public:
    class OpenCLFunction;
    using OpenCLFunctionGen = OpenCLFunction (*)();

    OpenCLFunctions(std::initializer_list<OpenCLFunctionGen> functionList);
    OpenCLFunctions(OpenCLFunctionGen function);

    const std::vector<OpenCLFunction>& functions() const;
    std::string declarations() const;
    std::string declarationsAndDefinitions() const;

    // list of `static` generator functions providing the OpenCL C source code
    static OpenCLFunction atomic_addf_g();
    static OpenCLFunction atomic_addf_l();
    static OpenCLFunction write_bufferf();
    // ...

private:
    std::vector<OpenCLFunction> _functions; //!< The list of OpenCL C functions
};

/*!
 * \class OpenCLFunctions::OpenCLFunction
 *
 * \brief Representation of a single OpenCL C function
 *
 * Instances of this class can be directly constructed from a string or via the `static` generator
 * functions of OpenCLFunctions. Each object wraps a string with the OpenCL C definition of the
 * function.
 */
class OpenCLFunctions::OpenCLFunction
{
public:
    OpenCLFunction(const char* clSourceCode);
    OpenCLFunction(std::string clSourceCode);

    std::string declaration() const;
    std::string definition() const;
    std::string signature() const;

private:
    std::string _code; //!< Source code (definition) of the OpenCL C function
};

} // namespace CTL

#endif // CTL_OPENCLFUNCTIONS_H

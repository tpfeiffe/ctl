#ifndef CTL_RAWDATAIO_H
#define CTL_RAWDATAIO_H

#include "io/metainfokeys.h"

#include <type_traits>

#include <QDebug>
#include <QFile>
#include <QVariantMap>

namespace CTL {
namespace io {

template<uint dim1, uint dim2, typename RawType, uint skipBytes = 0, uint gap = 0>
class RawDataIO
{
public:
    QVariantMap metaInfo(const QString& fileName) const;

    template <typename T>
    std::vector<T> readAll(const QString& fileName) const;
    template <typename T>
    std::vector<T> readChunk(const QString& fileName, uint chunkNb) const;

    template <typename T>
    bool write(const std::vector<T>& data,
               const QVariantMap& metaInfo,
               const QString& fileName) const;

private:
    using Self = RawDataIO<dim1, dim2, RawType, skipBytes, gap>;

    static uint dim3(const QString& fileName);

    struct BlockProcessing
    {
        template <typename T>
        static size_t read(QFile& infile, size_t dim3, std::vector<T>& data);
        template <typename T>
        static bool write(QFile& outfile, size_t dim3, const std::vector<T>& data);
    };
    struct ChunkByChunkProcessing
    {
        template <typename T>
        static size_t read(QFile& infile, size_t dim3, std::vector<T>& data);
        template <typename T>
        static bool write(QFile& outfile, size_t dim3, const std::vector<T>& data);
    };

    using Processing = typename std::conditional<gap == 0,
                                                 typename Self::BlockProcessing,
                                                 typename Self::ChunkByChunkProcessing>::type;
};


template<uint dim1, uint dim2, typename RawType, uint skipBytes, uint gap>
uint RawDataIO<dim1, dim2, RawType, skipBytes, gap>::dim3(const QString& fileName)
{
    QFile file(fileName);
    const auto fileSize = static_cast<size_t>(file.size()) - skipBytes;
    constexpr auto bytesPerChunk = dim1 * dim2 * sizeof(RawType);

    // throw if file does not contain a multiple of size of an individual data chunk
    if((fileSize + gap) % (bytesPerChunk + gap))
        throw std::runtime_error("RawDataIO: File does not contain a multiple of the chunk size.");

    return static_cast<uint>((fileSize + gap) / (bytesPerChunk + gap));
}

template<uint dim1, uint dim2, typename RawType, uint skipBytes, uint gap>
QVariantMap RawDataIO<dim1, dim2, RawType, skipBytes, gap>::metaInfo(const QString& fileName) const
{
    QVariantMap ret;

    CTL::io::meta_info::Dimensions dimensions( dim1, dim2, Self::dim3(fileName) );

    ret.insert(CTL::io::meta_info::dimensions, QVariant::fromValue(dimensions));

    return ret;
}

template<uint dim1, uint dim2, typename RawType, uint skipBytes, uint gap>
template<typename T>
std::vector<T> RawDataIO<dim1, dim2, RawType, skipBytes, gap>::readAll(const QString& fileName) const
{
    QFile infile(fileName);
    if(!infile.open(QIODevice::ReadOnly))
    {
        qCritical() << QString("Could not open file %1 for reading.").arg(fileName);
        return std::vector<T>();
    }

    const auto dim3 = Self::dim3(fileName);
    std::vector<RawType> rawValues(dim1 * dim2 * dim3);
    constexpr auto numBytesPerChunk = dim1 * dim2 * sizeof(RawType);
    const auto numBytes = numBytesPerChunk * dim3;
    infile.seek(skipBytes);

    const auto bytesRead = Processing::read(infile, dim3, rawValues);

    if(bytesRead != numBytes)
        qWarning() << QString("Did not read the expected amount of data (Expected: %1, Read: %2).")
                      .arg(numBytes).arg(bytesRead);

    // shortcut if RawType == T
    // C++17 version
    #if __cplusplus >= 201703L
    if constexpr (std::is_same_v<RawType, T>)
        return rawValues;
    #else
    if(std::is_same<RawType, T>::value)
    {
        std::vector<T> ret(rawValues.size());
        std::copy(rawValues.cbegin(), rawValues.cend(), ret.begin());
        return ret;
    }
    #endif

    // conversion from RawType -> T
    std::vector<T> ret(rawValues.size());
    std::transform(rawValues.cbegin(), rawValues.cend(), ret.begin(),
                   [] (const RawType& value) { return static_cast<T>(value); } );

    return ret;
}

template<uint dim1, uint dim2, typename RawType, uint skipBytes, uint gap>
template<typename T>
std::vector<T> RawDataIO<dim1, dim2, RawType, skipBytes, gap>::readChunk(const QString& fileName,
                                                                         uint chunkNb) const
{
    QFile infile(fileName);
    if(!infile.open(QIODevice::ReadOnly))
    {
        qCritical() << QString("Could not open file %1 for reading.").arg(fileName);
        return std::vector<T>();
    }
    if(chunkNb >= dim3(fileName))
    {
        qCritical() << "Requested chunk index exceeds number of chunks available in file" << fileName;
        return std::vector<T>();
    }

    std::vector<RawType> rawValues(dim1 * dim2);
    const auto numBytesPerChunk = rawValues.size() * sizeof(RawType);

    // skip until the requested chunk position
    infile.seek((numBytesPerChunk + gap) * chunkNb + skipBytes);
    // read the chunk data
    const auto destination = reinterpret_cast<char*>(rawValues.data());
    const auto bytesRead = static_cast<size_t>(infile.read(destination, numBytesPerChunk));
    infile.close();

    if(bytesRead != numBytesPerChunk)
        qWarning() << QString("Did not read the expected amount of data (Expected: %1, Read: %2).")
                      .arg(numBytesPerChunk).arg(bytesRead);

    // shortcut if RawType == T
    // C++17 version
    #if __cplusplus >= 201703L
    if constexpr (std::is_same_v<RawType, T>)
        return rawValues;
    #else
    if(std::is_same<RawType, T>::value)
    {
        std::vector<T> ret(rawValues.size());
        std::copy(rawValues.cbegin(), rawValues.cend(), ret.begin());
        return ret;
    }
    #endif

    // conversion from RawType -> T
    std::vector<T> ret(rawValues.size());
    std::transform(rawValues.cbegin(), rawValues.cend(), ret.begin(),
                   [] (const RawType& value) { return static_cast<T>(value); } );

    return ret;
}

template<uint dim1, uint dim2, typename RawType, uint skipBytes, uint gap>
template<typename T>
bool RawDataIO<dim1, dim2, RawType, skipBytes, gap>::write(const std::vector<T>& data,
                                                           const QVariantMap& metaInfo,
                                                           const QString& fileName) const
{
    // check if dimensions of this RawDataIO match dimensions in 'metaInfo' (dim1 & dim2)
    const auto dimData = metaInfo.value(meta_info::dimensions).value<meta_info::Dimensions>();
    if(dim1 != dimData.dim1 || dim2 != dimData.dim2)
    {
        qCritical() << QString("RawDataIO: Data to write have different dimensions than this"
                               " instance of RawDataIO [%1 x %2] vs. [%3 x %4]")
                       .arg(dimData.dim1).arg(dimData.dim2).arg(dim1).arg(dim2);
        return false;
    }

    QFile outfile(fileName);
    if(!outfile.open(QIODevice::WriteOnly))
    {
        qCritical() << QString("Could not open file %1 for writing.").arg(fileName);
        return false;
    }

    constexpr auto elementsPerChunk = dim1 * dim2;
    const auto dim3 = data.size() / elementsPerChunk;
    const auto numElements = elementsPerChunk * dim3;

    // write meaningless "skip bytes"
    if(skipBytes)
    {
        std::vector<char> headerMock(skipBytes);
        outfile.write(headerMock.data(), skipBytes);
    }

    // write the data
    // shortcut if RawType == T
    if(std::is_same<RawType, T>::value)
        return Processing::write(outfile, dim3, data);

    // convert input data into correct data type (T -> RawType)
    std::vector<RawType> rawValues(numElements);
    std::transform(data.cbegin(), data.cend(), rawValues.begin(),
                   [] (const T& value) { return static_cast<RawType>(value); } );

    return Processing::write(outfile, dim3, rawValues);
}


// ####################

template<uint dim1, uint dim2, typename RawType, uint skipBytes, uint gap>
template<typename T>
size_t RawDataIO<dim1, dim2, RawType, skipBytes, gap>::BlockProcessing::read(QFile& infile,
                                                                             size_t dim3,
                                                                             std::vector<T>& data)
{
    constexpr auto numBytesPerChunk = dim1 * dim2 * sizeof(RawType);
    const auto numBytes = numBytesPerChunk * dim3;

    const auto destination = reinterpret_cast<char*>(data.data());
    const auto bytesRead = static_cast<size_t>(infile.read(destination, numBytes));

    infile.close();

    return bytesRead;
}

template<uint dim1, uint dim2, typename RawType, uint skipBytes, uint gap>
template<typename T>
bool RawDataIO<dim1, dim2, RawType, skipBytes, gap>::BlockProcessing::write(QFile& outfile,
                                                                            size_t dim3,
                                                                            const std::vector<T>& data)
{
    constexpr auto elementsPerChunk = dim1 * dim2;
    constexpr auto numBytesPerChunk = elementsPerChunk * sizeof(RawType);
    const auto numBytes = numBytesPerChunk * dim3;

    // write the data
    const auto source = reinterpret_cast<const char*>(data.data());
    const auto bytesWritten = static_cast<size_t>(outfile.write(source, numBytes));
    outfile.close();

    return bytesWritten == numBytes;
}

template<uint dim1, uint dim2, typename RawType, uint skipBytes, uint gap>
template<typename T>
size_t RawDataIO<dim1, dim2, RawType, skipBytes, gap>::ChunkByChunkProcessing::read(QFile& infile,
                                                                                    size_t dim3,
                                                                                    std::vector<T>& data)
{
    constexpr auto numBytesPerChunk = dim1 * dim2 * sizeof(RawType);

    size_t bytesRead = 0;
    for(uint chunkNb = 0; chunkNb < dim3; ++chunkNb)
    {
        const auto destination = reinterpret_cast<char*>(data.data()) + chunkNb*numBytesPerChunk;
        bytesRead += static_cast<size_t>(infile.read(destination, numBytesPerChunk));
        infile.seek(infile.pos() + gap);
    };

    infile.close();

    return bytesRead;
}

template<uint dim1, uint dim2, typename RawType, uint skipBytes, uint gap>
template<typename T>
bool RawDataIO<dim1, dim2, RawType, skipBytes, gap>::ChunkByChunkProcessing::write(QFile& outfile,
                                                                                   size_t dim3,
                                                                                   const std::vector<T>& data)
{
    constexpr auto elementsPerChunk = dim1 * dim2;
    constexpr auto numBytesPerChunk = elementsPerChunk * sizeof(RawType);
    const auto numBytes = numBytesPerChunk * dim3;

    size_t bytesWritten = 0;
    for(uint chunkNb = 0; chunkNb < dim3; ++chunkNb)
    {
        const auto source = reinterpret_cast<const char*>(data.data() + chunkNb*elementsPerChunk);
        bytesWritten += static_cast<size_t>(outfile.write(source, numBytesPerChunk));

        // write meaningless "gap bytes"
        if(chunkNb < dim3 - 1)
        {
            std::vector<char> gapMock(gap);
            bytesWritten += static_cast<size_t>(outfile.write(gapMock.data(), gap));
        }
    }

    outfile.close();

    return bytesWritten == numBytes + (dim3 - 1) * gap;
}

} // namespace io
} // namespace CTL

#endif // CTL_RAWDATAIO_H
